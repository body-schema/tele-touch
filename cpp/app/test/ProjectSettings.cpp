#include "ProjectSettings.h"


void ProjectSettings::ReadCurrentSettings() {
    mINI::INIStructure config;
    mINI::INIFile configFile("./config/settings.ini");
    configFile.read(config);
    ROBOT_POS_INIT_R = Eigen::Vector3d(
            std::stod(config["RobotController"]["x_init"]),
            std::stod(config["RobotController"]["y_init"]),
            std::stod(config["RobotController"]["z_init"]));
    double X_MIN = std::stod(config["RobotController"]["x_min"]);
    double X_MAX = std::stod(config["RobotController"]["x_max"]);
    double Y_MIN = std::stod(config["RobotController"]["y_min"]);
    double Y_MAX = std::stod(config["RobotController"]["y_max"]);
    double Z_MIN = std::stod(config["RobotController"]["z_min"]);
    double Z_MAX = std::stod(config["RobotController"]["z_max"]);
    MIN_VECT = Eigen::Vector3d(X_MIN, Y_MIN, Z_MIN);
    MAX_VECT = Eigen::Vector3d(X_MAX, Y_MAX, Z_MAX);
    MAX_VEL_NORM = std::stod(config["RobotController"]["norm_cartesian_vel_max"]);
    MAX_VEL_COMPENSATION_MULTIPLIER = std::stod(config["RobotController"]["max_vel_compensation_multiplier"]);
    MAX_VEL_DECREASE_MULTIPLIER = std::stod(config["RobotController"]["max_vel_decrease_multiplier"]);
    MAX_DIST_RECOVERY_MULTIPLIER = std::stod(config["RobotController"]["max_dist_recovery_multiplier"]);
    MOVING_WINDOW_ACTIVE_FROM_SEC = 0.01 * std::stod(config["RobotController"]["moving_window_active_from_ms"]);
    ROBOT_POS_INIT_R = Eigen::Vector3d(
            std::stod(config["RobotController"]["x_init"]),
            std::stod(config["RobotController"]["y_init"]),
            std::stod(config["RobotController"]["z_init"])
    );
}