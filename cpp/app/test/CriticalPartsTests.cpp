#include "CriticalPartsTests.h"
#include "src/utility/PositionComputer.h"
#include "ProjectSettings.h"
#include <iostream>
#include <thread>

TEST_CASE("Trajectory must always be inside boundaries", "[trajectory_boundaries]") {
    double trajectory_duration_sec = .2;
    Eigen::Vector3d trajectory_start_pos = Eigen::Vector3d(-1., 5.5, 3.5);
    Eigen::Vector3d trajectory_goal_pos = Eigen::Vector3d(10., -1, 3.4);
    Eigen::Vector3d MAX_VECT = Eigen::Vector3d(std::max(trajectory_start_pos.x(), trajectory_goal_pos.x()),
                                               std::max(trajectory_start_pos.y(), trajectory_goal_pos.y()),
                                               std::max(trajectory_start_pos.z(), trajectory_goal_pos.z()));
    Eigen::Vector3d MIN_VECT = Eigen::Vector3d(std::min(trajectory_start_pos.x(), trajectory_goal_pos.x()),
                                               std::min(trajectory_start_pos.y(), trajectory_goal_pos.y()),
                                               std::min(trajectory_start_pos.z(), trajectory_goal_pos.z()));
    Eigen::AlignedBox3d BOUNDARY_BOX = Eigen::AlignedBox3d(MIN_VECT, MAX_VECT);

    std::chrono::high_resolution_clock::time_point trajectory_start_timestamp = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point current_timestamp = std::chrono::high_resolution_clock::now();
    while ((std::chrono::duration<double>(current_timestamp - trajectory_start_timestamp)).count() < 2) {
        REQUIRE(BOUNDARY_BOX.contains(PositionComputer::generate_linear_trajectory(
                current_timestamp,
                trajectory_start_timestamp,
                trajectory_duration_sec,
                trajectory_start_pos,
                trajectory_goal_pos
        )));
        current_timestamp = std::chrono::high_resolution_clock::now();
    }
    REQUIRE(BOUNDARY_BOX.contains(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    )));
}

TEST_CASE("Trajectory must always start and end in defined positions", "[trajectory_start_end]") {
    double trajectory_duration_sec = .2;
    Eigen::Vector3d trajectory_start_pos = Eigen::Vector3d(-1., 5.5, 3.5);
    Eigen::Vector3d trajectory_goal_pos = Eigen::Vector3d(10., -1, 3.4);

    std::chrono::high_resolution_clock::time_point trajectory_start_timestamp = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point current_timestamp = trajectory_start_timestamp;
    REQUIRE(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    ) == trajectory_start_pos);
    current_timestamp += std::chrono::seconds(2);
    REQUIRE(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    ) == trajectory_goal_pos);
}

TEST_CASE("Speed must be constant and consistent during real-time trajectory traversing", "[trajectory_speed]") {
    double trajectory_duration_sec = 4.;
    Eigen::Vector3d trajectory_start_pos = Eigen::Vector3d(-10., 0, 0);
    Eigen::Vector3d trajectory_goal_pos = Eigen::Vector3d(10., 0, 0);

    std::chrono::high_resolution_clock::time_point trajectory_start_timestamp = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point current_timestamp = trajectory_start_timestamp;
    current_timestamp += std::chrono::seconds(1);
    auto traversing_speed = abs(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    ).x() - trajectory_start_pos.x()) / 1;
    REQUIRE(traversing_speed == 5);
    current_timestamp += std::chrono::seconds(1);
    traversing_speed = abs(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    ).x() - trajectory_start_pos.x()) / 2;
    REQUIRE(traversing_speed == 5);
    current_timestamp += std::chrono::seconds(1);
    traversing_speed = abs(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    ).x() - trajectory_start_pos.x()) / 3;
    REQUIRE(traversing_speed == 5);
    current_timestamp += std::chrono::seconds(1);
    traversing_speed = abs(PositionComputer::generate_linear_trajectory(
            current_timestamp,
            trajectory_start_timestamp,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    ).x() - trajectory_start_pos.x()) / 4;
    REQUIRE(traversing_speed == 5);
}

TEST_CASE("Intergoal position must always be inside the boundary box", "[intergoal_boundaries]") {
    ProjectSettings projectSettings = ProjectSettings();
    projectSettings.ReadCurrentSettings();
    Eigen::AlignedBox3d BOUNDARY_BOX = Eigen::AlignedBox3d(projectSettings.MIN_VECT, projectSettings.MAX_VECT);

    bool z_inverse = false;
    bool y_inverse = false;

    bool previously_limited_intergoal = false;
    std::chrono::high_resolution_clock::time_point joint_control_timestamp = std::chrono::high_resolution_clock::now();
    MovingWindow mw_robot_pos_r(projectSettings.MOVING_WINDOW_SIZE, projectSettings.MOVING_WINDOW_DEADLINE_MS);
    Eigen::Vector3d test_start_point = projectSettings.MIN_VECT - Eigen::Vector3d(0.01, 0.01, 0.01);
    Eigen::Vector3d current_robot_pos = test_start_point;
    Eigen::Vector3d goal_position = current_robot_pos;
    mw_robot_pos_r.push_back(current_robot_pos, std::chrono::high_resolution_clock::now());
    auto intergoal_pos_r = PositionComputer::compute_intergoal_position(mw_robot_pos_r, goal_position,
                                                                        joint_control_timestamp,
                                                                        previously_limited_intergoal,
                                                                        projectSettings.MIN_VECT,
                                                                        projectSettings.MAX_VECT,
                                                                        projectSettings.MAX_VEL_NORM,
                                                                        projectSettings.MAX_VEL_COMPENSATION_MULTIPLIER,
                                                                        projectSettings.MAX_VEL_DECREASE_MULTIPLIER,
                                                                        projectSettings.MAX_DIST_RECOVERY_MULTIPLIER,
                                                                        projectSettings.MOVING_WINDOW_ACTIVE_FROM_SEC,
                                                                        projectSettings.HAND_AXES,
                                                                        projectSettings.ROBOT_POS_INIT_R,
                                                                        projectSettings.HAND_AXES_INVERSE);
    REQUIRE(BOUNDARY_BOX.contains(intergoal_pos_r));
    while (goal_position.x() < (projectSettings.MAX_VECT.x() + 0.01)) {
        while (goal_position.y() <= (projectSettings.MAX_VECT.y() + 0.01) &&
               goal_position.y() >= test_start_point.y()) {
            while (goal_position.z() <= (projectSettings.MAX_VECT.z() + 0.01) &&
                   goal_position.z() >= test_start_point.z()) {
                intergoal_pos_r = PositionComputer::compute_intergoal_position(mw_robot_pos_r, goal_position,
                                                                               joint_control_timestamp,
                                                                               previously_limited_intergoal,
                                                                               projectSettings.MIN_VECT,
                                                                               projectSettings.MAX_VECT,
                                                                               projectSettings.MAX_VEL_NORM,
                                                                               projectSettings.MAX_VEL_COMPENSATION_MULTIPLIER,
                                                                               projectSettings.MAX_VEL_DECREASE_MULTIPLIER,
                                                                               projectSettings.MAX_DIST_RECOVERY_MULTIPLIER,
                                                                               projectSettings.MOVING_WINDOW_ACTIVE_FROM_SEC,
                                                                               projectSettings.HAND_AXES,
                                                                               projectSettings.ROBOT_POS_INIT_R,
                                                                               projectSettings.HAND_AXES_INVERSE);
                current_robot_pos = intergoal_pos_r;
                joint_control_timestamp = std::chrono::high_resolution_clock::now();
                mw_robot_pos_r.push_back(current_robot_pos, joint_control_timestamp);
                REQUIRE(BOUNDARY_BOX.contains(intergoal_pos_r));
                if (!z_inverse) {
                    goal_position.z() += 0.01;
                } else {
                    goal_position.z() -= 0.01;
                }
                std::this_thread::sleep_for(std::chrono::microseconds(1000));
            }
            if (!z_inverse) {
                goal_position.z() = goal_position.z() - 0.01;
            } else {
                goal_position.z() = goal_position.z() + 0.01;
            }
            z_inverse = !z_inverse;
            if (!y_inverse) {
                goal_position.y() += 0.01;
            } else {
                goal_position.y() -= 0.01;
            }
            std::this_thread::sleep_for(std::chrono::microseconds(1000));
        }
        if (!y_inverse) {
            goal_position.y() = goal_position.y() - 0.01;
        } else {
            goal_position.y() = goal_position.y() + 0.01;
        }
        y_inverse = !y_inverse;
        goal_position.x() += 0.01;
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
    }
    REQUIRE(BOUNDARY_BOX.contains(intergoal_pos_r));
}

TEST_CASE("Speed must be under current limits during distance traversing using intergoal", "[intergoal_speed]") {
    auto MAX_SPEED_M_S = (1.0 * 1000.0) / (60.0 * 60.0); // 1 Km/h
    ProjectSettings projectSettings = ProjectSettings();
    projectSettings.ReadCurrentSettings();
    Eigen::AlignedBox3d BOUNDARY_BOX = Eigen::AlignedBox3d(projectSettings.MIN_VECT, projectSettings.MAX_VECT);

    bool previously_limited_intergoal = false;
    std::chrono::high_resolution_clock::time_point initial_joint_control_timestamp = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point previous_joint_control_timestamp = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point joint_control_timestamp = std::chrono::high_resolution_clock::now();
    MovingWindow mw_robot_pos_r(projectSettings.MOVING_WINDOW_SIZE, projectSettings.MOVING_WINDOW_DEADLINE_MS);
    Eigen::Vector3d test_start_point = projectSettings.MIN_VECT + Eigen::Vector3d(0.01, 0.01, 0.01);
    Eigen::Vector3d test_goal_point = projectSettings.MIN_VECT + Eigen::Vector3d(0.11, 0.01, 0.01);
    Eigen::Vector3d current_robot_pos = test_start_point;
    mw_robot_pos_r.push_back(current_robot_pos, joint_control_timestamp);
    auto intergoal_pos_r = PositionComputer::compute_intergoal_position(mw_robot_pos_r, test_goal_point,
                                                                        joint_control_timestamp,
                                                                        previously_limited_intergoal,
                                                                        projectSettings.MIN_VECT,
                                                                        projectSettings.MAX_VECT,
                                                                        projectSettings.MAX_VEL_NORM,
                                                                        projectSettings.MAX_VEL_COMPENSATION_MULTIPLIER,
                                                                        projectSettings.MAX_VEL_DECREASE_MULTIPLIER,
                                                                        projectSettings.MAX_DIST_RECOVERY_MULTIPLIER,
                                                                        projectSettings.MOVING_WINDOW_ACTIVE_FROM_SEC,
                                                                        projectSettings.HAND_AXES,
                                                                        projectSettings.ROBOT_POS_INIT_R,
                                                                        projectSettings.HAND_AXES_INVERSE);
    joint_control_timestamp = std::chrono::high_resolution_clock::now();
    REQUIRE((current_robot_pos.x() - intergoal_pos_r.x())/(std::chrono::duration<double>(joint_control_timestamp - previous_joint_control_timestamp).count()) < MAX_SPEED_M_S);
    previous_joint_control_timestamp = joint_control_timestamp;
    current_robot_pos = intergoal_pos_r;
    mw_robot_pos_r.push_back(current_robot_pos, joint_control_timestamp);
    while (std::abs(current_robot_pos.x() - test_goal_point.x()) > 0.00001) { // double comparator must not use == or != signs
        intergoal_pos_r = PositionComputer::compute_intergoal_position(mw_robot_pos_r, test_goal_point,
                                                                       joint_control_timestamp,
                                                                       previously_limited_intergoal,
                                                                       projectSettings.MIN_VECT,
                                                                       projectSettings.MAX_VECT,
                                                                       projectSettings.MAX_VEL_NORM,
                                                                       projectSettings.MAX_VEL_COMPENSATION_MULTIPLIER,
                                                                       projectSettings.MAX_VEL_DECREASE_MULTIPLIER,
                                                                       projectSettings.MAX_DIST_RECOVERY_MULTIPLIER,
                                                                       projectSettings.MOVING_WINDOW_ACTIVE_FROM_SEC,
                                                                       projectSettings.HAND_AXES,
                                                                       projectSettings.ROBOT_POS_INIT_R,
                                                                       projectSettings.HAND_AXES_INVERSE);
        joint_control_timestamp = std::chrono::high_resolution_clock::now();
        REQUIRE((current_robot_pos.x() - intergoal_pos_r.x())/(std::chrono::duration<double>(joint_control_timestamp - previous_joint_control_timestamp).count()) < MAX_SPEED_M_S);
        previous_joint_control_timestamp = joint_control_timestamp;
        current_robot_pos = intergoal_pos_r;
        mw_robot_pos_r.push_back(current_robot_pos, joint_control_timestamp);
    }
    REQUIRE((current_robot_pos.x() - test_start_point.x())/(std::chrono::duration<double>(joint_control_timestamp - initial_joint_control_timestamp).count()) < MAX_SPEED_M_S);
}

