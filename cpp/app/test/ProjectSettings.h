#ifndef QUALISYSKINOVAINTERFACE_PROJECTSETTINGS_H
#define QUALISYSKINOVAINTERFACE_PROJECTSETTINGS_H
#include <Eigen/Dense>
#include "ini.h"

class ProjectSettings {
public:
    Eigen::Vector3d MIN_VECT, MAX_VECT;
    Eigen::Vector3d ROBOT_POS_INIT_R;
    double MAX_VEL_NORM, MAX_VEL_COMPENSATION_MULTIPLIER, MAX_VEL_DECREASE_MULTIPLIER, MAX_DIST_RECOVERY_MULTIPLIER;
    double MOVING_WINDOW_ACTIVE_FROM_SEC;
    int MOVING_WINDOW_SIZE, MOVING_WINDOW_DEADLINE_MS;
    const Eigen::Vector3d HAND_AXES = Eigen::Vector3d(1., 0., 0.);
    const Eigen::Vector3d HAND_AXES_INVERSE = -1 * (HAND_AXES - Eigen::Vector3d::Ones());

    void ReadCurrentSettings();
};


#endif //QUALISYSKINOVAINTERFACE_PROJECTSETTINGS_H
