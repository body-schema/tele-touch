
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/app/lib/qualisys-1.23/Markup.cpp" "CMakeFiles/qualisys_cpp_sdk.dir/Markup.cpp.o" "gcc" "CMakeFiles/qualisys_cpp_sdk.dir/Markup.cpp.o.d"
  "/app/lib/qualisys-1.23/Network.cpp" "CMakeFiles/qualisys_cpp_sdk.dir/Network.cpp.o" "gcc" "CMakeFiles/qualisys_cpp_sdk.dir/Network.cpp.o.d"
  "/app/lib/qualisys-1.23/RTPacket.cpp" "CMakeFiles/qualisys_cpp_sdk.dir/RTPacket.cpp.o" "gcc" "CMakeFiles/qualisys_cpp_sdk.dir/RTPacket.cpp.o.d"
  "/app/lib/qualisys-1.23/RTProtocol.cpp" "CMakeFiles/qualisys_cpp_sdk.dir/RTProtocol.cpp.o" "gcc" "CMakeFiles/qualisys_cpp_sdk.dir/RTProtocol.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
