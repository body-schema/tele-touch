#include "PrimitiveSmoothDataset.h"

PrimitiveSmoothDataset::PrimitiveSmoothDataset(int smooth_factor) : smooth_factor{smooth_factor} {

}

void PrimitiveSmoothDataset::AddValue(double value) {
    if (smooth_counter >= smooth_factor - 1) {
        smooth_counter = 0;
        temp_dataset_value += value;
        dataset.push_back(temp_dataset_value / smooth_factor);
        temp_dataset_value = 0.0;
    } else {
        smooth_counter++;
        temp_dataset_value += value;
    }
}

std::vector<double> &PrimitiveSmoothDataset::GetDataset() {
    return dataset;
}

void PrimitiveSmoothDataset::Clear() {
    dataset.clear();
    smooth_counter = 0;
    temp_dataset_value = 0.0;
}

double PrimitiveSmoothDataset::GetLastElement() {
    if (!dataset.empty()) {
        return dataset.back();
    } else {
        return 0;
    }
}
