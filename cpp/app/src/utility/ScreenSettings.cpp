#include "ScreenSettings.h"

ScreenSettings::ScreenSettings(const std::string config_file_path) {
    mINI::INIStructure config;
    // read config
    mINI::INIFile configFile(config_file_path);
    configFile.read(config);
    try {
        SCREEN_HEIGHT_CM = std::stod(config["Gui"]["screen_height_cm"]);
        SCREEN_HEIGHT_PX = std::stod(config["Gui"]["screen_height_px"]);
        SCREEN_PX_DIVIDER = std::stod(config["Gui"]["screen_px_divider"]);
        CM_TO_PX_RATE = SCREEN_HEIGHT_PX / (SCREEN_HEIGHT_CM * SCREEN_PX_DIVIDER);
    } catch (std::exception &e) {
        spdlog::error("GuiApp.OnInit: Unable to parse screen_height_cm, screen_height_px, screen_px_divider: {}",
                      e.what());
        throw std::invalid_argument(
                "GuiApp.OnInit: Screen height in centimeters, pixels and pixels divider must be set");
    }
}

double ScreenSettings::GetScreenHeightCm() const {
    return SCREEN_HEIGHT_CM;
}

double ScreenSettings::GetScreenHeightPx() const {
    return SCREEN_HEIGHT_PX;
}

double ScreenSettings::GetScreenPxDivider() const {
    return SCREEN_PX_DIVIDER;
}

double ScreenSettings::GetCmToPxRate() const {
    return CM_TO_PX_RATE;
}
