#ifndef QUALISYSKINOVAINTERFACE_STRINGWITHANIMATEDTAILDOTS_H
#define QUALISYSKINOVAINTERFACE_STRINGWITHANIMATEDTAILDOTS_H


#include "IntValueIncrementor.h"

class StringWithAnimatedTailDots {

public:
    StringWithAnimatedTailDots(std::string string, double animation_freq_hz, bool add_spaces_at_string_start);
    ~StringWithAnimatedTailDots();

    void StartAnimation();

    void StopAnimation();

    bool IsAnimationStarted();

    std::string GetString();

private:
    std::string string;
    double animation_freq_hz;
    IntValueIncrementor *int_value_incrementor;
    bool is_started;
    bool add_spaces_at_string_start; // May be useful for centered strings
};


#endif //QUALISYSKINOVAINTERFACE_STRINGWITHANIMATEDTAILDOTS_H
