#include <cmath>
#include "IntValueIncrementor.h"

IntValueIncrementor::IntValueIncrementor(int initial_value, int increment_on, double incrementation_freq_hz)
        : initial_value{
        initial_value}, increment_on{increment_on}, incrementation_freq_hz{incrementation_freq_hz} {

}

int IntValueIncrementor::GetValue() {
    if (is_started) {
        auto t_now = std::chrono::high_resolution_clock::now();
        double sec_from_incrementor_start = std::chrono::duration<double>(t_now - start_time).count();
        value = std::floor(sec_from_incrementor_start * incrementation_freq_hz) * increment_on + initial_value;
        return value;
    } else {
        return value;
    }
}

void IntValueIncrementor::Start() {
    start_time = std::chrono::high_resolution_clock::now();
    is_started = true;
}

void IntValueIncrementor::Stop() {
    is_started = false;
    value = initial_value;
}

bool IntValueIncrementor::IsStarted() {
    return is_started;
}
