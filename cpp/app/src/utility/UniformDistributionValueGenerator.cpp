#include "UniformDistributionValueGenerator.h"

UniformDistributionValueGenerator::UniformDistributionValueGenerator(double lowerbound_value, double upperbound_value)
        : lowerbound_value{lowerbound_value}, upperbound_value{upperbound_value} {
    uniform_distribution = std::uniform_real_distribution<double>(lowerbound_value, upperbound_value);
    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    random_engine = std::mt19937(rd()); // Standard mersenne_twister_engine seeded with rd()
}

double UniformDistributionValueGenerator::GetLowerboundValue() const {
    return lowerbound_value;
}

double UniformDistributionValueGenerator::GetUpperboundValue() const {
    return upperbound_value;
}

double UniformDistributionValueGenerator::GetValue() {
    return uniform_distribution(random_engine);
}
