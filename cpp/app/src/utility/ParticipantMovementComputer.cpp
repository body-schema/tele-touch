#include <complex>
#include "ParticipantMovementComputer.h"

ParticipantMovementComputer::ParticipantMovementComputer(double initial_hand_position_x_cm)
        : initial_hand_position_x_cm{initial_hand_position_x_cm} {
    last_hand_position_x_cm = initial_hand_position_x_cm;
}

double ParticipantMovementComputer::GetMovementLengthCM(double new_hand_position_x_cm) {
    double movement_length = new_hand_position_x_cm - initial_hand_position_x_cm;
    last_hand_position_x_cm = new_hand_position_x_cm;
    return std::abs(movement_length);
}
