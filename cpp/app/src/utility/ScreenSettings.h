#ifndef QUALISYSKINOVAINTERFACE_SCREENSETTINGS_H
#define QUALISYSKINOVAINTERFACE_SCREENSETTINGS_H

#include <string>
#include "ini.h"
#include <spdlog/spdlog.h>

class ScreenSettings {

public:
    ScreenSettings(const std::string config_file_path);

    double GetScreenHeightCm() const;

    double GetScreenHeightPx() const;

    double GetScreenPxDivider() const;

    double GetCmToPxRate() const;

private:
    double SCREEN_HEIGHT_CM;
    double SCREEN_HEIGHT_PX;
    double SCREEN_PX_DIVIDER; // for mac screens
    double CM_TO_PX_RATE;

};

inline ScreenSettings *app_screen_settings = nullptr;


#endif //QUALISYSKINOVAINTERFACE_SCREENSETTINGS_H
