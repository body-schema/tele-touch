#ifndef QUALISYSKINOVAINTERFACE_UNIFORMDISTRIBUTIONVALUEGENERATOR_H
#define QUALISYSKINOVAINTERFACE_UNIFORMDISTRIBUTIONVALUEGENERATOR_H


#include <random>

class UniformDistributionValueGenerator {

public:
    UniformDistributionValueGenerator(double lowerbound_value, double upperbound_value);

    double GetLowerboundValue() const;

    double GetUpperboundValue() const;

    double GetValue();

private:
    double lowerbound_value;
    double upperbound_value;
    std::uniform_real_distribution<double> uniform_distribution;
    std::mt19937 random_engine;
};


#endif //QUALISYSKINOVAINTERFACE_UNIFORMDISTRIBUTIONVALUEGENERATOR_H
