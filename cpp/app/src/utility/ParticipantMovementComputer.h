#ifndef QUALISYSKINOVAINTERFACE_PARTICIPANTMOVEMENTCOMPUTER_H
#define QUALISYSKINOVAINTERFACE_PARTICIPANTMOVEMENTCOMPUTER_H


class ParticipantMovementComputer {
public:
    ParticipantMovementComputer(double initial_hand_position_x_cm);

    double GetMovementLengthCM(double new_hand_position_x_cm);

private:
    double initial_hand_position_x_cm;
    double last_hand_position_x_cm;

};


#endif //QUALISYSKINOVAINTERFACE_PARTICIPANTMOVEMENTCOMPUTER_H
