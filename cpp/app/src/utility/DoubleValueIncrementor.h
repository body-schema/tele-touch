#ifndef QUALISYSKINOVAINTERFACE_DOUBLEVALUEINCREMENTOR_H
#define QUALISYSKINOVAINTERFACE_DOUBLEVALUEINCREMENTOR_H


#include <chrono>
#include <cmath>

class DoubleValueIncrementor {

public:
    DoubleValueIncrementor(double initial_value, double increment_on, double incrementation_freq_hz,
                           bool with_acceleration_on_start = false, bool with_deceleration_on_end = false,
                           double acceleration_duration_sec = 0.0);

    std::pair<double, double> GetValueAndSpeed(std::chrono::high_resolution_clock::time_point t_now = std::chrono::high_resolution_clock::now()); // May have error in speed value during last call before stop.

    void Start();

    void Start(double maximum_value);

    void Stop();

    bool IsStarted() const;

    bool isFinished() const;

    double GetMaximumValue() const;

private:
    double value = 0;
    double increment_on;
    double incrementation_freq_hz;
    bool with_acceleration_on_start;
    bool with_deceleration_on_end;
    double acceleration_duration_sec;
    double value_after_acceleration = 0.0;
    std::chrono::system_clock::time_point last_update;
    std::chrono::system_clock::time_point start_time;
    std::chrono::system_clock::time_point deceleration_start_time;
    bool deceleration_started = false;
    bool is_working = false;
    double maximum_value;
    bool is_finished = false;
};


#endif //QUALISYSKINOVAINTERFACE_DOUBLEVALUEINCREMENTOR_H
