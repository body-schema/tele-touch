#ifndef QUALISYSKINOVAINTERFACE_INTVALUEINCREMENTOR_H
#define QUALISYSKINOVAINTERFACE_INTVALUEINCREMENTOR_H


#include <chrono>

class IntValueIncrementor {

public:
    IntValueIncrementor(int initial_value, int increment_on, double incrementation_freq_hz);

    int GetValue();

    void Start();

    void Stop();

    bool IsStarted();

private:
    int value = 0;
    int initial_value;
    int increment_on;
    double incrementation_freq_hz;

    std::chrono::system_clock::time_point start_time;
    bool is_started;

};


#endif //QUALISYSKINOVAINTERFACE_INTVALUEINCREMENTOR_H
