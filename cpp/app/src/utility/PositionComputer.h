#ifndef QUALISYSKINOVAINTERFACE_POSITIONCOMPUTER_H
#define QUALISYSKINOVAINTERFACE_POSITIONCOMPUTER_H

#include <Eigen/Dense>
#include <chrono>
#include "src/legacy/robot/MovingWindow/MovingWindow.h"

class PositionComputer {
public:
    static inline Eigen::Vector3d generate_linear_trajectory(
            std::chrono::high_resolution_clock::time_point currentTimestamp,
            std::chrono::high_resolution_clock::time_point beginningTimestamp,
            double duration_seconds,
            Eigen::Vector3d startPosition,
            Eigen::Vector3d goalPosition
    ) {
        if (currentTimestamp < beginningTimestamp || duration_seconds <= 0) {
            return startPosition;
        }

        double elapsedTime = std::chrono::duration<double>(currentTimestamp - beginningTimestamp).count();
        double progress = elapsedTime / duration_seconds;

        if (progress > 1) {
            progress = 1;
        }

        Eigen::Vector3d currentPosition = startPosition + progress * (goalPosition - startPosition);

        return currentPosition;
    }

    static inline Eigen::Vector3d compute_intergoal_position(MovingWindow &mw_robot_pos, Eigen::Vector3d &goal_pos,
                                                             std::chrono::high_resolution_clock::time_point &joint_control_timestamp,
                                                             bool &previously_limited_intergoal,
                                                             const Eigen::Vector3d MIN_VECT,
                                                             const Eigen::Vector3d MAX_VECT, double MAX_VEL_NORM,
                                                             const double MAX_VEL_COMPENSATION_MULTIPLIER,
                                                             const double MAX_VEL_DECREASE_MULTIPLIER,
                                                             const double MAX_DIST_RECOVERY_MULTIPLIER,
                                                             const double MOVING_WINDOW_ACTIVE_FROM_SEC,
                                                             const Eigen::Vector3d HAND_AXES,
                                                             const Eigen::Vector3d ROBOT_POS_INIT_R,
                                                             const Eigen::Vector3d HAND_AXES_INVERSE
                                                             ) {
        // All the inputs are in robot frame
        Eigen::Vector3d intergoal_pos = goal_pos;
        const Point &point = mw_robot_pos.points().back();
        Eigen::Vector3d robot_pos = point.first;

        TimePoint now = std::chrono::high_resolution_clock::now();
        double dt = std::chrono::duration<double>(now - joint_control_timestamp).count();

        // max distance from the last frame - first limiting factor
        double max_distance = MAX_VEL_NORM * MAX_VEL_COMPENSATION_MULTIPLIER * dt;

        // calculate average velocity
        const std::deque<Point> &points = mw_robot_pos.points();
        double sum_distance = 0.;
        double sum_time = 0.;
        for (const Point &historical_point: points) {
            double historical_dt = std::chrono::duration<double>(now - historical_point.second).count();
            if (historical_dt >
                MOVING_WINDOW_ACTIVE_FROM_SEC) { // Take samples atleast 20ms old, otherwise, there is too much noise
                sum_distance += (robot_pos - historical_point.first).norm();
                sum_time += historical_dt;
            }
        }
        double average_velocity = 0.;
        if (sum_time != 0.) {
            average_velocity = sum_distance / sum_time;
        }

        double distance = (robot_pos - intergoal_pos).norm();

        // If the change is lower than the max distance travelable in up to 1ms
        if (((previously_limited_intergoal && distance < max_distance * MAX_DIST_RECOVERY_MULTIPLIER) ||
             (!previously_limited_intergoal && distance < max_distance)) && average_velocity < MAX_VEL_NORM) {
            previously_limited_intergoal = false;
            return goal_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
        } else {
            // Limit the distance to the max distance travelable in 1ms
            previously_limited_intergoal = true;
            double distance_multi = max_distance * MAX_VEL_DECREASE_MULTIPLIER;
            intergoal_pos = (
                    (robot_pos + (goal_pos - robot_pos).normalized() * distance_multi).cwiseProduct(HAND_AXES) +
                    ROBOT_POS_INIT_R.cwiseProduct(HAND_AXES_INVERSE)).cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);

            return intergoal_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
        }
    }
};


#endif //QUALISYSKINOVAINTERFACE_POSITIONCOMPUTER_H
