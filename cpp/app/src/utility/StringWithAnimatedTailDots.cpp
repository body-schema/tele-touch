#include <string>
#include "StringWithAnimatedTailDots.h"

StringWithAnimatedTailDots::StringWithAnimatedTailDots(std::string string, double animation_freq_hz,
                                                       bool add_spaces_at_string_start) : string{string},
                                                                                          animation_freq_hz{
                                                                                                  animation_freq_hz},
                                                                                          add_spaces_at_string_start{
                                                                                                  add_spaces_at_string_start} {
    int_value_incrementor = new IntValueIncrementor(0, 1, animation_freq_hz);
}

StringWithAnimatedTailDots::~StringWithAnimatedTailDots() {
    delete int_value_incrementor;
}

void StringWithAnimatedTailDots::StartAnimation() {
    int_value_incrementor->Start();
    is_started = true;
}

void StringWithAnimatedTailDots::StopAnimation() {
    int_value_incrementor->Stop();
    is_started = false;
}

std::string StringWithAnimatedTailDots::GetString() {
    if (is_started) {
        int dots_count = int_value_incrementor->GetValue();
        auto string_with_tail_dots = string;
        if (dots_count > 3) {
            int_value_incrementor->Stop();
            int_value_incrementor->Start();
        } else {
            for (int i = 0; i < dots_count; ++i) {
                if (add_spaces_at_string_start) {
                    string_with_tail_dots.insert(0, " ");
                }
                string_with_tail_dots.append(".");
            }
        }
        return string_with_tail_dots;
    } else {
        return string;
    }
}

bool StringWithAnimatedTailDots::IsAnimationStarted() {
    return is_started;
}

