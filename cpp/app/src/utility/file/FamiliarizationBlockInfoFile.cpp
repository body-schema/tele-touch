#include "FamiliarizationBlockInfoFile.h"

FamiliarizationBlockInfoFile::FamiliarizationBlockInfoFile(const std::string &filename) : HeaderAndValuesFile(filename) {
    headersAndValues = defaultHeadersAndValues;
    loadInfo();
}

void FamiliarizationBlockInfoFile::resetInfo() {
    headersAndValues = defaultHeadersAndValues;
}

void FamiliarizationBlockInfoFile::FillInfo(ParticipantInfo &participantInfo, FamiliarizationSettings &familiarizationSettings) {
    putInfo(FamiliarizationBlockInfoFile::participantHeader, participantInfo.participantCodename);
    putInfo(FamiliarizationBlockInfoFile::ageHeader, participantInfo.age);
    putInfo(FamiliarizationBlockInfoFile::genderHeader, participantInfo.gender);
    putInfo(FamiliarizationBlockInfoFile::lengthOfForearmHeader, participantInfo.lengthOfForearm);
    putInfo(FamiliarizationBlockInfoFile::handednessHeader, participantInfo.handedness);
    putInfo(FamiliarizationBlockInfoFile::sensitivityToTactileTestHeader, participantInfo.sensitivityOfTactileTest);
    putInfo(FamiliarizationBlockInfoFile::orderOfTheBlocksHeader, participantInfo.orderOfTheBlocks);
    putInfo(FamiliarizationBlockInfoFile::description, participantInfo.description);
    putInfo(FamiliarizationBlockInfoFile::speed_lowerbound_cm_s, std::to_string(familiarizationSettings.ideal_speed_lowerbound_cm_s));
    putInfo(FamiliarizationBlockInfoFile::speed_upperbound_cm_s, std::to_string(familiarizationSettings.ideal_speed_upperbound_cm_s));
    putInfo(FamiliarizationBlockInfoFile::reference_line_speed_cm_s, std::to_string(familiarizationSettings.reference_line_speed_cm_s));
    putInfo(FamiliarizationBlockInfoFile::reference_line_speedup_time_s, std::to_string(familiarizationSettings.reference_line_speedup_time_s));
    putInfo(FamiliarizationBlockInfoFile::filtering_rate, std::to_string(familiarizationSettings.smoothing_intencity));
    putInfo(FamiliarizationBlockInfoFile::lines_transparency, std::to_string(familiarizationSettings.is_transparency_enable));
    putInfo(FamiliarizationBlockInfoFile::gray_zone_size_cm, std::to_string(familiarizationSettings.borders_size_cm));
    putInfo(FamiliarizationBlockInfoFile::ideal_trial_number, std::to_string(familiarizationSettings.ideal_trials_number));
    putInfo(FamiliarizationBlockInfoFile::minimum_ref_line_length_cm, std::to_string(familiarizationSettings.reference_line_minimum_length_cm));
    putInfo(FamiliarizationBlockInfoFile::maximum_ref_line_length_cm, std::to_string(familiarizationSettings.reference_line_maximum_length_cm));
    putInfo(FamiliarizationBlockInfoFile::lines_width_cm, std::to_string(familiarizationSettings.line_width_cm));
}
