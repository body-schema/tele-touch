#include "ExperimentBlockInfoFile.h"
#include "lib/date/date.h"

ExperimentBlockInfoFile::ExperimentBlockInfoFile(const std::string &filename) : HeaderAndValuesFile(filename) {
    headersAndValues = defaultHeadersAndValues;
    loadInfo();
}

void ExperimentBlockInfoFile::resetInfo() {
    headersAndValues = defaultHeadersAndValues;
}

void ExperimentBlockInfoFile::FillInfo(const ParticipantInfo &participant_info,
                                       const ExperimentSettings &experiment_settings,
                                       ExperimentBlockType experiment_block_type,
                                       CamerasSettings cameras_settings,
                                        double participant_arm_length_cm,
                                       unsigned int total_frames_number,
                                       int trials_done) {
    putInfo(ExperimentBlockInfoFile::participantHeader, participant_info.participantCodename);
    putInfo(ExperimentBlockInfoFile::ageHeader, participant_info.age);
    putInfo(ExperimentBlockInfoFile::genderHeader, participant_info.gender);
    putInfo(ExperimentBlockInfoFile::lengthOfForearmHeader, participant_info.lengthOfForearm);
    putInfo(ExperimentBlockInfoFile::handednessHeader, participant_info.handedness);
    putInfo(ExperimentBlockInfoFile::sensitivityToTactileTestHeader, participant_info.sensitivityOfTactileTest);
    putInfo(ExperimentBlockInfoFile::orderOfTheBlocksHeader, participant_info.orderOfTheBlocks);
    putInfo(ExperimentBlockInfoFile::description_header, participant_info.description);
    putInfo(ExperimentBlockInfoFile::block_type_header, getBlockTypeStr(experiment_block_type));
    putInfo(ExperimentBlockInfoFile::condition_header, getConditionStr(experiment_block_type));
    putInfo(ExperimentBlockInfoFile::judgement_type_header, getJudgementTypeStr(experiment_block_type));
    putInfo(ExperimentBlockInfoFile::arm_length_cm_header, std::to_string(participant_arm_length_cm));
    putInfo(ExperimentBlockInfoFile::total_trials_header, std::to_string(experiment_settings.max_trial_number));
    putInfo(ExperimentBlockInfoFile::trials_done_header, std::to_string(trials_done));
    putInfo(ExperimentBlockInfoFile::cameras_number_header, std::to_string(cameras_settings.cameras_number));
    putInfo(ExperimentBlockInfoFile::markers_number_header, std::to_string(cameras_settings.markers_number));
    putInfo(ExperimentBlockInfoFile::cameras_frequency_header, std::to_string(cameras_settings.camera_frequency));
    putInfo(ExperimentBlockInfoFile::frames_number_header, std::to_string(total_frames_number));
    putInfo(ExperimentBlockInfoFile::lines_width_cm_header, std::to_string(experiment_settings.lines_width_cm));
    putInfo(ExperimentBlockInfoFile::initial_judgement_line_max_height_header, std::to_string(experiment_settings.initial_judgement_line_max_height));
    putInfo(ExperimentBlockInfoFile::initial_judgement_line_min_height_header, std::to_string(experiment_settings.initial_judgement_line_min_height));
    putInfo(ExperimentBlockInfoFile::judgement_line_increasing_step_header, std::to_string(experiment_settings.judgement_line_increasing_step));
    putInfo(ExperimentBlockInfoFile::block_datetime_header, date::format("%F_%H-%M-%S", std::chrono::system_clock::now()));
}
