#ifndef QUALISYSKINOVAINTERFACE_FAMILIARIZATIONBLOCKINFOFILE_H
#define QUALISYSKINOVAINTERFACE_FAMILIARIZATIONBLOCKINFOFILE_H


#include <string>
#include "HeaderAndValuesFile.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/datastruct/FamiliarizationSettings.h"

class FamiliarizationBlockInfoFile : public HeaderAndValuesFile {

public:
    inline static const std::string participantHeader = "participant_codename";
    inline static const std::string ageHeader = "age";
    inline static const std::string genderHeader = "gender";
    inline static const std::string lengthOfForearmHeader = "length_of_forearm";
    inline static const std::string handednessHeader = "handedness";
    inline static const std::string sensitivityToTactileTestHeader = "sensitivity_to_tactile_test";
    inline static const std::string orderOfTheBlocksHeader = "order_of_the_blocks";
    inline static const std::string description = "description";
    inline static const std::string speed_lowerbound_cm_s = "speed_lowerbound_cm_s";
    inline static const std::string speed_upperbound_cm_s = "speed_upperbound_cm_s";
    inline static const std::string reference_line_speed_cm_s = "reference_line_speed_cm_s";
    inline static const std::string reference_line_speedup_time_s = "reference_line_speedup_time_s";
    inline static const std::string filtering_rate = "filtering_rate";
    inline static const std::string lines_transparency = "lines_transparency";
    inline static const std::string gray_zone_size_cm = "gray_zone_size_cm";
    inline static const std::string ideal_trial_number = "ideal_trials_number";
    inline static const std::string minimum_ref_line_length_cm = "minimum_ref_line_length_cm";
    inline static const std::string maximum_ref_line_length_cm = "maximum_ref_line_length_cm";
    inline static const std::string lines_width_cm = "lines_width_cm";
    inline static const std::vector<std::pair<std::string, std::string>> defaultHeadersAndValues = {
            {participantHeader,              ""},
            {ageHeader,                      ""},
            {genderHeader,                   ""},
            {lengthOfForearmHeader,          ""},
            {handednessHeader,               ""},
            {sensitivityToTactileTestHeader, ""},
            {orderOfTheBlocksHeader,         ""},
            {description,                    ""},
            {speed_lowerbound_cm_s,                    ""},
            {speed_upperbound_cm_s,                    ""},
            {reference_line_speed_cm_s,                    ""},
            {reference_line_speedup_time_s,                    ""},
            {filtering_rate,                    ""},
            {lines_transparency,                    ""},
            {gray_zone_size_cm,                    ""},
            {ideal_trial_number,                    ""},
            {minimum_ref_line_length_cm,                    ""},
            {maximum_ref_line_length_cm,                    ""},
            {lines_width_cm,                    ""}};


    explicit FamiliarizationBlockInfoFile(const std::string &filename);

    void resetInfo() override;

    void FillInfo(ParticipantInfo& participantInfo, FamiliarizationSettings &familiarizationSettings);
};


#endif //QUALISYSKINOVAINTERFACE_FAMILIARIZATIONBLOCKINFOFILE_H
