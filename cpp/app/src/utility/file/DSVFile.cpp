#include "DSVFile.h"

DSVFile::DSVFile(const std::string &filename) : GenericFile(filename) {
    //nothing
}

DSVFile::DSVFile() : GenericFile() {
    //nothing
}

void DSVFile::appendDSVValue(const std::string &value, bool withEndDelimiter, bool newLine) {
    //todo: add the delimiter in the value checker
    if (withEndDelimiter) {
        if (newLine) {
            writeString(value + currentDelimiter + "\n");
        } else {
            writeString(value + currentDelimiter);
        }
    } else {
        if (newLine) {
            writeString(value + "\n");
        } else {
            writeString(value);
        }
    }
}

void DSVFile::changeDelimiter(const std::string &newDelimiter) {
    currentDelimiter = newDelimiter;
}

void DSVFile::appendDSVValuePair(const std::string &firstValue, const std::string &secondValue, bool withEndDelimiter,
                                 bool newLine) {
    appendDSVValue(firstValue, true, newLine);
    appendDSVValue(secondValue, withEndDelimiter, newLine);
}
