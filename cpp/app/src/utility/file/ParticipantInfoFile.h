#ifndef TELETOUCHOUTPUTTEST_PARTICIPANTINFOFILE_H
#define TELETOUCHOUTPUTTEST_PARTICIPANTINFOFILE_H

#include <vector>
#include <map>
#include "GenericFile.h"
#include "string"
#include "HeaderAndValuesFile.h"
#include "src/datastruct/wxParticipantInfo.h"

class ParticipantInfoFile : public HeaderAndValuesFile {

public:
    inline static const std::string participantHeader = "participant_codename";
    inline static const std::string ageHeader = "age";
    inline static const std::string genderHeader = "gender";
    inline static const std::string lengthOfForearmHeader = "length_of_forearm";
    inline static const std::string handednessHeader = "handedness";
    inline static const std::string sensitivityToTactileTestHeader = "sensitivity_to_tactile_test";
    inline static const std::string orderOfTheBlocksHeader = "order_of_the_blocks";
    inline static const std::string description = "description";
    inline static const std::vector<std::pair<std::string, std::string>> defaultHeadersAndValues = {
            {participantHeader,              ""},
            {ageHeader,                      ""},
            {genderHeader,                   ""},
            {lengthOfForearmHeader,          ""},
            {handednessHeader,               ""},
            {sensitivityToTactileTestHeader, ""},
            {orderOfTheBlocksHeader,         ""},
            {description,                    ""}};


    explicit ParticipantInfoFile(const std::string &filename);

    void resetInfo() override;

    void fillParticipantInfoStruct(ParticipantInfo& participantInfo);

    void getParticipantInfoFromStruct(ParticipantInfo& participantInfo);
};


#endif //TELETOUCHOUTPUTTEST_PARTICIPANTINFOFILE_H
