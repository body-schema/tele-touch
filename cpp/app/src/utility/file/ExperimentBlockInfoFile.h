#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTBLOCKINFOFILE_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTBLOCKINFOFILE_H


#include "HeaderAndValuesFile.h"
#include "src/datastruct/ExperimentBlockType.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/datastruct/ExperimentSettings.h"
#include "src/service/CameraService/CameraService.h"

class ExperimentBlockInfoFile : public HeaderAndValuesFile {

public:
    inline static const std::string participantHeader = "participant_codename";
    inline static const std::string ageHeader = "age";
    inline static const std::string genderHeader = "gender";
    inline static const std::string lengthOfForearmHeader = "length_of_forearm";
    inline static const std::string handednessHeader = "handedness";
    inline static const std::string sensitivityToTactileTestHeader = "sensitivity_to_tactile_test";
    inline static const std::string orderOfTheBlocksHeader = "order_of_the_blocks";
    inline static const std::string description_header = "description";
    inline static const std::string block_type_header = "block_type";
    inline static const std::string condition_header = "condition";
    inline static const std::string judgement_type_header = "judgement_type";
    inline static const std::string arm_length_cm_header = "participant_arm_length_cm";
    inline static const std::string total_trials_header = "total_trials";
    inline static const std::string trials_done_header = "trials_done";
    inline static const std::string cameras_number_header = "no_of_cameras";
    inline static const std::string markers_number_header = "no_of_markers";
    inline static const std::string cameras_frequency_header = "cameras_frequency";
    inline static const std::string frames_number_header = "no_of_frames";
    inline static const std::string lines_width_cm_header = "lines_width_cm";
    inline static const std::string initial_judgement_line_max_height_header = "initial_judgement_line_max_height_cm";
    inline static const std::string initial_judgement_line_min_height_header = "initial_judgement_line_min_height_cm";
    inline static const std::string judgement_line_increasing_step_header = "judgement_line_increasing_step_cm";
    inline static const std::string block_datetime_header = "block_end_datetime";
    inline static const std::vector<std::pair<std::string, std::string>> defaultHeadersAndValues = {
            {participantHeader,                        ""},
            {ageHeader,                                ""},
            {genderHeader,                             ""},
            {lengthOfForearmHeader,                    ""},
            {handednessHeader,                         ""},
            {sensitivityToTactileTestHeader,           ""},
            {orderOfTheBlocksHeader,                   ""},
            {description_header,                       ""},
            {block_type_header,                        ""},
            {condition_header,                         ""},
            {judgement_type_header,                    ""},
            {arm_length_cm_header,                     ""},
            {total_trials_header,                      ""},
            {trials_done_header,                       ""},
            {cameras_number_header,                    ""},
            {markers_number_header,                    ""},
            {cameras_frequency_header,                 ""},
            {frames_number_header,                     ""},
            {lines_width_cm_header,                    ""},
            {initial_judgement_line_max_height_header, ""},
            {initial_judgement_line_min_height_header, ""},
            {judgement_line_increasing_step_header,    ""},
            {block_datetime_header,                    ""}};


    explicit ExperimentBlockInfoFile(const std::string &filename);

    void resetInfo() override;

    void FillInfo(const ParticipantInfo &participant_info,
                  const ExperimentSettings &experiment_settings,
                  ExperimentBlockType experiment_block_type,
                  CamerasSettings cameras_settings,
                  double participant_arm_length_cm,
                  unsigned int frames_number,
                  int trials_done);
};

#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTBLOCKINFOFILE_H
