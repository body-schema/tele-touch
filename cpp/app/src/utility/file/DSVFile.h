#ifndef TELETOUCHOUTPUTTEST_DSVFILE_H
#define TELETOUCHOUTPUTTEST_DSVFILE_H

#include "GenericFile.h"

//Delimiter separated file
class DSVFile : public GenericFile {

public:
    DSVFile();

    explicit DSVFile(const std::string &filename);

    void appendDSVValue(const std::string &value, bool withEndDelimiter = true, bool newLine = false);

    void appendDSVValuePair(const std::string &firstValue, const std::string &secondValue, bool withEndDelimiter = true,
                            bool newLine = true);

    void changeDelimiter(const std::string &newDelimiter);

protected:
    std::string currentDelimiter = ",";

};


#endif //TELETOUCHOUTPUTTEST_DSVFILE_H
