#include "GenericFile.h"

GenericFile::GenericFile(const std::string &filename) {
    fileName = filename;
    //creates file if is not exist
    createFile();
}

GenericFile::GenericFile() {
    fileName = "default";
}

void GenericFile::open(std::ios_base::openmode mode) {
    if (isOpen()) {
        close();
    }
    fileStream.open(fileName, mode);
}

void GenericFile::close() {
    fileStream.close();
}

bool GenericFile::isOpen() {
    return fileStream.is_open();
}

void GenericFile::createFile() {
    open(std::fstream::app);
    close();
}

GenericFile::~GenericFile() noexcept {
    if (isOpen()) {
        close();
    }
}

void GenericFile::writeString(const std::string &string) {
    if (isOpen()) {
        fileStream << string;
    }
}

std::string GenericFile::readLine(int lineNumber) {
    open(std::fstream::in);
    std::string line;
    if (isOpen()) {
        for (int i = 0; i < lineNumber; i++) {
            getline(fileStream, line);
        }
        getline(fileStream, line);
        close();
    }
    return line;
}

void GenericFile::writeLine(const std::string &string) {
    if (isOpen()) {
        fileStream << string << "\n";
        //close();
    }
}

bool GenericFile::replaceLineWithText(const std::string &text, const std::string &newLine) {
    if (isOpen()) {
        const std::string tempFileName = this->fileName + "_temp";
        GenericFile tempFile = GenericFile(tempFileName);
        this->open();
        tempFile.open();
        std::string line;
        bool isLineFound = false;
        while (getline(fileStream, line)) {
            if (!isLineFound && line.find(text) != std::string::npos) {
                isLineFound = true;
                tempFile.writeLine(newLine);
            } else {
                tempFile.writeLine(line);
            }
        }
        tempFile.close();
        this->close();
        std::remove(this->fileName.c_str());
        std::rename(tempFileName.c_str(), this->fileName.c_str());
        return true;
    } else {
        return false;
    }
}

GenericFile &GenericFile::operator=(const GenericFile &other) {
    fileName = other.fileName;
    //creates file if is not exist
    createFile();
    return *this;
}

bool GenericFile::getNextLine(std::string &line) {
    if (isOpen()) {
        getline(fileStream, line);
        return !fileStream.eof();
    } else {
        return false;
    }
}

void GenericFile::clear() {
    this->open(std::fstream::out);
    this->close();
}
