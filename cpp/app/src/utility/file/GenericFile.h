#ifndef TELETOUCHOUTPUTTEST_GENERICFILE_H
#define TELETOUCHOUTPUTTEST_GENERICFILE_H

#include "string"
#include <fstream>
#include <cstdio>

class GenericFile {
public:
    std::string fileName;

    GenericFile();

    explicit GenericFile(const std::string &filename);

    GenericFile &operator=(const GenericFile &other);


    ~GenericFile();

    void open(std::ios_base::openmode mode = std::ios_base::in | std::ios_base::out);

    void close();

    void clear();

    bool isOpen();

    void writeString(const std::string &string);

    void writeLine(const std::string &string);

    std::string readLine(int lineNumber); //closes the file if it was opened

    bool getNextLine(std::string &line); //returns false if eof reached

    bool replaceLineWithText(const std::string &text, const std::string &newLine);

protected:
    std::fstream fileStream;

private:
    void createFile();
};

#endif //TELETOUCHOUTPUTTEST_GENERICFILE_H
