#include "HeaderAndValuesFile.h"

HeaderAndValuesFile::HeaderAndValuesFile(const std::string &filename) : DSVFile(filename) {
    //nothing
}

std::pair<std::string, std::string> *HeaderAndValuesFile::findInfoByHeader(const std::string &header) {
    for (auto &pair: headersAndValues) {
        if (pair.first == header) {
            return &pair;
        }
    }
    return nullptr;
}

void HeaderAndValuesFile::putInfo(std::string header, std::string value,
                                  std::vector<std::pair<std::string, std::string>>::const_iterator *position) {
    std::pair<std::string, std::string> *existingPairAndValue = findInfoByHeader(header);
    if (existingPairAndValue != nullptr) {
        existingPairAndValue->second = value;
    } else {
        if (position == nullptr) {
            headersAndValues.emplace_back(header, value);
        } else {
            headersAndValues.emplace(*position, header, value);
        }
    }
}

void HeaderAndValuesFile::saveInfo() {
    open(std::ofstream::out | std::ofstream::trunc);
    if (isOpen()) {
        auto currentIterator = headersAndValues.begin();
        //Write headers
        while (currentIterator != headersAndValues.end()) {
            auto previousIterator = currentIterator;
            currentIterator++;
            appendDSVValue(previousIterator->first, currentIterator != headersAndValues.end(), currentIterator == headersAndValues.end());
        }
        //Write values
        currentIterator = headersAndValues.begin();
        while (currentIterator != headersAndValues.end()) {
            auto previousIterator = currentIterator;
            currentIterator++;
            appendDSVValue(previousIterator->second, currentIterator != headersAndValues.end());
        }
    }
}

void HeaderAndValuesFile::loadInfo() {
    resetInfo();
    open(std::fstream::in);
    std::string headers, values;
    getNextLine(headers);
    getNextLine(values);
    if (!headers.empty() && !values.empty()) {
        size_t currentHeaderEndPos, currentValueEndPos = 0;
        while ((currentHeaderEndPos = headers.find(currentDelimiter)) != std::string::npos &&
               (currentValueEndPos = values.find(currentDelimiter)) != std::string::npos) {
            std::string header = headers.substr(0, currentHeaderEndPos);
            std::string value = values.substr(0, currentValueEndPos);
            putInfo(header, value);
            headers.erase(0, currentHeaderEndPos + currentDelimiter.length());
            values.erase(0, currentValueEndPos + currentDelimiter.length());
        }
        std::string header = headers.substr(0, headers.length());
        std::string value = values.substr(0, values.length());
        putInfo(header, value);
    }
    close();
}

std::vector<std::pair<std::string, std::string>> HeaderAndValuesFile::getInfo() {
    return headersAndValues;
}

void HeaderAndValuesFile::resetInfo() {
    headersAndValues = {};
}

std::string HeaderAndValuesFile::getValue(const std::string &header) {
    return findInfoByHeader(header)->second;
}
