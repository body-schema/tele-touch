#ifndef TELETOUCHOUTPUTTEST_HEADERANDVALUESFILE_H
#define TELETOUCHOUTPUTTEST_HEADERANDVALUESFILE_H

#include <vector>
#include "DSVFile.h"

/*File with the following structure:    Header1 <delimiter> Header2 <delimiter>...
                                        Value1  <delimiter> Value2  <delimiter>...*/
class HeaderAndValuesFile : public DSVFile {

public:

    explicit HeaderAndValuesFile(const std::string &filename);

    void putInfo(std::string header, std::string value,
                 std::vector<std::pair<std::string, std::string>>::const_iterator *position = nullptr); //if position not provided, puts to the end of vector

    void saveInfo(); //to a file

    void loadInfo(); //from a file

    virtual void resetInfo();

    std::vector<std::pair<std::string, std::string>> getInfo(); //returns copy of the current info

    std::string getValue(const std::string &header);

protected:
    std::vector<std::pair<std::string, std::string>> headersAndValues = {};

private:
    std::pair<std::string, std::string> *findInfoByHeader(const std::string &header); // returns nullptr if no exist
};


#endif //TELETOUCHOUTPUTTEST_HEADERANDVALUESFILE_H
