#include "ParticipantInfoFile.h"

ParticipantInfoFile::ParticipantInfoFile(const std::string &filename) : HeaderAndValuesFile(filename) {
    headersAndValues = defaultHeadersAndValues;
    loadInfo();
}

void ParticipantInfoFile::resetInfo() {
    headersAndValues = defaultHeadersAndValues;
}

void ParticipantInfoFile::fillParticipantInfoStruct(ParticipantInfo &participantInfo) {
    participantInfo.participantCodename = getValue(ParticipantInfoFile::participantHeader);
    participantInfo.age = getValue(ParticipantInfoFile::ageHeader);
    participantInfo.gender = getValue(ParticipantInfoFile::genderHeader);
    participantInfo.lengthOfForearm = getValue(ParticipantInfoFile::lengthOfForearmHeader);
    participantInfo.handedness = getValue(ParticipantInfoFile::handednessHeader);
    participantInfo.sensitivityOfTactileTest = getValue(ParticipantInfoFile::sensitivityToTactileTestHeader);
    participantInfo.orderOfTheBlocks = getValue(ParticipantInfoFile::orderOfTheBlocksHeader);
    participantInfo.description = getValue(ParticipantInfoFile::description);
}

void ParticipantInfoFile::getParticipantInfoFromStruct(ParticipantInfo &participantInfo) {
    putInfo(ParticipantInfoFile::participantHeader, participantInfo.participantCodename);
    putInfo(ParticipantInfoFile::ageHeader, participantInfo.age);
    putInfo(ParticipantInfoFile::genderHeader, participantInfo.gender);
    putInfo(ParticipantInfoFile::lengthOfForearmHeader, participantInfo.lengthOfForearm);
    putInfo(ParticipantInfoFile::handednessHeader, participantInfo.handedness);
    putInfo(ParticipantInfoFile::sensitivityToTactileTestHeader, participantInfo.sensitivityOfTactileTest);
    putInfo(ParticipantInfoFile::orderOfTheBlocksHeader, participantInfo.orderOfTheBlocks);
    putInfo(ParticipantInfoFile::description, participantInfo.description);
}
