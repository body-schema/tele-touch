#include <spdlog/spdlog.h>
#include "DoubleValueIncrementor.h"

// Modified version of IntValueIncrementor with acceleration/deceleration features
DoubleValueIncrementor::DoubleValueIncrementor(double initial_value, double increment_on, double incrementation_freq_hz,
                                               bool with_acceleration_on_start, bool with_deceleration_on_end,
                                               double acceleration_duration_sec)
        : value{
        initial_value}, increment_on{increment_on}, incrementation_freq_hz{incrementation_freq_hz},
          with_acceleration_on_start{with_acceleration_on_start}, with_deceleration_on_end{with_deceleration_on_end},
          acceleration_duration_sec{acceleration_duration_sec} {

}

std::pair<double, double> DoubleValueIncrementor::GetValueAndSpeed(std::chrono::high_resolution_clock::time_point t_now) {
    if (is_working && value >= maximum_value) {
        Stop();
        return {value, 0.0};
    } else if (is_working) {
        double incrementation_speed = 0.0;
        t_now = std::chrono::high_resolution_clock::now(); // nanoseconds by default
        double sec_from_incrementor_start =
                (double) (t_now - start_time).count() / 1000000000.0; // second with fraction part
        double second_since_last_update = (double) (t_now - last_update).count() / 1000000000.0;
        if (with_acceleration_on_start && sec_from_incrementor_start < acceleration_duration_sec) {
            incrementation_speed =
                    increment_on * sqrt(1 - pow((sec_from_incrementor_start - acceleration_duration_sec) /
                                                acceleration_duration_sec, 2)); // Ellipse-like function to calculate acceleration
        } else if (with_deceleration_on_end &&
                   value >= maximum_value - value_after_acceleration) {
            if (!deceleration_started) {
                deceleration_started = true;
                deceleration_start_time = last_update;
                t_now = std::chrono::high_resolution_clock::now(); // tod avoid negative numbers during the speed computation
            }
            double time_since_fam_start_decelerate_sec = (double) (
                    t_now - deceleration_start_time).count() / 1000000000.0;
            incrementation_speed =
                    increment_on * sqrt(1.0 - pow(time_since_fam_start_decelerate_sec/acceleration_duration_sec, 2));
        } else {
            if (value_after_acceleration == 0.0) {
                value_after_acceleration = value;
            }
            incrementation_speed = incrementation_freq_hz * increment_on;
        }
        if (std::isnan(incrementation_speed)) {
            incrementation_speed = 0.0;
        }
        value += incrementation_speed * second_since_last_update;
        last_update = std::chrono::high_resolution_clock::now();
        if (value > maximum_value) {
            value = maximum_value;
            Stop();
        }
        return {value, incrementation_speed};
    } else {
        return {value, 0.0};
    }
}

void DoubleValueIncrementor::Start() {
    if (!is_working) {
        with_deceleration_on_end = false;
        is_working = true;
        is_finished = false;
        start_time = std::chrono::high_resolution_clock::now();
        last_update = std::chrono::high_resolution_clock::now();
    }
}

void DoubleValueIncrementor::Start(double maximum_value) {
    if (!is_working) {
        this->maximum_value = maximum_value;
        is_working = true;
        is_finished = false;
        deceleration_started = false;
        start_time = std::chrono::high_resolution_clock::now();
        last_update = std::chrono::high_resolution_clock::now();
    }
}

void DoubleValueIncrementor::Stop() {
    is_working = false;
    is_finished = true;
}

bool DoubleValueIncrementor::IsStarted() const {
    return is_working;
}

bool DoubleValueIncrementor::isFinished() const {
    return is_finished;
}

double DoubleValueIncrementor::GetMaximumValue() const {
    return maximum_value;
}
