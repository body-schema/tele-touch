#ifndef QUALISYSKINOVAINTERFACE_PRIMITIVESMOOTHDATASET_H
#define QUALISYSKINOVAINTERFACE_PRIMITIVESMOOTHDATASET_H


#include <vector>

class PrimitiveSmoothDataset {

public:
    PrimitiveSmoothDataset(int smooth_factor);
    void AddValue(double value);
    std::vector<double>& GetDataset();
    double GetLastElement();
    void Clear();

private:
    int smooth_counter = 0;
    int smooth_factor;
    double temp_dataset_value = 0.0;
    std::vector<double> dataset;
};


#endif //QUALISYSKINOVAINTERFACE_PRIMITIVESMOOTHDATASET_H
