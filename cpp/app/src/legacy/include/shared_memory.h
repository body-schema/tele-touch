#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#pragma once


#include <Eigen/Dense>
#include <chrono>
#include <list>

enum SCENE_TYPE {
    SCENE_INSTRUCTION,
    SCENE_PUSH_TO_START,
    SCENE_321,
    SCENE_JUDGEMENT,
    SCENE_SAVING_INFORMATION,
    SCENE_CAPTURE_STARTING,
    SCENE_FAMILIARIZATION_EVALUATION,
    NONE
};

enum VARIANT_TYPE {
    VARIANT_FAMILIARIZATION,
    VARIANT_TOUCH,
    VARIANT_MOVEMENT
};

enum TRAJECTORY_TYPE {
    TRAJECTORY_RIGHT,
    TRAJECTORY_HOME_RIGHT,
    TRAJECTORY_HOME_RANDOM,
    FOLLOW_THE_HAND,
    FOLLOW_THE_HAND_2D
};

enum UI_EVENT_TYPE {
    SAVING_INFORMATION_SCENE_STARTED,
    FAMILIARIZATION_EVALUATION_SCENE_STARTED,
    CAPTURE_STARTING_SCENE_STARTED,
    UI_EVENT_UNDEFINED
};

struct SharedDataRobot {
    Eigen::Matrix<double,7,1> joint_angles = Eigen::Matrix<double,7,1>::Zero();
    Eigen::Matrix<double,7,1> angular_velocities = Eigen::Matrix<double,7,1>::Zero();
    std::chrono::high_resolution_clock::time_point joint_measurement_timestamp;
    Eigen::Matrix<double,7,1> goal_joint_angles = Eigen::Matrix<double,7,1>::Zero();
    std::chrono::high_resolution_clock::time_point goal_computed_timestamp;
    std::chrono::high_resolution_clock::time_point joint_control_timestamp;
};

struct SharedDataUi {
    std::string participant_id;
    bool event_trigger = false;
    std::list<std::pair<std::string, UI_EVENT_TYPE>> event_list;

    char control_input = 0;
    double ratio = 1.;
    
    SCENE_TYPE scene = SCENE_CAPTURE_STARTING; // Default scene
    std::chrono::high_resolution_clock::time_point scene_change_timestamp;
    VARIANT_TYPE variant = VARIANT_FAMILIARIZATION; // Default variant
    std::chrono::high_resolution_clock::time_point variant_change_timestamp;

    int trial = 1;
    int total_trials = 1;
    int ideal_trials = 0;
    int minimum_ideal_trials_required = 0;

    double distance_cm = 1.;

    int maxFamiliarizationLineHeight = 0;
    double max_distance_cm = 21.; // Todo: use information from the config file by default
};

#endif // SHARED_MEMORY_H