#include "MovingWindow.h"

void MovingWindow::clear_points() {
    TimePoint now = std::chrono::high_resolution_clock::now();
    while (!points_.empty() && (now - points_.front().second) > std::chrono::milliseconds(decay_ms_)) {
        points_.pop_front();
    }
}

void MovingWindow::push_back(const Eigen::Vector3d &point, const TimePoint &time) {
    if (points_.empty() || points_.back().second != time) {
        points_.emplace_back(point, time);
        while (!points_.empty() && points_.size() > window_size_) {
            points_.pop_front();
        }
    }
}

const std::deque<Point> &MovingWindow::points() {
    clear_points();
    return points_;
}

const Point &MovingWindow::front() {
    clear_points();
    return points_.front();
}

const Point &MovingWindow::back() {
    clear_points();
    return points_.back();
}