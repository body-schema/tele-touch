#ifndef QUALISYSKINOVAINTERFACE_MOVINGWINDOW_H
#define QUALISYSKINOVAINTERFACE_MOVINGWINDOW_H

#include <Eigen/Core>
#include <deque>
#include "json.h"

using json = nlohmann::json;
using TimePoint = std::chrono::high_resolution_clock::time_point;
using Point = std::pair<Eigen::Vector3d, TimePoint>;

class MovingWindow {
private:
    size_t window_size_;
    int decay_ms_;
    std::deque<Point> points_;

    void clear_points();

public:
    MovingWindow(size_t window_size, int decay_ms) : window_size_(window_size), decay_ms_(decay_ms) {}

    void push_back(const Eigen::Vector3d& point, const TimePoint& time);

    const std::deque<Point>& points();

    const Point& front();

    const Point& back();
};


#endif //QUALISYSKINOVAINTERFACE_MOVINGWINDOW_H
