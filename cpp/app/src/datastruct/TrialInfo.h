#ifndef QUALISYSKINOVAINTERFACE_TRIALINFO_H
#define QUALISYSKINOVAINTERFACE_TRIALINFO_H

#include <chrono>
#include <Eigen/Core>

struct TrialInfo {
    double gain_value = 1.0;
    std::chrono::high_resolution_clock::time_point trial_start_datetime;
    std::chrono::high_resolution_clock::time_point robot_participant_sync_datetime;
    unsigned int robot_participant_sync_frame;
    std::chrono::high_resolution_clock::time_point judgement_start_datetime;
    unsigned int judgement_start_frame;
    Eigen::Vector3d trial_robot_initial_coordinates;
    Eigen::Vector3d trial_robot_final_coordinates;
    Eigen::Vector3d trial_MW_begin_coordinates;
    Eigen::Vector3d trial_MW_end_coordinates;
};

#endif //QUALISYSKINOVAINTERFACE_TRIALINFO_H
