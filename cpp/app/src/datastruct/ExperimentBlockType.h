#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTBLOCKTYPE_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTBLOCKTYPE_H
#pragma once

#include <string>

enum ExperimentBlockType {
    AM,
    AT,
    PM,
    PT,
    FAMILIARIZATION,
    TESTING_2D
};

inline std::string getBlockTypeStr(ExperimentBlockType experimentBlockType) {
    switch (experimentBlockType) {
        case AM:
            return "AM";
        case AT:
            return "AT";
        case PM:
            return "PM";
        case PT:
            return "PT";
        case FAMILIARIZATION:
            return "FAMILIARIZATION";
        case TESTING_2D:
            return "2D_TESTING";
    }
    return "Undefined";
}

inline std::string getJudgementTypeStr(ExperimentBlockType experimentBlockType) {
    std::string blockTypeStr = getBlockTypeStr(experimentBlockType);
    if (blockTypeStr == "AM" || blockTypeStr == "PM") {
        return "Movement";
    } else if (blockTypeStr == "AT" || blockTypeStr == "PT") {
        return "Touch";
    }
    return "Undefined";
}

inline std::string getConditionStr(ExperimentBlockType experimentBlockType) {
    std::string blockTypeStr = getBlockTypeStr(experimentBlockType);
    if (blockTypeStr == "AM" || blockTypeStr == "AT") {
        return "Active";
    } else if (blockTypeStr == "PM" || blockTypeStr == "PT") {
        return "Passive";
    }
    return "Undefined";
}

inline int getBlockOrderNumber(ExperimentBlockType experimentBlockType, const std::string &orderOfTheBlocks) {
    std::string currentOrderOfTheBlock;
    if (orderOfTheBlocks.empty()) {
        return -1;
    }
    std::copy(orderOfTheBlocks.begin(), orderOfTheBlocks.end(), std::back_inserter(currentOrderOfTheBlock));
    std::string blockTypeStr = getBlockTypeStr(experimentBlockType);
    std::string temp_string;
    int blockOrderNumber = 1;
    while (blockOrderNumber < 5) {
        temp_string = currentOrderOfTheBlock.substr(0, 2);
        if (temp_string == blockTypeStr) {
            return blockOrderNumber;
        }
        currentOrderOfTheBlock = currentOrderOfTheBlock.erase(0, 2);
        if (temp_string != "->") {
            blockOrderNumber++;
        }
        temp_string = "";
    }
    return -1;
}

// AM returns by default
inline ExperimentBlockType toBlockTypeEnum(std::string blockTypeStr) {
    if (blockTypeStr == "AM") {
        return AM;
    } else if (blockTypeStr == "AT") {
        return AT;
    } else if (blockTypeStr == "PM") {
        return PM;
    } else if (blockTypeStr == "PT") {
        return PT;
    } else if (blockTypeStr == "FAMILIARIZATION") {
        return FAMILIARIZATION;
    } else if (blockTypeStr == "TESTING_2D") {
        return TESTING_2D;
    }
    return AM;
}

#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTBLOCKTYPE_H
