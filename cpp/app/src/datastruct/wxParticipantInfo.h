#ifndef QUALISYSKINOVAINTERFACE_WXPARTICIPANTINFO_H
#define QUALISYSKINOVAINTERFACE_WXPARTICIPANTINFO_H

#include <wx/string.h>
#include "ParticipantInfo.h"

struct wxParticipantInfo {
    wxString participantCodename = "";
    wxString age = "";
    wxString gender = "";
    wxString lengthOfForearm = "";
    wxString handedness = "";
    wxString sensitivityOfTactileTest = "";
    wxString orderOfTheBlocks = "";
    wxString description = "";
};

static inline ParticipantInfo wxPartInfo_to_PartInfo(const wxParticipantInfo &wxParticipantInfo) {
    ParticipantInfo tmp_participant_info;
    tmp_participant_info.participantCodename = wxParticipantInfo.participantCodename;
    tmp_participant_info.age = wxParticipantInfo.age;
    tmp_participant_info.description = wxParticipantInfo.description;
    tmp_participant_info.gender = wxParticipantInfo.gender;
    tmp_participant_info.handedness = wxParticipantInfo.handedness;
    tmp_participant_info.lengthOfForearm = wxParticipantInfo.lengthOfForearm;
    tmp_participant_info.orderOfTheBlocks = wxParticipantInfo.orderOfTheBlocks;
    tmp_participant_info.sensitivityOfTactileTest = wxParticipantInfo.sensitivityOfTactileTest;
    return tmp_participant_info;
}

static inline wxParticipantInfo PartInfo_to_wxPartInfo(const ParticipantInfo &participantInfo) {
    wxParticipantInfo wxParticipantInfo;
    wxParticipantInfo.participantCodename = participantInfo.participantCodename;
    wxParticipantInfo.age = participantInfo.age;
    wxParticipantInfo.description = participantInfo.description;
    wxParticipantInfo.gender = participantInfo.gender;
    wxParticipantInfo.handedness = participantInfo.handedness;
    wxParticipantInfo.lengthOfForearm = participantInfo.lengthOfForearm;
    wxParticipantInfo.orderOfTheBlocks = participantInfo.orderOfTheBlocks;
    wxParticipantInfo.sensitivityOfTactileTest = participantInfo.sensitivityOfTactileTest;
    return wxParticipantInfo;
}

#endif //QUALISYSKINOVAINTERFACE_WXPARTICIPANTINFO_H
