#ifndef QUALISYSKINOVAINTERFACE_BEHAVIORALINFORMATION_H
#define QUALISYSKINOVAINTERFACE_BEHAVIORALINFORMATION_H

#include "ExperimentBlockType.h"
#include "src/datastruct/wxParticipantInfo.h"
#include "src/utility/file/DSVFile.h"
#include <Eigen/Dense>
#include <string>

struct BehavioralInformation {
    wxParticipantInfo participantInfo;
    int experimentBlockOrderNumber;
    ExperimentBlockType experimentBlockType;
    std::string measuredArmLength = "-";
    int totalTrials = -1;
    int camerasNumber = -1; // todo:: always equals to zero BUG
    int markersNumber = -1;
    bool hasCameraInfo = false;
    int motionCaptureSysFrequency = -1;
    unsigned int frameNumbers = 0;
    std::string dataIncluded = "3D";
    std::string lastCaptureStartDateTime;
    std::string lastTrialStartDateTime;
    unsigned int lastTrialStartFrame;
    std::string lastRobotParticipantSyncDateTime;
    Eigen::Vector3d lastTrialRobotInitialCoordinates;
    Eigen::Vector3d lastTrialRobotFinalCoordinates;
    Eigen::Vector3d lastTrialMWBeginCoordinates;
    Eigen::Vector3d lastTrialMWEndCoordinates;
    std::string lastJudgementStartDateTime;
    unsigned int lastJudgementStartFrame;
    std::string lastTrialEndDateTime;
    unsigned int lastTrialEndFrame;
    DSVFile tempTrialInfoFile;
}; // Todo: move to shared data

inline std::string convertVector3dToString(Eigen::Vector3d vector3D) {
    return std::to_string(vector3D.x()) + ";" + std::to_string(vector3D.y()) + ";" + std::to_string(vector3D.z());
}

#endif //QUALISYSKINOVAINTERFACE_BEHAVIORALINFORMATION_H
