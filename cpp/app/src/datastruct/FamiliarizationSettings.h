#ifndef QUALISYSKINOVAINTERFACE_FAMILIARIZATIONSETTINGS_H
#define QUALISYSKINOVAINTERFACE_FAMILIARIZATIONSETTINGS_H

#include "ExperimentBlockType.h"

struct FamiliarizationSettings {
    double ideal_speed_lowerbound_cm_s;
    double ideal_speed_upperbound_cm_s;
    double reference_line_speed_cm_s;
    double reference_line_speedup_time_s;
    int smoothing_intencity;
    bool is_transparency_enable;
    double borders_size_cm;
    int ideal_trials_number;
    double reference_line_minimum_length_cm;
    double reference_line_maximum_length_cm;
    double line_width_cm;
};

#endif //QUALISYSKINOVAINTERFACE_FAMILIARIZATIONSETTINGS_H
