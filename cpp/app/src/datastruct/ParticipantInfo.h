#ifndef QUALISYSKINOVAINTERFACE_PARTICIPANTINFO_H
#define QUALISYSKINOVAINTERFACE_PARTICIPANTINFO_H

#include <string>

struct ParticipantInfo {
    std::string participantCodename = "";
    std::string age;
    std::string gender = "";
    std::string lengthOfForearm = "";
    std::string handedness = "";
    std::string sensitivityOfTactileTest = "";
    std::string orderOfTheBlocks = "";
    std::string description = "";
};

#endif //QUALISYSKINOVAINTERFACE_PARTICIPANTINFO_H
