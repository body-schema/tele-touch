#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTSETTINGS_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTSETTINGS_H

struct ExperimentSettings {
    double lines_width_cm;
    int max_trial_number;
    double judgement_line_increasing_step;
    double initial_judgement_line_max_height;
    double initial_judgement_line_min_height;
};

#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTSETTINGS_H
