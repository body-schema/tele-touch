#ifndef TELE_TOUCH_FAMILIARIZATIONVIEW_H
#define TELE_TOUCH_FAMILIARIZATIONVIEW_H

#include <list>
#include "src/ui/wx/wxutility/rectangle/ScreenRectangle.h"
#include "src/ui/wx/wxutility/TextStyle.h"

class FamiliarizationView {

public:
    virtual ~FamiliarizationView() {}

    virtual void DrawPNGImage(const std::string &file) = 0;

    virtual void DrawFamiliarizationActiveScene(const Rectangle &participant_rectangle,
                                                const Rectangle &reference_rectangle,
                                                const Rectangle &upper_border_rectangle,
                                                const Rectangle &lower_border_rectangle,
                                                double ref_line_max_length_cm,
                                                const TextStyle &text_style) = 0;

    virtual void DrawFamiliarizationEvaluationScene(const Rectangle &participant_rectangle,
                                                    const Rectangle &upper_border_rectangle,
                                                    const Rectangle &lower_border_rectangle,
                                                    double ref_line_max_length_cm,
                                                    double speed_lowerbound,
                                                    double speed_upperbound,
                                                    std::vector<double> reference_line_recorded_speed,
                                                    std::vector<double> participant_line_recorded_speed, double reference_line_speed,
                                                    std::vector<std::pair<const std::string, const TextStyle>> &evaluation_feedback,
                                                    std::pair<const std::string, const TextStyle> trial_information) = 0;

    virtual void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) = 0;

};

#endif //TELE_TOUCH_FAMILIARIZATIONVIEW_H
