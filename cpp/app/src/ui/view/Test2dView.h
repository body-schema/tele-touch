#ifndef QUALISYSKINOVAINTERFACE_TEST2DVIEW_H
#define QUALISYSKINOVAINTERFACE_TEST2DVIEW_H

#include "src/ui/wx/wxutility/TextStyle.h"

class Test2dView {
public:
    virtual ~Test2dView() {}

    virtual void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) = 0;

};

#endif //QUALISYSKINOVAINTERFACE_TEST2DVIEW_H
