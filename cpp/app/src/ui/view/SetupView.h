#ifndef QUALISYSKINOVAINTERFACE_SETUPVIEW_H
#define QUALISYSKINOVAINTERFACE_SETUPVIEW_H

#include "src/datastruct/wxParticipantInfo.h"
#include "src/datastruct/FamiliarizationSettings.h"
#include "src/datastruct/ExperimentSettings.h"

class SetupView {
public:
    virtual ~SetupView() {}

    virtual void OpenCreateParticipantDialog(bool load_participant) = 0;

    virtual void OpenEditParticipantDialog(ParticipantInfo participant_info) = 0;

    virtual void DisplayUserInfo(ParticipantInfo &participantInfo) = 0;

    virtual void OpenParticipantOpenDialog() = 0;

    virtual void OpenExperimentView(ParticipantInfo participant_info, ExperimentSettings *dynamicExperimentSettings, ExperimentBlockType experimentBlockType) = 0;

    virtual void OpenFamiliarizationView(ParticipantInfo participant_info, FamiliarizationSettings *dynamicFamiliarizationSettings) = 0;

    virtual void DisplayFamiliarizationSettings() = 0;

    virtual void DisplayActiveMovementSettings() = 0;

    virtual void DisplayActiveTouchSettings() = 0;

    virtual void DisplayPassiveMovementSettings() = 0;

    virtual void DisplayPassiveTouchSettings() = 0;

    virtual void Display2DTestingSettings() = 0;

    virtual void Open2dTestView() = 0;

    virtual FamiliarizationSettings *GetFamiliarizationSettings() = 0;

    virtual ExperimentSettings *GetExperimentSettings() = 0;
};


#endif //QUALISYSKINOVAINTERFACE_SETUPVIEW_H
