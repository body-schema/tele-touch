#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTVIEW_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTVIEW_H


#include "src/ui/wx/wxutility/rectangle/Rectangle.h"
#include "src/ui/wx/wxutility/TextStyle.h"

class ExperimentView {

public:
    virtual ~ExperimentView() {}

    virtual void DrawExperimentActiveScene() = 0;

    virtual void DrawExperimentEvaluationScene(const Rectangle &participant_judgement_rectangle, bool on_left_screen_side = true) = 0;

    virtual void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) = 0;

    virtual void DrawDownRightCornerText(const std::string &text_line, const TextStyle &text_style) = 0;
};

#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTVIEW_H
