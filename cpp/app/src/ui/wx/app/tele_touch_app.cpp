#include "tele_touch_app.h"

#include "src/service/SingletonServiceProvider.h"
#include "src/utility/ScreenSettings.h"

bool TeleTouchApp::OnInit() {
    app_screen_settings = new ScreenSettings("../config/settings.ini");
    initialFrame = new SetupFrame();
    initialFrame->Show(true);
    return true;
}

int TeleTouchApp::OnExit() {
    delete app_screen_settings;
    SingletonServiceProvider::RemoveAllServices();
    return wxAppBase::OnExit();
}


