#ifndef QUALISYSKINOVAINTERFACE_TELE_TOUCH_APP_H
#define QUALISYSKINOVAINTERFACE_TELE_TOUCH_APP_H

#include <wx/wx.h>
#include "src/ui/wx/frame/setup/SetupFrame.h"

class TeleTouchApp : public wxApp {
public:
    bool OnInit() override;

    int OnExit() override;

    SetupFrame *initialFrame;
};


#endif //QUALISYSKINOVAINTERFACE_TELE_TOUCH_APP_H
