#ifndef QUALISYSKINOVAINTERFACE_TEST2DPANEL_H
#define QUALISYSKINOVAINTERFACE_TEST2DPANEL_H

#include <wx/wx.h>
#include <vector>
#include <string>
#include "src/ui/wx/wxutility/TextStyle.h"
#include "src/ui/wx/wxutility/ColorConverter.h"
#include "src/ui/wx/drawer/CustomDrawer.h"


class Test2dPanel : public wxPanel  {

public:
    Test2dPanel(wxFrame *parent);

    void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style);

};


#endif //QUALISYSKINOVAINTERFACE_TEST2DPANEL_H
