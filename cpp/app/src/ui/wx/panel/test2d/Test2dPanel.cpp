#include "Test2dPanel.h"

Test2dPanel::Test2dPanel(wxFrame *parent) : wxPanel(parent) {

}

void Test2dPanel::DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) {
    wxPaintDC dc(this);
    auto custom_drawer = CustomDrawer(dc);
    custom_drawer.DrawCenteredText(text_lines, RGBA_to_wxColor(text_style.GetColour()), text_style.GetHeightPx());
}

