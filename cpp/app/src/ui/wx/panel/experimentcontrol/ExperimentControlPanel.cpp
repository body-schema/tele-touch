#include "ExperimentControlPanel.h"

#include <utility>

ExperimentControlPanel::ExperimentControlPanel(wxWindow *parent, std::function<void(std::string new_block_type)> onBlockTypeChange) : wxPanel(parent,
                                                                                                wxID_ANY,
                                                                                                wxDefaultPosition,
                                                                                                wxDefaultSize,
                                                                                                wxTAB_TRAVERSAL |
                                                                                                wxNO_BORDER,
                                                                                                wxPanelNameStr), onBlockTypeChange(std::move(onBlockTypeChange)) {
    app_settings_box->SetFont(this->GetFont().Scale(1.5));

    block_type_input = new wxChoice(this, ID_BLOCK_TYPE_INPUT, wxDefaultPosition, wxDefaultSize, 0, {}, 0);
    block_type_input->Append("AM");
    block_type_input->Append("AT");
    block_type_input->Append("PM");
    block_type_input->Append("PT");
    block_type_input->Append("FAMILIARIZATION");
    block_type_input->Append("TESTING_2D");
    block_type_input->SetSelection(0);

    speed_lowerbound_input = new wxTextCtrl(this, wxID_ANY, "1.5", wxDefaultPosition, wxDefaultSize, 0,
                                            wxTextValidator(wxFILTER_NUMERIC,
                                                            &familiarization_settings.ideal_speed_lowerbound_cm_s)); // Todo:: move default values to the presenter?
    speed_upperbound_input = new wxTextCtrl(this, wxID_ANY, "4.5", wxDefaultPosition, wxDefaultSize, 0,
                                            wxTextValidator(wxFILTER_NUMERIC,
                                                            &familiarization_settings.ideal_speed_upperbound_cm_s));
    reference_line_speed_input = new wxTextCtrl(this, wxID_ANY, "3", wxDefaultPosition, wxDefaultSize, 0,
                                                wxTextValidator(wxFILTER_NUMERIC,
                                                                &familiarization_settings.reference_line_speed_cm_s));
    reference_line_speedup_time_input = new wxTextCtrl(this, wxID_ANY, "1", wxDefaultPosition, wxDefaultSize, 0,
                                                       wxTextValidator(wxFILTER_NUMERIC, &familiarization_settings.reference_line_speedup_time_s));
    smoothing_filter_rate_input = new wxTextCtrl(this, wxID_ANY, "10", wxDefaultPosition, wxDefaultSize, 0,
                                                 wxTextValidator(wxFILTER_DIGITS,
                                                               &familiarization_settings.smoothing_intencity));
    borders_size_input = new wxTextCtrl(this, wxID_ANY, "1.5", wxDefaultPosition, wxDefaultSize, 0,
                                        wxTextValidator(wxFILTER_NUMERIC, &familiarization_settings.borders_size_cm));
    ideal_trials_number_input = new wxTextCtrl(this, wxID_ANY, "10", wxDefaultPosition, wxDefaultSize, 0,
                                               wxTextValidator(wxFILTER_DIGITS,
                                                               &familiarization_settings.ideal_trials_number));
    reference_line_minimum_length_input = new wxTextCtrl(this, wxID_ANY, "10", wxDefaultPosition, wxDefaultSize, 0,
                                                         wxTextValidator(wxFILTER_NUMERIC,
                                                               &familiarization_settings.minimum_ref_line_length_cm));
    reference_line_maximum_length_input = new wxTextCtrl(this, wxID_ANY, "25", wxDefaultPosition, wxDefaultSize, 0,
                                                         wxTextValidator(wxFILTER_NUMERIC,
                                                                         &familiarization_settings.maximum_ref_line_length_cm));
    line_width_input = new wxTextCtrl(this, wxID_ANY, "2", wxDefaultPosition, wxDefaultSize, 0,
                                      wxTextValidator(wxFILTER_NUMERIC,
                                                      &familiarization_settings.line_width_cm));

    experiment_line_width_input = new wxTextCtrl(this, wxID_ANY, "2", wxDefaultPosition, wxDefaultSize, 0,
                                                 wxTextValidator(wxFILTER_NUMERIC,
                                                                 &experiment_settings.lines_width_cm));
    experiment_max_trial_number_input = new wxTextCtrl(this, wxID_ANY, "10", wxDefaultPosition, wxDefaultSize, 0,
                                                 wxTextValidator(wxFILTER_DIGITS,
                                                                 &experiment_settings.max_trial_number));
    experiment_judgement_line_increasing_step_input = new wxTextCtrl(this, wxID_ANY, "0.325", wxDefaultPosition, wxDefaultSize, 0,
                                                       wxTextValidator(wxFILTER_NUMERIC,
                                                                       &experiment_settings.judgement_line_increasing_step));
    experiment_initial_judgement_line_max_height_input = new wxTextCtrl(this, wxID_ANY, "25", wxDefaultPosition, wxDefaultSize, 0,
                                                       wxTextValidator(wxFILTER_NUMERIC,
                                                                       &experiment_settings.initial_judgement_line_max_height));
    experiment_initial_judgement_line_min_height_input = new wxTextCtrl(this, wxID_ANY, "5", wxDefaultPosition, wxDefaultSize, 0,
                                                       wxTextValidator(wxFILTER_NUMERIC,
                                                                       &experiment_settings.initial_judgement_line_min_height));
    is_smoothing_enable->SetValue(true);

    InitializeSizers();
    DisplayActiveMovementSettings(); // Default settings group
    InitializeEventHandlers();
};

void ExperimentControlPanel::InitializeSizers() {
    auto *mainSizer = new wxStaticBoxSizer(app_settings_box, wxVERTICAL);
    mainSizer->Add(block_type_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    mainSizer->Add(block_type_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer = new wxBoxSizer(wxVERTICAL);

    familiarizationSettingsSizer->Add(speed_lowerbound_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(speed_lowerbound_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(speed_upperbound_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(speed_upperbound_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(reference_line_speed_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(reference_line_speed_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(reference_line_speedup_time_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(reference_line_speedup_time_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(is_smoothing_enable, 0, wxEXPAND | wxTOP | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(smoothing_filter_rate_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(smoothing_filter_rate_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(borders_size_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(borders_size_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(ideal_trials_number_label, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(ideal_trials_number_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(reference_line_minimum_length_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(reference_line_minimum_length_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(reference_line_maximum_length_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(reference_line_maximum_length_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    familiarizationSettingsSizer->Add(line_width_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    familiarizationSettingsSizer->Add(line_width_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    experimentSettingsSizer = new wxBoxSizer(wxVERTICAL);
    experimentSettingsSizer->Add(experiment_line_width_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    experimentSettingsSizer->Add(experiment_line_width_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    experimentSettingsSizer->Add(experiment_max_trial_number_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    experimentSettingsSizer->Add(experiment_max_trial_number_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    experimentSettingsSizer->Add(experiment_judgement_line_increasing_step_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    experimentSettingsSizer->Add(experiment_judgement_line_increasing_step_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    experimentSettingsSizer->Add(experiment_initial_judgement_line_max_height_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    experimentSettingsSizer->Add(experiment_initial_judgement_line_max_height_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    experimentSettingsSizer->Add(experiment_initial_judgement_line_min_height_label, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    experimentSettingsSizer->Add(experiment_initial_judgement_line_min_height_input, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);

    mainSizer->Add(familiarizationSettingsSizer, 0, wxEXPAND);
    mainSizer->Add(experimentSettingsSizer, 0, wxEXPAND);
    mainSizer->AddStretchSpacer(1);
    mainSizer->Add(start_app_button, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, default_setting_element_padding);
    experimentSettingsSizer->Show(false);
    SetSizerAndFit(mainSizer);
}

void ExperimentControlPanel::InitializeEventHandlers() {
    this->Bind(wxEVT_CHOICE, [&](wxCommandEvent &event) {
        if (event.GetId() == ExperimentControlPanel::ID_BLOCK_TYPE_INPUT) {
            onBlockTypeChange(block_type_input->GetStringSelection().ToStdString());
        }
    });
}

FamiliarizationSettings *ExperimentControlPanel::GetFamiliarizationSettings() {
    Validate();
    TransferDataFromWindow();
    auto *dynamic_familiarization_settings = new FamiliarizationSettings();
    dynamic_familiarization_settings->ideal_speed_lowerbound_cm_s = std::strtod(
            familiarization_settings.ideal_speed_lowerbound_cm_s.c_str(),
            nullptr);
    dynamic_familiarization_settings->ideal_speed_upperbound_cm_s = std::strtod(
            familiarization_settings.ideal_speed_upperbound_cm_s, nullptr);
    dynamic_familiarization_settings->reference_line_speed_cm_s = std::strtod(
            familiarization_settings.reference_line_speed_cm_s, nullptr);
    dynamic_familiarization_settings->smoothing_intencity = std::stoi(
            familiarization_settings.smoothing_intencity.ToStdString());
    dynamic_familiarization_settings->borders_size_cm = std::strtod(
            familiarization_settings.borders_size_cm, nullptr);
    dynamic_familiarization_settings->is_transparency_enable = is_smoothing_enable->GetValue();
    dynamic_familiarization_settings->ideal_trials_number = std::stoi(
            familiarization_settings.ideal_trials_number.ToStdString());
    dynamic_familiarization_settings->reference_line_speedup_time_s = std::strtod(
            familiarization_settings.reference_line_speedup_time_s, nullptr);
    dynamic_familiarization_settings->reference_line_minimum_length_cm = std::strtod(
            familiarization_settings.minimum_ref_line_length_cm, nullptr);
    dynamic_familiarization_settings->reference_line_maximum_length_cm = std::strtod(
            familiarization_settings.maximum_ref_line_length_cm, nullptr);
    dynamic_familiarization_settings->line_width_cm = std::strtod(
            familiarization_settings.line_width_cm, nullptr);
    return dynamic_familiarization_settings;
}

ExperimentSettings *ExperimentControlPanel::GetExperimentSettings() {
    Validate();
    TransferDataFromWindow();
    auto *dynamic_experiment_settings = new ExperimentSettings();
    dynamic_experiment_settings->lines_width_cm = std::strtod(
            experiment_settings.lines_width_cm, nullptr);
    dynamic_experiment_settings->max_trial_number = std::stoi(
            experiment_settings.max_trial_number.ToStdString());
    dynamic_experiment_settings->judgement_line_increasing_step = std::stod(
            experiment_settings.judgement_line_increasing_step.ToStdString());
    dynamic_experiment_settings->initial_judgement_line_max_height = std::stod(
            experiment_settings.initial_judgement_line_max_height.ToStdString());
    dynamic_experiment_settings->initial_judgement_line_min_height = std::stod(
            experiment_settings.initial_judgement_line_min_height.ToStdString());
    return dynamic_experiment_settings;
}

void ExperimentControlPanel::DisplayFamiliarizationSettings() {
    experimentSettingsSizer->Show(false);
    familiarizationSettingsSizer->Show(true);
    this->Layout();
}

void ExperimentControlPanel::DisplayActiveMovementSettings() {
    familiarizationSettingsSizer->Show(false);
    experimentSettingsSizer->Show(true);
    this->Layout();
}

void ExperimentControlPanel::DisplayActiveTouchSettings() {
    familiarizationSettingsSizer->Show(false);
    experimentSettingsSizer->Show(true);
    this->Layout();
}

void ExperimentControlPanel::DisplayPassiveMovementSettings() {
    familiarizationSettingsSizer->Show(false);
    experimentSettingsSizer->Show(true);
    this->Layout();
}

void ExperimentControlPanel::DisplayPassiveTouchSettings() {
    familiarizationSettingsSizer->Show(false);
    experimentSettingsSizer->Show(true);
    this->Layout();
}

void ExperimentControlPanel::Display2DTestingSettings() {
    familiarizationSettingsSizer->Show(false);
    experimentSettingsSizer->Show(false);
    this->Layout();
}
