#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTCONTROLPANEL_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTCONTROLPANEL_H

#include <wx/wx.h>
#include "src/datastruct/wxParticipantInfo.h"
#include "src/datastruct/FamiliarizationSettings.h"
#include "src/datastruct/ExperimentSettings.h"

class ExperimentControlPanel : public wxPanel {
public:
    static const wxWindowID ID_START_EXPERIMENT_BTN = 1011;
    static const wxWindowID ID_BLOCK_TYPE_INPUT = 1101;

    ExperimentControlPanel(wxWindow *parent, std::function<void(std::string new_block_type)> onBlockTypeChange);

    FamiliarizationSettings *GetFamiliarizationSettings();

    ExperimentSettings *GetExperimentSettings();

    void DisplayFamiliarizationSettings();

    void DisplayActiveMovementSettings();

    void DisplayActiveTouchSettings();

    void DisplayPassiveMovementSettings();

    void DisplayPassiveTouchSettings();

    void Display2DTestingSettings();

private:
    void InitializeSizers();

    void InitializeEventHandlers();

    wxBoxSizer *familiarizationSettingsSizer;
    wxBoxSizer *experimentSettingsSizer;

    wxStaticBox *app_settings_box = new wxStaticBox(this, wxID_ANY, "Block settings");

    wxButton *start_app_button = new wxButton(this, ID_START_EXPERIMENT_BTN, "Start the block", wxDefaultPosition, wxSize(-1, 100));

    wxStaticText *block_type_label = new wxStaticText(this, wxID_ANY, "Block type:");
    wxChoice *block_type_input;

    wxStaticText *speed_lowerbound_label = new wxStaticText(this, wxID_ANY, "Speed lower bound (cm/s):");
    wxTextCtrl *speed_lowerbound_input;

    wxStaticText *speed_upperbound_label = new wxStaticText(this, wxID_ANY, "Speed upper bound (cm/s):");
    wxTextCtrl *speed_upperbound_input;

    wxStaticText *reference_line_speed_label = new wxStaticText(this, wxID_ANY, "Reference line speed (cm/s):");
    wxTextCtrl *reference_line_speed_input;

    wxStaticText *reference_line_speedup_time_label = new wxStaticText(this, wxID_ANY, "Reference line acceleration/deceleration time (sec):");
    wxTextCtrl *reference_line_speedup_time_input;

    wxStaticText *smoothing_filter_rate_label = new wxStaticText(this, wxID_ANY, "Smoothing rate (must be >0):");
    wxTextCtrl *smoothing_filter_rate_input;

    wxCheckBox *is_smoothing_enable = new wxCheckBox(this, wxID_ANY, "Lines transparency");

    wxStaticText *borders_size_label = new wxStaticText(this, wxID_ANY, "Gray zone size (cm):");
    wxTextCtrl *borders_size_input;

    wxStaticText *ideal_trials_number_label = new wxStaticText(this, wxID_ANY, "Good trials number:");
    wxTextCtrl *ideal_trials_number_input;

    wxStaticText *reference_line_minimum_length_label = new wxStaticText(this, wxID_ANY, "Minimum reference line length (cm):");
    wxTextCtrl *reference_line_minimum_length_input;

    wxStaticText *reference_line_maximum_length_label = new wxStaticText(this, wxID_ANY, "Maximum reference line length (cm):");
    wxTextCtrl *reference_line_maximum_length_input;

    wxStaticText *line_width_label = new wxStaticText(this, wxID_ANY, "Lines width (cm):");
    wxTextCtrl *line_width_input;

    wxStaticText *experiment_line_width_label = new wxStaticText(this, wxID_ANY, "Lines width (cm):");
    wxTextCtrl *experiment_line_width_input;

    wxStaticText *experiment_max_trial_number_label = new wxStaticText(this, wxID_ANY, "Trials number:");
    wxTextCtrl *experiment_max_trial_number_input;

    wxStaticText *experiment_judgement_line_increasing_step_label = new wxStaticText(this, wxID_ANY, "Judgement line increasing step (cm):");
    wxTextCtrl *experiment_judgement_line_increasing_step_input;

    wxStaticText *experiment_initial_judgement_line_max_height_label = new wxStaticText(this, wxID_ANY, "Initial judgement line max height (cm):");
    wxTextCtrl *experiment_initial_judgement_line_max_height_input;

    wxStaticText *experiment_initial_judgement_line_min_height_label = new wxStaticText(this, wxID_ANY, "Initial judgement line min height (cm):");
    wxTextCtrl *experiment_initial_judgement_line_min_height_input;

    int default_setting_element_padding = 8;

    struct wxFamiliarizationSettings {
        wxString ideal_speed_lowerbound_cm_s;
        wxString ideal_speed_upperbound_cm_s;
        wxString reference_line_speed_cm_s;
        wxString reference_line_speedup_time_s;
        wxString smoothing_intencity;
        wxString borders_size_cm;
        wxString is_smoothing_enable;
        wxString ideal_trials_number;
        wxString minimum_ref_line_length_cm;
        wxString maximum_ref_line_length_cm;
        wxString line_width_cm;
    };

    struct wxExperimentSettings {
        wxString lines_width_cm;
        wxString max_trial_number;
        wxString judgement_line_increasing_step;
        wxString initial_judgement_line_max_height;
        wxString initial_judgement_line_min_height;
    };

    wxFamiliarizationSettings familiarization_settings;
    wxExperimentSettings experiment_settings;

    std::function<void(std::string new_block_type)> onBlockTypeChange;
};

#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTCONTROLPANEL_H
