#include "ExperimentPanel.h"
#include "src/ui/wx/drawer/CustomDrawer.h"
#include "src/ui/wx/drawer/GridDrawer.h"
#include "src/ui/wx/wxutility/ColorConverter.h"
#include "src/utility/ScreenSettings.h"

ExperimentPanel::ExperimentPanel(wxFrame *parent) : wxPanel(parent){

}

void ExperimentPanel::DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) {
    wxPaintDC dc(this);
    auto custom_drawer = CustomDrawer(dc);
    custom_drawer.DrawCenteredText(text_lines, RGBA_to_wxColor(text_style.GetColour()), text_style.GetHeightPx());
}

void ExperimentPanel::DrawExperimentEvaluationScene(const Rectangle &participant_judgement_rectangle, bool on_left_screen_side) {
    wxPaintDC dc(this);
    dc.SetPen(*wxTRANSPARENT_PEN);
    auto grid_drawer = GridDrawer(dc);
    int window_width_px, window_height_px;
    dc.GetSize(&window_width_px, &window_height_px);
    grid_drawer.SetXGridSize(16);
    grid_drawer.SetYGridSize(16);
    if (on_left_screen_side) {
        grid_drawer.DrawRectangleByGrid(Rectangle(participant_judgement_rectangle, app_screen_settings->GetCmToPxRate()), 4, 0, false,
                                        true, CENTERED_BY_CEIL_LEFT_BORDER);
    } else {
        grid_drawer.DrawRectangleByGrid(Rectangle(participant_judgement_rectangle, app_screen_settings->GetCmToPxRate()), 12, 0, false,
                                        true, CENTERED_BY_CEIL_LEFT_BORDER);
    }
}

void ExperimentPanel::DrawDownRightCornerText(const std::string &text_line, const TextStyle &text_style) {
    wxPaintDC dc(this);
    dc.SetPen(*wxTRANSPARENT_PEN);
    auto grid_drawer = GridDrawer(dc);
    int window_width_px, window_height_px;
    dc.GetSize(&window_width_px, &window_height_px);
    grid_drawer.SetXGridSize(16);
    grid_drawer.SetYGridSize(16);
    grid_drawer.DrawTextLineByGreed(text_line, 14, 14, FROM_CEIL_TOP_LEFT_ANGLE,
                                    RGBA_to_wxColor(text_style.GetColour()),
                                    text_style.GetHeightPx());
}
