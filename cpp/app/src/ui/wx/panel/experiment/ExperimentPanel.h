#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTPANEL_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTPANEL_H

#include <wx/wx.h>
#include "src/ui/wx/wxutility/rectangle/Rectangle.h"
#include "src/ui/wx/wxutility/TextStyle.h"

class ExperimentPanel : public wxPanel  {

public:
    ExperimentPanel(wxFrame *parent);

    void DrawExperimentActiveScene();

    void DrawExperimentEvaluationScene(const Rectangle &participant_judgement_rectangle, bool on_left_screen_side = true);

    void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style);

    void DrawDownRightCornerText(const std::string &text_line, const TextStyle &text_style);

};


#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTPANEL_H
