#include "FamiliarizationPanel.h"
#include "src/ui/wx/drawer/CustomDrawer.h"
#include "src/ui/wx/drawer/GridDrawer.h"
#include "wx/charts/wxchartscategoricaldata.h"
#include "wx/charts/wxchartstheme.h"
#include "src/ui/wx/graph/wxSpeedEvaluationGraphCtl.h"
#include "src/utility/ScreenSettings.h"
#include "src/ui/wx/wxutility/ColorConverter.h"

FamiliarizationPanel::FamiliarizationPanel(wxFrame *parent) : wxPanel(parent) {

}

void FamiliarizationPanel::DrawImage(const std::string &file, wxBitmapType format) {
    wxPaintDC dc(this);
    auto custom_drawer = CustomDrawer(dc);
    custom_drawer.DrawImage(file, format);
}

void FamiliarizationPanel::DrawFamiliarizationActiveScene(const Rectangle &participant_rectangle,
                                                          const Rectangle &reference_rectangle,
                                                          const Rectangle &upper_border_rectangle,
                                                          const Rectangle &lower_border_rectangle,
                                                          double ref_line_max_length_cm,
                                                          const TextStyle &text_style) {
    wxPaintDC dc(this);
    dc.SetPen(*wxTRANSPARENT_PEN);
    auto grid_drawer = GridDrawer(dc);
    int window_width_px, window_height_px;
    dc.GetSize(&window_width_px, &window_height_px);
    grid_drawer.SetXGridSize(16);
    grid_drawer.SetYGridSize(16);
    DrawFamiliarizationBorders(grid_drawer, upper_border_rectangle, lower_border_rectangle, ref_line_max_length_cm,
                               window_width_px, window_height_px);

    grid_drawer.DrawRectangleByGrid(Rectangle(participant_rectangle, app_screen_settings->GetCmToPxRate()), 4, 0, false,
                                    true, CENTERED_BY_CEIL_LEFT_BORDER);
    grid_drawer.DrawRectangleByGrid(Rectangle(reference_rectangle, app_screen_settings->GetCmToPxRate()), 12, 0, false,
                                    true, CENTERED_BY_CEIL_LEFT_BORDER);

    grid_drawer.DrawCenteredText({"+"}, RGBA_to_wxColor(text_style.GetColour()), text_style.GetHeightPx());
}

void FamiliarizationPanel::DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) {
    ClearScreen();
    wxPaintDC dc(this);
    auto custom_drawer = CustomDrawer(dc);
    custom_drawer.DrawCenteredText(text_lines, RGBA_to_wxColor(text_style.GetColour()), text_style.GetHeightPx());
}

void FamiliarizationPanel::DrawFamiliarizationEvaluationScene(const Rectangle &participant_rectangle,
                                                              const Rectangle &upper_border_rectangle,
                                                              const Rectangle &lower_border_rectangle,
                                                              double ref_line_max_length_cm,
                                                              double speed_lowerbound, double speed_upperbound,
                                                              std::vector<double> reference_line_recorded_speed,
                                                              std::vector<double> participant_line_recorded_speed,
                                                              double reference_line_speed,
                                                              std::vector<std::pair<const std::string, const TextStyle>> &evaluation_feedback,
                                                              std::pair<const std::string, const TextStyle> trial_information) {
    wxPaintDC dc(this);
    dc.SetPen(*wxTRANSPARENT_PEN);
    auto grid_drawer = GridDrawer(dc);
    int window_width_px, window_height_px;
    dc.GetSize(&window_width_px, &window_height_px);
    grid_drawer.SetXGridSize(16);
    grid_drawer.SetYGridSize(16);
    std::pair<int, int> top_and_bottom_border_top_line_pos = DrawFamiliarizationBorders(grid_drawer,
                                                                                        upper_border_rectangle,
                                                                                        lower_border_rectangle,
                                                                                        ref_line_max_length_cm,
                                                                                        window_width_px,
                                                                                        window_height_px);

    grid_drawer.DrawRectangleByGrid(Rectangle(participant_rectangle, app_screen_settings->GetCmToPxRate()), 4, 0, false,
                                    true, CENTERED_BY_CEIL_LEFT_BORDER);

    DrawParticipantLineAndBorderDiff(grid_drawer, participant_rectangle, upper_border_rectangle.GetHeight(),
                                     ref_line_max_length_cm, top_and_bottom_border_top_line_pos.first,
                                     top_and_bottom_border_top_line_pos.second);

    if (line_chart_ctrl == nullptr) {
        DrawSpeedGraph({(int) (window_width_px / 1.8), (int) (window_height_px / 1.6)},
                       {(int) (window_width_px - (window_width_px / 1.6)),
                        (int) ((window_height_px - (window_height_px / 1.6)) / 2)}, reference_line_recorded_speed,
                       participant_line_recorded_speed, speed_lowerbound,
                       speed_upperbound, reference_line_speed); //todo:: move to grid drawer
    }

    grid_drawer.DrawCenteredTextOnTop(evaluation_feedback, 20);

    grid_drawer.DrawTextLineByGreed(trial_information.first, 15, 15, FROM_CEIL_TOP_LEFT_ANGLE,
                                    RGBA_to_wxColor(trial_information.second.GetColour()),
                                    trial_information.second.GetHeightPx());
}

std::pair<int, int> FamiliarizationPanel::DrawFamiliarizationBorders(CustomDrawer &custom_drawer,
                                                                     const Rectangle &upper_border_rectangle,
                                                                     const Rectangle &lower_border_rectangle,
                                                                     double ref_line_max_length_cm,
                                                                     int window_width_px, int window_height_px) {
    int ref_line_max_length_px = static_cast<int>(ref_line_max_length_cm * app_screen_settings->GetCmToPxRate());
    int border_height_px = static_cast<int>(upper_border_rectangle.GetHeight() * app_screen_settings->GetCmToPxRate());
    int top_border_line_top_pos_Y = ((window_height_px - ref_line_max_length_px) / 2) -
                                    (border_height_px /
                                     2);
    int bottom_border_line_top_pos_Y =
            (((window_height_px - ref_line_max_length_px) / 2) + ref_line_max_length_px) -
            (border_height_px /
             2);
    custom_drawer.DrawRectangle(
            ScreenRectangle(window_width_px, border_height_px, 0, top_border_line_top_pos_Y,
                            upper_border_rectangle.GetColour(), upper_border_rectangle.WithTransparencyGradient()));
    custom_drawer.DrawRectangle(ScreenRectangle(window_width_px, border_height_px, 0, bottom_border_line_top_pos_Y,
                                                lower_border_rectangle.GetColour(),
                                                lower_border_rectangle.WithTransparencyGradient()));
    return {top_border_line_top_pos_Y, bottom_border_line_top_pos_Y};
}

void FamiliarizationPanel::DrawSpeedGraph(wxSize graph_size, wxPoint graph_pos,
                                          std::vector<double> reference_line_recorded_speed,
                                          std::vector<double> participant_line_recorded_speed,
                                          double speed_lowerbound, double speed_upperbound,
                                          double reference_line_speed) {
    wxVector<wxString> speedMeasurementsLabels;
    wxVector<wxDouble> wx_participant_line_recorded_speed;
    wxVector<wxDouble> wx_reference_line_recorded_speed;
    int acceleration_sample_size = 0;
    int ref_line_constant_speed_sample_size = 0;
    for (int i = 0; i < reference_line_recorded_speed.size(); ++i) {
        if (reference_line_recorded_speed[i] != reference_line_speed && ref_line_constant_speed_sample_size == 0) {
            acceleration_sample_size++;
        } else if (reference_line_recorded_speed[i] == reference_line_speed) {
            ref_line_constant_speed_sample_size++;
        }
        speedMeasurementsLabels.push_back("-"); // Labels stub
        wx_reference_line_recorded_speed.push_back(reference_line_recorded_speed[i]);
        wx_participant_line_recorded_speed.push_back(participant_line_recorded_speed[i]);
    }
    wxChartsCategoricalData::ptr allData = wxChartsCategoricalData::make_shared(speedMeasurementsLabels);

    wxChartsDoubleDataset::ptr participant_speed_ds(
            new wxChartsDoubleDataset("Participant speed", wx_participant_line_recorded_speed));
    allData->AddDataset(participant_speed_ds);

    wxChartsDoubleDataset::ptr ref_line_speed_ds(
            new wxChartsDoubleDataset("Reference line speed", wx_reference_line_recorded_speed));
    allData->AddDataset(ref_line_speed_ds);

    wxVector<wxDouble> lowerbound_ds;
    wxVector<wxDouble> upperbound_ds;
    for (int i = 0; i < ref_line_constant_speed_sample_size; ++i) {
        lowerbound_ds.push_back(speed_lowerbound);
        upperbound_ds.push_back(speed_upperbound);
    }
    wxChartsDoubleDataset::ptr lowerbound_dataset(new wxChartsDoubleDataset("Lowerbound", lowerbound_ds));
    wxChartsDoubleDataset::ptr upperbound_dataset(new wxChartsDoubleDataset("Upperbound", upperbound_ds));
    allData->AddDataset(upperbound_dataset);
    allData->AddDataset(lowerbound_dataset);
    if (wxChartsDefaultTheme->GetDatasetTheme(
            wxChartsDatasetId::CreateImplicitId(3)) == nullptr) {
        wxChartsDefaultTheme->SetDatasetTheme(wxChartsDatasetId::CreateImplicitId(3),
                                              wxChartsDefaultTheme->GetDatasetTheme(
                                                      wxChartsDatasetId::CreateImplicitId(
                                                              2))); // the chart library doesnt have theme for id 3.
    }

    // Create the line chart widget from the constructed data
    line_chart_ctrl = new wxSpeedEvaluationGraphCtl(this, wxID_ANY, allData, ref_line_speed_ds, participant_speed_ds,
                                                    lowerbound_dataset, upperbound_dataset,
                                                    wxCHARTSLINETYPE_STRAIGHT, graph_pos, graph_size, wxBORDER_SIMPLE,
                                                    acceleration_sample_size);
    line_chart_ctrl->Bind(wxEVT_LEFT_DOWN, &FamiliarizationPanel::OnMouseClick,
                                this);
    line_chart_ctrl->Bind(wxEVT_RIGHT_DOWN, &FamiliarizationPanel::OnMouseClick,
                          this);
}

void FamiliarizationPanel::ClearScreen() {
    if (line_chart_ctrl != nullptr) {
        line_chart_ctrl->Unbind(wxEVT_LEFT_DOWN, &FamiliarizationPanel::OnMouseClick,
                                this);
        line_chart_ctrl->Unbind(wxEVT_RIGHT_DOWN, &FamiliarizationPanel::OnMouseClick,
                                this);
        line_chart_ctrl->Destroy();
        line_chart_ctrl = nullptr;
    }
}

void FamiliarizationPanel::DrawParticipantLineAndBorderDiff(GridDrawer &grid_drawer,
                                                            const Rectangle &participant_rectangle,
                                                            double border_height_cm,
                                                            double ref_line_max_length_cm,
                                                            int top_border_line_top_pos_px,
                                                            int bottom_border_line_top_pos_px) {
    double diff_between_inner_borders_cm = ref_line_max_length_cm - border_height_cm;
    double diff_between_outer_borders_cm = ref_line_max_length_cm + border_height_cm;
    double current_part_line_height_cm = participant_rectangle.GetHeight();
    int difference_line_width_px = participant_rectangle.GetWidth() * app_screen_settings->GetCmToPxRate();
    int window_width_px, window_height_px;
    grid_drawer.getDc().GetSize(&window_width_px, &window_height_px);
    if (current_part_line_height_cm > diff_between_outer_borders_cm) {
        int half_line_deviation_px = ceil(((current_part_line_height_cm - diff_between_outer_borders_cm) / 2) *
                                          app_screen_settings->GetCmToPxRate());
        Rectangle top_length_evaluation_rectangle = Rectangle(difference_line_width_px,
                                                              half_line_deviation_px,
                                                              MeasurementUnit::PX,
                                                              wxColor_to_RGBA(*wxRED));
        Rectangle bottom_length_evaluation_rectangle = Rectangle(difference_line_width_px,
                                                                 half_line_deviation_px,
                                                                 MeasurementUnit::PX,
                                                                 wxColor_to_RGBA(*wxRED));
        grid_drawer.DrawRectangleByAxisXGrid(top_length_evaluation_rectangle, 4, top_border_line_top_pos_px -
                                                                                 half_line_deviation_px, false,
                                             CENTERED_BY_CEIL_LEFT_BORDER);
        grid_drawer.DrawRectangleByAxisXGrid(bottom_length_evaluation_rectangle, 4,
                                             bottom_border_line_top_pos_px + floor(border_height_cm *
                                                                                   app_screen_settings->GetCmToPxRate()),
                                             false,
                                             CENTERED_BY_CEIL_LEFT_BORDER);
    } else if (participant_rectangle.GetHeight() < diff_between_inner_borders_cm) {
        int half_line_deviation_px = ceil(((diff_between_inner_borders_cm - current_part_line_height_cm) / 2) *
                                          app_screen_settings->GetCmToPxRate());
        Rectangle top_length_evaluation_rectangle = Rectangle(difference_line_width_px,
                                                              half_line_deviation_px,
                                                              MeasurementUnit::PX,
                                                              wxColor_to_RGBA(*wxBLUE));
        Rectangle bottom_length_evaluation_rectangle = Rectangle(difference_line_width_px,
                                                                 half_line_deviation_px,
                                                                 MeasurementUnit::PX,
                                                                 wxColor_to_RGBA(*wxBLUE));
        grid_drawer.DrawRectangleByAxisXGrid(top_length_evaluation_rectangle, 4, top_border_line_top_pos_px +
                                                                                 (border_height_cm *
                                                                                  app_screen_settings->GetCmToPxRate()),
                                             false,
                                             CENTERED_BY_CEIL_LEFT_BORDER);
        grid_drawer.DrawRectangleByAxisXGrid(bottom_length_evaluation_rectangle, 4, bottom_border_line_top_pos_px -
                                                                                    half_line_deviation_px - 1, false,
                                             CENTERED_BY_CEIL_LEFT_BORDER); // Magic number -1 is intentionally here for fixing GUI 1 pixel space error at some screen sizes.
        //Todo: 1 pixel space error may be fixed in another way
    }
}

void FamiliarizationPanel::OnMouseClick(wxMouseEvent &evt) {
    this->AddPendingEvent(evt);
}

