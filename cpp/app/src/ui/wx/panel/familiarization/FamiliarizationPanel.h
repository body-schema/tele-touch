#ifndef QUALISYSKINOVAINTERFACE_FAMILIARIZATIONPANEL_H
#define QUALISYSKINOVAINTERFACE_FAMILIARIZATIONPANEL_H


#include <wx/wx.h>
#include <list>
#include "src/ui/wx/wxutility/rectangle/ScreenRectangle.h"
#include "src/ui/wx/wxutility/TextStyle.h"
#include "src/ui/wx/drawer/CustomDrawer.h"
#include "src/ui/wx/graph/wxSpeedEvaluationGraphCtl.h"
#include "src/ui/wx/drawer/GridDrawer.h"

class FamiliarizationPanel : public wxPanel {

public:
    FamiliarizationPanel(wxFrame *parent);

    void DrawImage(const std::string &file, wxBitmapType format);

    void DrawFamiliarizationActiveScene(const Rectangle &participant_rectangle,
                                                const Rectangle &reference_rectangle,
                                                const Rectangle &upper_border_rectangle,
                                                const Rectangle &lower_border_rectangle,
                                        double ref_line_max_length_cm,
                                        const TextStyle &text_style);

    void DrawFamiliarizationEvaluationScene(const Rectangle &participant_rectangle,
                                            const Rectangle &upper_border_rectangle,
                                            const Rectangle &lower_border_rectangle,
                                            double ref_line_max_length_cm,
                                            double speed_lowerbound,
                                            double speed_upperbound,
                                            std::vector<double> reference_line_recorded_speed,
                                            std::vector<double> participant_line_recorded_speed, double reference_line_speed,
                                            std::vector<std::pair<const std::string, const TextStyle>> &evaluation_feedback,
                                            std::pair<const std::string, const TextStyle> trial_information);

    void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style);

private:
    wxSpeedEvaluationGraphCtl *line_chart_ctrl = nullptr;

    std::pair<int,int> DrawFamiliarizationBorders(CustomDrawer &custom_drawer, const Rectangle &upper_border_rectangle,
                                    const Rectangle &lower_border_rectangle,
                                    double ref_line_max_length_cm,
                                    int window_width_px, int window_height_px);

    void DrawParticipantLineAndBorderDiff(GridDrawer &grid_drawer,
                                          const Rectangle &participant_rectangle,
                                          double border_height_cm,
                                          double ref_line_max_length_cm,
                                          int top_border_line_top_pos_px,
                                          int bottom_border_line_top_pos_px);
    void DrawSpeedGraph(wxSize graph_size, wxPoint graph_pos, std::vector<double> reference_line_recorded_speed,
                        std::vector<double> participant_line_recorded_speed, double speed_lowerbound, double speed_upperbound, double reference_line_speed);
    void ClearScreen();
    void OnMouseClick(wxMouseEvent &evt);
};


#endif //QUALISYSKINOVAINTERFACE_FAMILIARIZATIONPANEL_H
