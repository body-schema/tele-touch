#ifndef QUALISYSKINOVAINTERFACE_PARTICIPANTINFOPANEL_H
#define QUALISYSKINOVAINTERFACE_PARTICIPANTINFOPANEL_H

#include <wx/wx.h>
#include "src/datastruct/wxParticipantInfo.h"

class ParticipantInfoPanel : public wxScrolledWindow {
public:
    static const wxWindowID ID_EDIT_PARTICIPANT = 1001;

    explicit ParticipantInfoPanel(wxWindow *parent);

    void UpdateInfo(wxParticipantInfo &newParticipantInfo);

private:
    wxStaticBox *staticBox = new wxStaticBox(this, wxID_ANY, "Participant info");

    wxStaticText *participantLabel = new wxStaticText(staticBox, wxID_ANY, "Participant: ");
    wxStaticText *participantValue = new wxStaticText(staticBox, wxID_ANY, "");;

    wxStaticText *ageLabel = new wxStaticText(staticBox, wxID_ANY, "Age: ");
    wxStaticText *ageValue = new wxStaticText(staticBox, wxID_ANY, "");;

    wxStaticText *genderLabel = new wxStaticText(staticBox, wxID_ANY, "Gender: ");
    wxStaticText *genderValue = new wxStaticText(staticBox, wxID_ANY, "");

    wxStaticText *lengthOfForearmLabel = new wxStaticText(staticBox, wxID_ANY, "Length of forearm: ");
    wxStaticText *lengthOfForearmValue = new wxStaticText(staticBox, wxID_ANY, "");

    wxStaticText *handednessLabel = new wxStaticText(staticBox, wxID_ANY, "Handedness: ");
    wxStaticText *handednessValue = new wxStaticText(staticBox, wxID_ANY, "");

    wxStaticText *sensitivityOfTactileTestLabel = new wxStaticText(staticBox, wxID_ANY,
                                                                   "Sensitivity to tactile test: ");
    wxStaticText *sensitivityOfTactileTestValue = new wxStaticText(staticBox, wxID_ANY, "");

    wxStaticText *orderOfTheBlocksLabel = new wxStaticText(staticBox, wxID_ANY, "Order of the blocks: ");
    wxStaticText *orderOfTheBlocksValue = new wxStaticText(staticBox, wxID_ANY, "");

    wxStaticText *descriptionLabel = new wxStaticText(staticBox, wxID_ANY, "Description: ");
    wxStaticText *descriptionValue = new wxStaticText(staticBox, wxID_ANY, "");

    wxButton *editButton = new wxButton(staticBox, ID_EDIT_PARTICIPANT,"Edit");

    void InitializeParticipantText();

    void InitializeMainSizer();
};


#endif //QUALISYSKINOVAINTERFACE_PARTICIPANTINFOPANEL_H
