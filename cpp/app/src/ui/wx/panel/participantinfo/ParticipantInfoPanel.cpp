#include "ParticipantInfoPanel.h"

ParticipantInfoPanel::ParticipantInfoPanel(wxWindow *parent) : wxScrolledWindow(parent,
                                                                                                          wxID_ANY,
                                                                                                          wxDefaultPosition,
                                                                                                          wxDefaultSize,
                                                                                                          wxTAB_TRAVERSAL |
                                                                                                          wxNO_BORDER,
                                                                                                          wxPanelNameStr) {
    InitializeParticipantText();
    InitializeMainSizer();
}

void ParticipantInfoPanel::UpdateInfo(wxParticipantInfo &newParticipantInfo) {
    participantValue->SetLabel(newParticipantInfo.participantCodename);
    ageValue->SetLabel(newParticipantInfo.age);
    genderValue->SetLabel(newParticipantInfo.gender);
    lengthOfForearmValue->SetLabel(newParticipantInfo.lengthOfForearm);
    handednessValue->SetLabel(newParticipantInfo.handedness);
    sensitivityOfTactileTestValue->SetLabel(newParticipantInfo.sensitivityOfTactileTest);
    orderOfTheBlocksValue->SetLabel(newParticipantInfo.orderOfTheBlocks);
    descriptionValue->SetLabel(newParticipantInfo.description);
}

void ParticipantInfoPanel::InitializeParticipantText() {
    staticBox->SetFont(this->GetFont().Scale(1.5));
    wxFont labelsFont = this->GetFont().Scale(1.3);
    wxFont valueFont = this->GetFont().Scale(1.3);
    labelsFont.SetWeight(wxFONTWEIGHT_SEMIBOLD);
    participantLabel->SetFont(labelsFont);
    participantValue->SetFont(valueFont);
    ageLabel->SetFont(labelsFont);
    ageValue->SetFont(valueFont);
    genderLabel->SetFont(labelsFont);
    genderValue->SetFont(valueFont);
    lengthOfForearmLabel->SetFont(labelsFont);
    lengthOfForearmValue->SetFont(valueFont);
    handednessLabel->SetFont(labelsFont);
    handednessValue->SetFont(valueFont);
    sensitivityOfTactileTestLabel->SetFont(labelsFont);
    sensitivityOfTactileTestValue->SetFont(valueFont);
    orderOfTheBlocksLabel->SetFont(labelsFont);
    orderOfTheBlocksValue->SetFont(valueFont);
    descriptionLabel->SetFont(labelsFont);
    descriptionValue->SetFont(valueFont);
}

void ParticipantInfoPanel::InitializeMainSizer() {
    auto *mainSizer = new wxStaticBoxSizer(staticBox, wxVERTICAL);
    auto *participantInfoGrid = new wxFlexGridSizer(2);
    participantInfoGrid->Add(participantLabel, 0);
    participantInfoGrid->Add(participantValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(ageLabel);
    participantInfoGrid->Add(ageValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(genderLabel);
    participantInfoGrid->Add(genderValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(lengthOfForearmLabel);
    participantInfoGrid->Add(lengthOfForearmValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(handednessLabel);
    participantInfoGrid->Add(handednessValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(sensitivityOfTactileTestLabel);
    participantInfoGrid->Add(sensitivityOfTactileTestValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(orderOfTheBlocksLabel);
    participantInfoGrid->Add(orderOfTheBlocksValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(descriptionLabel);
    participantInfoGrid->Add(descriptionValue, 0, wxLEFT | wxBOTTOM, 10);
    participantInfoGrid->Add(editButton, 0, wxTOP, 16);
    mainSizer->AddSpacer(50);
    mainSizer->Add(participantInfoGrid, 0, wxLEFT | wxRIGHT, 30);
    mainSizer->AddSpacer(50);
    SetScrollRate(10,10);
    SetSizerAndFit(mainSizer);
    SetVirtualSize(mainSizer->GetSize());
}
