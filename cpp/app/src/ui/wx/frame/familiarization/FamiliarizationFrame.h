#ifndef TELE_TOUCH_FAMILIARIZATIONFRAME_H
#define TELE_TOUCH_FAMILIARIZATIONFRAME_H

#include <wx/wx.h>
#include "src/ui/view/FamiliarizationView.h"
#include "src/ui/wx/panel/familiarization/FamiliarizationPanel.h"
#include "src/datastruct/FamiliarizationSettings.h"
#include "src/ui/presenter/FamiliarizationViewPresenter.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/ui/wx/timer/RenderTimer.h"

class FamiliarizationFrame: public wxFrame, FamiliarizationView  {

public:
    FamiliarizationFrame(FamiliarizationSettings &familiarization_settings, ParticipantInfo participantInfo);

    void DrawPNGImage(const std::string &file) override;

    void DrawFamiliarizationActiveScene(const Rectangle &participant_rectangle,
                                                const Rectangle &reference_rectangle,
                                                const Rectangle &upper_border_rectangle,
                                                const Rectangle &lower_border_rectangle,
                                        double ref_line_max_length_cm,
                                        const TextStyle &text_style) override;

    void DrawFamiliarizationEvaluationScene(const Rectangle &reference_rectangle,
                                            const Rectangle &upper_border_rectangle,
                                            const Rectangle &lower_border_rectangle,
                                            double ref_line_max_length_cm,
                                            double speed_lowerbound,
                                            double speed_upperbound,
                                            std::vector<double> reference_line_recorded_speed,
                                            std::vector<double> participant_line_recorded_speed, double reference_line_speed,
                                            std::vector<std::pair<const std::string, const TextStyle>> &evaluation_feedback,
                                            std::pair<const std::string, const TextStyle> trial_information) override;

    void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) override;

    ~FamiliarizationFrame();


private:
    FamiliarizationViewPresenter *familiarization_view_presenter;
    FamiliarizationPanel * familiarization_panel;
    RenderTimer *renderTimer;
};



#endif //TELE_TOUCH_FAMILIARIZATIONFRAME_H
