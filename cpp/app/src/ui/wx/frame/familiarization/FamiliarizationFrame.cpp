#include "FamiliarizationFrame.h"
#include "src/ui/wx/timer/RenderTimer.h"

FamiliarizationFrame::FamiliarizationFrame(FamiliarizationSettings &familiarization_settings, ParticipantInfo participantInfo) : wxFrame(nullptr,
                                                                                                                                           wxID_ANY,
                                                                                                                                           "Familiarization block",
                                                                                                                                           wxDefaultPosition,
                                                                                                                                           wxDefaultSize,
                                                                                                                                           wxDEFAULT_FRAME_STYLE) {
    familiarization_view_presenter = new FamiliarizationViewPresenter(*this, familiarization_settings, participantInfo);

    familiarization_panel = new FamiliarizationPanel(this);
    familiarization_panel->Bind(wxEVT_PAINT, &FamiliarizationViewPresenter::OnPaintEvent,
                                familiarization_view_presenter);
    familiarization_panel->Bind(wxEVT_LEFT_DOWN, &FamiliarizationViewPresenter::OnMouseClick,
                                familiarization_view_presenter);
    familiarization_panel->Bind(wxEVT_RIGHT_DOWN, &FamiliarizationViewPresenter::OnMouseClick,
                                familiarization_view_presenter);
    this->Maximize();

    renderTimer = new RenderTimer(familiarization_panel);
    renderTimer->start();
}

FamiliarizationFrame::~FamiliarizationFrame() {
    delete renderTimer;
    delete familiarization_view_presenter;
}

void FamiliarizationFrame::DrawPNGImage(const std::string &file) {
    familiarization_panel->DrawImage(file, wxBITMAP_TYPE_PNG);
}

void FamiliarizationFrame::DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) {
    familiarization_panel->DrawCenteredText(text_lines, text_style);
}

void FamiliarizationFrame::DrawFamiliarizationActiveScene(const Rectangle &participant_rectangle,
                                                          const Rectangle &reference_rectangle,
                                                          const Rectangle &upper_border_rectangle,
                                                          const Rectangle &lower_border_rectangle,
                                                          double ref_line_max_length_cm,
                                                          const TextStyle &text_style) {
    familiarization_panel->DrawFamiliarizationActiveScene(participant_rectangle, reference_rectangle,
                                                          upper_border_rectangle, lower_border_rectangle,
                                                          ref_line_max_length_cm, text_style);
}

void FamiliarizationFrame::DrawFamiliarizationEvaluationScene(const Rectangle &reference_rectangle,
                                                              const Rectangle &upper_border_rectangle,
                                                              const Rectangle &lower_border_rectangle,
                                                              double ref_line_max_length_cm,
                                                              double speed_lowerbound, double speed_upperbound,
                                                              std::vector<double> reference_line_recorded_speed,
                                                              std::vector<double> participant_line_recorded_speed, double reference_line_speed,
                                                              std::vector<std::pair<const std::string, const TextStyle>> &evaluation_feedback,
                                                              std::pair<const std::string, const TextStyle> trial_information) {
    familiarization_panel->DrawFamiliarizationEvaluationScene(reference_rectangle, upper_border_rectangle,
                                                              lower_border_rectangle, ref_line_max_length_cm,
                                                              speed_lowerbound, speed_upperbound,
                                                              reference_line_recorded_speed,
                                                              participant_line_recorded_speed, reference_line_speed,
                                                              evaluation_feedback, trial_information);
}
