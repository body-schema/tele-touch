#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTFRAME_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTFRAME_H

#include <wx/wx.h>
#include "src/ui/view/ExperimentView.h"
#include "src/datastruct/ExperimentSettings.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/datastruct/ExperimentBlockType.h"
#include "src/ui/presenter/ExperimentViewPresenter.h"
#include "src/ui/wx/panel/experiment/ExperimentPanel.h"
#include "src/ui/wx/timer/RenderTimer.h"

class ExperimentFrame : public wxFrame, ExperimentView  {

public:
    ExperimentFrame(ExperimentSettings &experiment_settings, ParticipantInfo participantInfo, ExperimentBlockType experimentBlockType);

    void DrawExperimentActiveScene() override;

    void DrawExperimentEvaluationScene(const Rectangle &participant_judgement_rectangle, bool on_left_screen_side) override;

    void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) override;

    void DrawDownRightCornerText(const std::string &text_line, const TextStyle &text_style) override;

    ~ExperimentFrame();

private:
    ExperimentViewPresenter *experiment_view_presenter;
    ExperimentPanel * experiment_panel;
    RenderTimer *renderTimer;
};


#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTFRAME_H
