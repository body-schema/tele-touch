#include "ExperimentFrame.h"
#include "src/datastruct/ExperimentBlockType.h"
#include "src/ui/wx/timer/RenderTimer.h"

ExperimentFrame::ExperimentFrame(ExperimentSettings &experiment_settings, ParticipantInfo participantInfo, ExperimentBlockType experimentBlockType) : wxFrame(nullptr,
                                                                                                                     wxID_ANY,
                                                                                                                     "Experiment block - " + getBlockTypeStr(experimentBlockType),
                                                                                                                     wxDefaultPosition,
                                                                                                                     wxDefaultSize,
                                                                                                                     wxDEFAULT_FRAME_STYLE) {
    experiment_view_presenter = new ExperimentViewPresenter(*this, experiment_settings, participantInfo, experimentBlockType);

    experiment_panel = new ExperimentPanel(this);
    experiment_panel->Bind(wxEVT_PAINT, &ExperimentViewPresenter::OnPaintEvent,
                           experiment_view_presenter);
    experiment_panel->Bind(wxEVT_LEFT_DOWN, &ExperimentViewPresenter::OnMouseClick,
                           experiment_view_presenter);
    experiment_panel->Bind(wxEVT_RIGHT_DOWN, &ExperimentViewPresenter::OnMouseClick,
                           experiment_view_presenter);
    experiment_panel->Bind(wxEVT_KEY_DOWN, &ExperimentViewPresenter::OnKeyDown,
                           experiment_view_presenter);
    this->Maximize();

    renderTimer = new RenderTimer(experiment_panel);
    renderTimer->start();
    this->Maximize();
}

ExperimentFrame::~ExperimentFrame() {
    delete experiment_view_presenter;
}

void ExperimentFrame::DrawExperimentActiveScene() {
    delete renderTimer;
    delete experiment_view_presenter;
}

void ExperimentFrame::DrawExperimentEvaluationScene(const Rectangle &participant_judgement_rectangle, bool on_left_screen_side) {
    experiment_panel->DrawExperimentEvaluationScene(participant_judgement_rectangle, on_left_screen_side);
}

void ExperimentFrame::DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) {
    experiment_panel->DrawCenteredText(text_lines, text_style);
}

void ExperimentFrame::DrawDownRightCornerText(const string &text_line, const TextStyle &text_style) {
    experiment_panel->DrawDownRightCornerText(text_line, text_style);
}
