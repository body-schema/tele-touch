#ifndef QUALISYSKINOVAINTERFACE_SETUPFRAME_H
#define QUALISYSKINOVAINTERFACE_SETUPFRAME_H
#pragma once

#include <wx/wx.h>
#include "src/ui/view/SetupView.h"
#include "src/ui/presenter/SetupViewPresenter.h"
#include "src/ui/wx/dialog/editparticipant/EditParticipantDialog.h"
#include "src/ui/wx/panel/participantinfo/ParticipantInfoPanel.h"
#include "src/ui/wx/panel/experimentcontrol/ExperimentControlPanel.h"
#include "src/ui/wx/frame/familiarization/FamiliarizationFrame.h"

enum {
    ID_CREATE_PARTICIPANT = 1,
    ID_CREATE_AND_OPEN_PARTICIPANT = 2,
    ID_OPEN_PARTICIPANT = 3
};

class SetupFrame : public wxFrame, SetupView {
public:
    SetupFrame();

    void OpenCreateParticipantDialog(bool load_participant) override;

    void OpenEditParticipantDialog(ParticipantInfo participant_info) override;

    void DisplayUserInfo(ParticipantInfo &participantInfo) override;

    void OpenParticipantOpenDialog() override;

    void OpenExperimentView(ParticipantInfo participant_info, ExperimentSettings *dynamicExperimentSettings, ExperimentBlockType experimentBlockType) override;

    void OpenFamiliarizationView(ParticipantInfo participant_info, FamiliarizationSettings *dynamicFamiliarizationSettings) override;

    void DisplayFamiliarizationSettings() override;

    void DisplayActiveMovementSettings() override;

    void DisplayActiveTouchSettings() override;

    void DisplayPassiveMovementSettings() override;

    void DisplayPassiveTouchSettings() override;

    void Display2DTestingSettings() override;

    void Open2dTestView() override;

    FamiliarizationSettings *GetFamiliarizationSettings() override;

    ExperimentSettings *GetExperimentSettings() override;

    ~SetupFrame();

private:
    SetupViewPresenter *setup_view_presenter;

    wxMenuBar *menuBar = new wxMenuBar();
    wxMenu *menuParticipant = new wxMenu();
    wxBoxSizer *mainSizer = new wxBoxSizer(wxHORIZONTAL);
    ParticipantInfoPanel *participantInfoPanel = new ParticipantInfoPanel(this);

    ExperimentControlPanel *experimentControlPanel;

    void InitializeMenuBar();

    void InitializeEventHandlers();

    void InitializeMainSizer();
};


#endif //QUALISYSKINOVAINTERFACE_SETUPFRAME_H
