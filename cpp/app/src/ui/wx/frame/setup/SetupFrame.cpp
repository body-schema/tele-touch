#include "SetupFrame.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/ui/wx/frame/experiment/ExperimentFrame.h"
#include "src/ui/wx/frame/test2d/Test2dFrame.h"

SetupFrame::SetupFrame() : wxFrame(nullptr, wxID_ANY, "Start menu", wxDefaultPosition, wxDefaultSize,
                                   wxDEFAULT_FRAME_STYLE) {
    setup_view_presenter = new SetupViewPresenter(this);
    experimentControlPanel = new ExperimentControlPanel(this, std::bind(&SetupViewPresenter::OnChangeBlockType,
                                                                        setup_view_presenter, std::placeholders::_1));

    InitializeMenuBar();
    InitializeEventHandlers();

    this->CreateStatusBar();
    this->SetStatusText("No participant was selected.");
    this->Maximize();

    InitializeMainSizer();
    this->Show();
}

SetupFrame::~SetupFrame() {
    delete setup_view_presenter;
}

void SetupFrame::InitializeMenuBar() {
    menuParticipant->Append(ID_CREATE_PARTICIPANT, "&Create...\tCtrl-H",
                            "Creates csv/tsv file with participant information...");
    menuParticipant->Append(ID_CREATE_AND_OPEN_PARTICIPANT, "&Create and open...\tCtrl-H",
                            "Creates csv/tsv file with participant information and opens it...");
    menuParticipant->Append(ID_OPEN_PARTICIPANT, "&Open...\tCtrl-H",
                            "Open csv/tsv file with participant information...");
    menuParticipant->AppendSeparator();
    menuParticipant->Append(wxID_EXIT);
    menuBar->Append(menuParticipant, "&Participant");
    this->SetMenuBar(menuBar);
}

void SetupFrame::InitializeMainSizer() {
    mainSizer->Add(participantInfoPanel, 8, wxEXPAND | wxALL, 8);
    mainSizer->Add(experimentControlPanel, 2, wxEXPAND | wxALL, 8);
    this->SetSizerAndFit(mainSizer);
}

void SetupFrame::InitializeEventHandlers() {
    this->Bind(wxEVT_BUTTON, [&](wxCommandEvent &event) {
        if (event.GetId() == ParticipantInfoPanel::ID_EDIT_PARTICIPANT) {
            setup_view_presenter->OnEditParticipantRequest();
        } else if (event.GetId() == ExperimentControlPanel::ID_START_EXPERIMENT_BTN) {
            setup_view_presenter->OnStartExperimentRequest();
        }
    });
    menuBar->Bind(wxEVT_MENU, [&](wxCommandEvent &event) {
        if (event.GetId() == ID_CREATE_PARTICIPANT) {
            setup_view_presenter->OnCreateParticipantRequest();
        } else if (event.GetId() == ID_CREATE_AND_OPEN_PARTICIPANT) {
            setup_view_presenter->OnCreateAndOpenParticipantRequest();
        } else if (event.GetId() == ID_OPEN_PARTICIPANT) {
            setup_view_presenter->OnOpenParticipantRequest();
        } else if (event.GetId() == wxID_EXIT) {
            Close(true);
        }
        event.StopPropagation();
    });
}

void SetupFrame::OpenCreateParticipantDialog(bool load_participant) {
    auto file_path = std::string();
    wxParticipantInfo temp_participant_info;
    EditParticipantDialog dialog(this, temp_participant_info, file_path);
    if (dialog.ShowModal() == wxID_OK) {
        auto dialog_participant_info = wxPartInfo_to_PartInfo(temp_participant_info);
        if (load_participant) {
            setup_view_presenter->OnSaveAndOpenParticipant(dialog_participant_info, file_path);
        } else {
            setup_view_presenter->OnSaveParticipant(dialog_participant_info, file_path);
        }
    }
}

void SetupFrame::OpenEditParticipantDialog(ParticipantInfo participant_info) {
    auto wx_participant_info = PartInfo_to_wxPartInfo(participant_info);
    auto file_path = std::string();
    EditParticipantDialog dialog(this, wx_participant_info, file_path);
    if (dialog.ShowModal() == wxID_OK) {
        auto dialog_participant_info = wxPartInfo_to_PartInfo(wx_participant_info);
        setup_view_presenter->OnSaveAndOpenParticipant(dialog_participant_info, file_path);
    }
}

void SetupFrame::DisplayUserInfo(ParticipantInfo &participant_info) {
    auto wx_participant_info = PartInfo_to_wxPartInfo(participant_info);
    participantInfoPanel->UpdateInfo(wx_participant_info);
}

void SetupFrame::OpenParticipantOpenDialog() {
    wxFileDialog openFileDialog(this, wxEmptyString, wxEmptyString, wxEmptyString,
                                "Csv Files (*.csv)|*.csv|All Files (*.*)|*.*",
                                wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    openFileDialog.SetFilterIndex(0);
    if (openFileDialog.ShowModal() == wxID_OK) {
        std::string file_path = openFileDialog.GetPath().ToStdString();
        setup_view_presenter->OnOpenParticipant(file_path);
    }
}

void SetupFrame::OpenFamiliarizationView(ParticipantInfo participant_info, FamiliarizationSettings *dynamicFamiliarizationSettings) {
    auto *familiarizationFrame = new FamiliarizationFrame(*dynamicFamiliarizationSettings, participant_info);
    familiarizationFrame->Show();
    this->Close(false);
}

void SetupFrame::OpenExperimentView(ParticipantInfo participant_info, ExperimentSettings *dynamicExperimentSettings, ExperimentBlockType experimentBlockType) {
    auto *experimentFrame = new ExperimentFrame(*dynamicExperimentSettings, participant_info, experimentBlockType);
    experimentFrame->Show();
    this->Close(false);
}

void SetupFrame::DisplayFamiliarizationSettings() {
    experimentControlPanel->DisplayFamiliarizationSettings();
}

void SetupFrame::DisplayActiveMovementSettings() {
    experimentControlPanel->DisplayActiveMovementSettings();
}

void SetupFrame::DisplayActiveTouchSettings() {
    experimentControlPanel->DisplayActiveTouchSettings();
}

void SetupFrame::DisplayPassiveMovementSettings() {
    experimentControlPanel->DisplayPassiveMovementSettings();
}

void SetupFrame::DisplayPassiveTouchSettings() {
    experimentControlPanel->DisplayPassiveTouchSettings();
}

FamiliarizationSettings *SetupFrame::GetFamiliarizationSettings() {
    return experimentControlPanel->GetFamiliarizationSettings();
}

ExperimentSettings *SetupFrame::GetExperimentSettings() {
    return experimentControlPanel->GetExperimentSettings();
}

void SetupFrame::Display2DTestingSettings() {
    return experimentControlPanel->Display2DTestingSettings();
}

void SetupFrame::Open2dTestView() {
    auto *test2dFrame = new Test2dFrame();
    test2dFrame->Show();
    this->Close(false);
}

