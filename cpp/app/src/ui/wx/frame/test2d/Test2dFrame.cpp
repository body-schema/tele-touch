#include "Test2dFrame.h"

Test2dFrame::Test2dFrame() : wxFrame(nullptr, wxID_ANY, "Test 2d", wxDefaultPosition, wxDefaultSize,
                                     wxDEFAULT_FRAME_STYLE) {
    test2D_Panel = new Test2dPanel(this);

    test_2D_view_presenter = new Test2dViewPresenter(*this);
    test2D_Panel->Bind(wxEVT_PAINT, &Test2dViewPresenter::OnPaintEvent,
                       test_2D_view_presenter);
    test2D_Panel->Bind(wxEVT_LEFT_DOWN, &Test2dViewPresenter::OnMouseClick,
                       test_2D_view_presenter);
    test2D_Panel->Bind(wxEVT_RIGHT_DOWN, &Test2dViewPresenter::OnMouseClick,
                       test_2D_view_presenter);

    this->Maximize();

    renderTimer = new RenderTimer(test2D_Panel);
    renderTimer->start();
    this->Maximize();
}

void Test2dFrame::DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) {
    test2D_Panel->DrawCenteredText(text_lines, text_style);
}

Test2dFrame::~Test2dFrame() {

}
