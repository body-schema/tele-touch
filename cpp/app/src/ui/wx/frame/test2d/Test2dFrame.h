#ifndef QUALISYSKINOVAINTERFACE_TEST2DFRAME_H
#define QUALISYSKINOVAINTERFACE_TEST2DFRAME_H

#include <wx/wx.h>
#include "src/ui/presenter/Test2dViewPresenter.h"
#include "src/ui/view/Test2dView.h"
#include "src/ui/wx/timer/RenderTimer.h"
#include "src/ui/wx/panel/test2d/Test2dPanel.h"

class Test2dFrame : public wxFrame, Test2dView  {

public:
    Test2dFrame();

    void DrawCenteredText(const std::vector<std::string> &text_lines, const TextStyle &text_style) override;

    ~Test2dFrame() override;

private:
    Test2dViewPresenter *test_2D_view_presenter;
    Test2dPanel * test2D_Panel;
    RenderTimer *renderTimer;

};


#endif //QUALISYSKINOVAINTERFACE_TEST2DFRAME_H
