#ifndef QUALISYSKINOVAINTERFACE_RENDERTIMER_H
#define QUALISYSKINOVAINTERFACE_RENDERTIMER_H
#pragma once

#include <wx/timer.h>
#include <wx/wx.h>

class RenderTimer : public wxTimer {
    wxPanel *pane;
public:
    RenderTimer(wxPanel *pane);

    void Notify();

    void start();
};

#endif //QUALISYSKINOVAINTERFACE_RENDERTIMER_H
