#include "wxGraphCurve.h"

wxGraphCurve::wxGraphCurve(const wxString &name, const wxVector<wxDouble> &data, int dataset_id, bool show_dots,
                           bool enable_tooltip, bool fill_under, wxColour line_color) : wxChartsDoubleDataset(name,
                                                                                                              data),
                                                                                        dataset_id(dataset_id),
                                                                                        show_dots(show_dots),
                                                                                        enable_tooltip(enable_tooltip),
                                                                                        fill_under(fill_under),
                                                                                        line_color(line_color) {

}
