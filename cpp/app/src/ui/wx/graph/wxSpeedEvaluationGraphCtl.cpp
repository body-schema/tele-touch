#include "wxSpeedEvaluationGraphCtl.h"

wxSpeedEvaluationGraphCtl::wxSpeedEvaluationGraphCtl(wxWindow *parent, wxWindowID id,
                                                     wxChartsCategoricalData::ptr &all_data,
                                                     wxChartsDoubleDataset::ptr &ref_line_speed_ds,
                                                     wxChartsDoubleDataset::ptr &participant_speed_ds,
                                                     wxChartsDoubleDataset::ptr &lowerbound_ds,
                                                     wxChartsDoubleDataset::ptr &upperbound_ds,
                                                     const wxChartsLineType &lineType, const wxPoint &pos,
                                                     const wxSize &size, long style, int bound_sample_offset) : wxChartCtrl(parent, id, pos,
                                                                                                   size, style),
                                                                                       speed_evaluation_graph(
                                                                                               all_data, lineType,
                                                                                               size, ref_line_speed_ds,
                                                                                               participant_speed_ds,
                                                                                               lowerbound_ds, upperbound_ds, bound_sample_offset) {
}

wxSpeedEvaluationGraphCtl::wxSpeedEvaluationGraphCtl(wxWindow *parent, wxWindowID id,
                                                     wxChartsCategoricalData::ptr &all_data,
                                                     wxChartsDoubleDataset::ptr &ref_line_speed_ds,
                                                     wxChartsDoubleDataset::ptr &participant_speed_ds,
                                                     wxChartsDoubleDataset::ptr &lowerbound_ds,
                                                     wxChartsDoubleDataset::ptr &upperbound_ds,
                                                     const wxChartsLineType &lineType,
                                                     const wxLineChartOptions &options, const wxPoint &pos,
                                                     const wxSize &size, long style, int bound_sample_offset) : wxChartCtrl(parent, id, pos,
                                                                                                   size, style),
                                                                                       speed_evaluation_graph(
                                                                                               all_data, lineType,
                                                                                               size, ref_line_speed_ds,
                                                                                               participant_speed_ds,
                                                                                               lowerbound_ds, upperbound_ds, bound_sample_offset) {
}

wxChart &wxSpeedEvaluationGraphCtl::GetChart() {
    return speed_evaluation_graph;
}
