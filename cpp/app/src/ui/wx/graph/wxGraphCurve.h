#ifndef QUALISYSKINOVAINTERFACE_WXGRAPHCURVE_H
#define QUALISYSKINOVAINTERFACE_WXGRAPHCURVE_H


#include "wx/charts/wxchartsdoubledataset.h"

class wxGraphCurve : public wxChartsDoubleDataset {

public:
    wxGraphCurve(const wxString &name, const wxVector<wxDouble> &data, int dataset_id, bool show_dots,
                 bool enable_tooltip, bool fill_under, wxColour line_color);

    int dataset_id;
    bool show_dots;
    bool enable_tooltip;
    bool fill_under;
    wxColour line_color;
};


#endif //QUALISYSKINOVAINTERFACE_WXGRAPHCURVE_H
