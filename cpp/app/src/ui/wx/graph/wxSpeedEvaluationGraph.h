#ifndef QUALISYSKINOVAINTERFACE_WXSPEEDEVALUATIONGRAPH_H
#define QUALISYSKINOVAINTERFACE_WXSPEEDEVALUATIONGRAPH_H

#include <sstream>
#include "wx/charts/wxlinechart.h"
#include "wx/charts/wxchartsdatasettheme.h"
#include "wx/charts/wxchartstheme.h"

const static wxColour SPEED_LOWERBOUND_COLOR = wxColour(100, 2, 101);
const static wxColour SPEED_UPPERBOUND_COLOR = wxColour(227, 93, 29);

class wxSpeedEvaluationGraph : public wxLineChart {

public:

    wxSpeedEvaluationGraph(wxChartsCategoricalData::ptr &all_data, const wxChartsLineType &lineType,
                           const wxSize &size, wxChartsDoubleDataset::ptr &ref_line_speed_ds, wxChartsDoubleDataset::ptr &participant_speed_ds,
                           wxChartsDoubleDataset::ptr & lowerbound_dataset, wxChartsDoubleDataset::ptr & upperbound_dataset, int bound_sample_offset);

private:
    void
    AddDataset(wxChartsDoubleDataset::ptr &dataset, int dataset_id, bool show_dots = true, bool enable_tooltip = false,
               bool fill_under = true, const wxColor *line_color = nullptr, int bound_sample_offset = 0);

    void DoDraw(wxGraphicsContext &gc,
                             bool suppressTooltips) override;
    std::vector<int> graph_x_offset = std::vector<int>(4); //todo: temporary solution

};


#endif //QUALISYSKINOVAINTERFACE_WXSPEEDEVALUATIONGRAPH_H
