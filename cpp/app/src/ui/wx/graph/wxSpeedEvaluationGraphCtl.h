#ifndef QUALISYSKINOVAINTERFACE_WXSPEEDEVALUATIONGRAPHCTL_H
#define QUALISYSKINOVAINTERFACE_WXSPEEDEVALUATIONGRAPHCTL_H

#include "wx/charts/wxchartscategoricaldata.h"
#include "wx/charts/wxlinechart.h"
#include "wx/charts/wxlinechartctrl.h"
#include "wxSpeedEvaluationGraph.h"

class wxSpeedEvaluationGraphCtl : public wxChartCtrl {
public:

    wxSpeedEvaluationGraphCtl(wxWindow *parent, wxWindowID id, wxChartsCategoricalData::ptr &all_data,
                              wxChartsDoubleDataset::ptr &ref_line_speed_ds,
                              wxChartsDoubleDataset::ptr &participant_speed_ds,
                              wxChartsDoubleDataset::ptr &lowerbound_ds, wxChartsDoubleDataset::ptr &upperbound_ds,
                              const wxChartsLineType &lineType, const wxPoint &pos = wxDefaultPosition,
                              const wxSize &size = wxDefaultSize, long style = 0, int bound_sample_offset = 0);

    wxSpeedEvaluationGraphCtl(wxWindow *parent, wxWindowID id, wxChartsCategoricalData::ptr &all_data,
                              wxChartsDoubleDataset::ptr &ref_line_speed_ds,
                              wxChartsDoubleDataset::ptr &participant_speed_ds,
                              wxChartsDoubleDataset::ptr &lowerbound_ds, wxChartsDoubleDataset::ptr &upperbound_ds,
                              const wxChartsLineType &lineType, const wxLineChartOptions &options,
                              const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
                              long style = 0, int bound_sample_offset = 0);

private:
    wxChart &GetChart() override;

private:
    wxSpeedEvaluationGraph speed_evaluation_graph;

};


#endif //QUALISYSKINOVAINTERFACE_WXSPEEDEVALUATIONGRAPHCTL_H
