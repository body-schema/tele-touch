#include "wxSpeedEvaluationGraph.h"
#include <wx/brush.h>

wxSpeedEvaluationGraph::wxSpeedEvaluationGraph(wxChartsCategoricalData::ptr &all_data,
                                               const wxChartsLineType &lineType,
                                               const wxSize &size, wxChartsDoubleDataset::ptr &ref_line_speed_ds,
                                               wxChartsDoubleDataset::ptr &participant_speed_ds, wxChartsDoubleDataset::ptr & lowerbound_dataset,
                                               wxChartsDoubleDataset::ptr & upperbound_dataset, int bound_sample_offset) : wxLineChart(all_data, lineType, size) {
    m_datasets.clear();
    AddDataset(ref_line_speed_ds, 1, true, false, true);
    graph_x_offset[0] = 0;
    AddDataset(participant_speed_ds, 0, true, true, true);
    graph_x_offset[1] = 0;

    AddDataset(lowerbound_dataset, 2, false, false, false, &SPEED_LOWERBOUND_COLOR, bound_sample_offset);
    graph_x_offset[2] = bound_sample_offset;
    AddDataset(upperbound_dataset, 3, false, false, false, &SPEED_UPPERBOUND_COLOR, bound_sample_offset);
    graph_x_offset[3] = bound_sample_offset;
}

void wxSpeedEvaluationGraph::AddDataset(wxChartsDoubleDataset::ptr &dataset, int dataset_id, bool show_dots,
                                        bool enable_tooltip, bool fill_under,
                                        const wxColor *line_color, int bound_sample_offset) {
    wxSharedPtr<wxChartsDatasetTheme> datasetTheme = wxChartsDefaultTheme->GetDatasetTheme(
            wxChartsDatasetId::CreateImplicitId(dataset_id));
    wxSharedPtr<wxLineChartDatasetOptions> datasetOptions = datasetTheme->GetLineChartDatasetOptions();


    Dataset::ptr newDataset(new Dataset(show_dots,
                                        datasetOptions->ShowLine(),
                                        line_color == nullptr ? datasetOptions->GetLineColor() : *line_color,
                                        fill_under, fill_under ? datasetOptions->GetFillColor() : wxColour(0, 0, 0, 0),
                                        m_lineType));

    const wxVector<wxDouble> &datasetData = dataset->GetData();
    for (size_t i = 0; i < datasetData.size(); ++i) {
        std::stringstream tooltip;
        tooltip << datasetData[i];
        wxSharedPtr<wxChartTooltipProvider> tooltipProvider(
                new wxChartTooltipProviderStatic("", tooltip.str(),
                                                 datasetOptions->GetLineColor())
        );

        Point::ptr point(
                new Point(datasetData[i], tooltipProvider, 20 + i * 10, 0,
                          datasetOptions->GetDotRadius(), datasetOptions->GetDotPenOptions().GetWidth(),
                          datasetOptions->GetDotPenOptions().GetColor(),
                          datasetOptions->GetDotBrushOptions().GetColor(),
                          enable_tooltip ? m_options->GetHitDetectionRange() : 0.0)
        );

        newDataset->AppendPoint(point);
    }

    m_datasets.push_back(newDataset);
}

void wxSpeedEvaluationGraph::DoDraw(wxGraphicsContext &gc, bool suppressTooltips) {
    m_grid.Fit(gc);
    m_grid.Draw(gc);

    for (size_t i = 0; i < m_datasets.size(); ++i)
    {
        const wxVector<Point::ptr>& points = m_datasets[i]->GetPoints();

        wxGraphicsPath path = gc.CreatePath();

        if (points.size() > 0)
        {
            const Point::ptr& point = points[0];
            wxPoint2DDouble firstPosition = m_grid.GetMapping().GetWindowPositionAtTickMark(graph_x_offset[i], point->GetValue());
            path.MoveToPoint(firstPosition);

            wxPoint2DDouble lastPosition;
            for (size_t j = 1; j < points.size(); ++j)
            {
                const Point::ptr& point = points[j];
                lastPosition = m_grid.GetMapping().GetWindowPositionAtTickMark(graph_x_offset[i] + j, point->GetValue());
                if (m_datasets[i]->GetType() == wxCHARTSLINETYPE_STEPPED)
                {
                    wxPoint2DDouble temp = m_grid.GetMapping().GetWindowPositionAtTickMark(graph_x_offset[i] + j, points[j - 1]->GetValue());
                    path.AddLineToPoint(temp);
                }
                path.AddLineToPoint(lastPosition);
            }

            if (m_datasets[i]->ShowLine())
            {
                wxSharedPtr<wxChartsDatasetTheme> datasetTheme = wxChartsDefaultTheme->GetDatasetTheme(wxChartsDatasetId::CreateImplicitId(i));
                wxSharedPtr<wxLineChartDatasetOptions> datasetOptions = datasetTheme->GetLineChartDatasetOptions();

                wxPen pen(m_datasets[i]->GetLineColor(), datasetOptions->GetLineWidth());
                gc.SetPen(pen);
            }
            else
            {
                // TODO : transparent pen
            }

            gc.StrokePath(path);

            wxPoint2DDouble yPos = m_grid.GetMapping().GetXAxis().GetTickMarkPosition(0);

            path.AddLineToPoint(lastPosition.m_x, yPos.m_y);
            path.AddLineToPoint(firstPosition.m_x, yPos.m_y);
            path.CloseSubpath();

            wxBrush brush(m_datasets[i]->GetFillColor());
            gc.SetBrush(brush);
            gc.FillPath(path);
        }

        if (m_datasets[i]->ShowDots())
        {
            for (size_t j = 0; j < points.size(); ++j)
            {
                const Point::ptr& point = points[j];
                point->SetPosition(m_grid.GetMapping().GetWindowPositionAtTickMark(graph_x_offset[i]+ j, point->GetValue()));
                point->Draw(gc);
            }
        }
    }

    if (!suppressTooltips)
    {
        DrawTooltips(gc);
    }
}
