#include "Line.h"

Line::Line(int length, int x_start, int y_start, int x_end, int y_end, const wxColour &colour) :
        length(length), colour(colour), x_start(x_start), y_start(y_start), x_end(x_end), y_end(y_end) {

}

const wxColour &Line::GetColour() const {
    return colour;
}

void Line::SetColour(const wxColour &colour) {
    Line::colour = colour;
}

int Line::GetLength() const {
    return length;
}

void Line::SetLength(int length) {
    Line::length = length;
}

int Line::GetXStart() const {
    return x_start;
}

void Line::SetXStart(int xStart) {
    x_start = xStart;
}

int Line::GetYStart() const {
    return y_start;
}

void Line::SetYStart(int yStart) {
    y_start = yStart;
}

int Line::GetXEnd() const {
    return x_end;
}

void Line::SetXEnd(int xEnd) {
    x_end = xEnd;
}

int Line::GetYEnd() const {
    return y_end;
}

void Line::SetYEnd(int yEnd) {
    y_end = yEnd;
}
