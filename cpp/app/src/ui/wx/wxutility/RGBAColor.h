#ifndef QUALISYSKINOVAINTERFACE_RGBACOLOR_H
#define QUALISYSKINOVAINTERFACE_RGBACOLOR_H


#include <tuple>
#include <wx/colour.h>

typedef std::tuple<int, int, int, int> RGBAColor; // r, g, b, alpha

#endif //QUALISYSKINOVAINTERFACE_RGBACOLOR_H
