#ifndef QUALISYSKINOVAINTERFACE_SCREENRECTANGLE_H
#define QUALISYSKINOVAINTERFACE_SCREENRECTANGLE_H


#include "Rectangle.h"

class ScreenRectangle : public Rectangle {
public:
    ScreenRectangle(int width, int height, int x, int y, RGBAColor colour = {0, 0, 0, 0}, bool withTransparency = false);

    ScreenRectangle(const Rectangle &rectangle, int x, int y);

    int GetX() const;

    void SetX(int x);

    int GetY() const;

    void SetY(int y);

private:
    int x, y;
};


#endif //QUALISYSKINOVAINTERFACE_SCREENRECTANGLE_H
