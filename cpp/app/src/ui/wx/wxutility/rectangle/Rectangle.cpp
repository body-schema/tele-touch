#include "Rectangle.h"

Rectangle::Rectangle(double width, double height, MeasurementUnit measurement_unit, RGBAColor colour, bool withTransparency) :
        width{width}, height{height}, colour(colour),
        withTransparencyGradient(withTransparency), measurement_unit{measurement_unit} {

}

Rectangle::Rectangle(const Rectangle &rectangle, double CM_TO_PX_RATE):
        width(static_cast<int>(rectangle.width * CM_TO_PX_RATE)), height(static_cast<int>(rectangle.height * CM_TO_PX_RATE)), colour(rectangle.colour),
        withTransparencyGradient(rectangle.withTransparencyGradient), measurement_unit{MeasurementUnit::PX} {

}

void Rectangle::SetTransparencyGradient(bool withTransparencyGradient) {
    this->withTransparencyGradient = withTransparencyGradient;
}

void Rectangle::SetColour(RGBAColor newColour) {
    this->colour = newColour;
}

const RGBAColor &Rectangle::GetColour() const {
    return colour;
}

bool Rectangle::WithTransparencyGradient() const {
    return withTransparencyGradient;
}

double Rectangle::GetWidth() const {
    return width;
}

void Rectangle::SetWidth(double width) {
    Rectangle::width = width;
}

double Rectangle::GetHeight() const {
    return height;
}

void Rectangle::SetHeight(double height) {
    Rectangle::height = height;
}

void Rectangle::SetTransparency(int transparency) {
    std::get<3>(colour) = transparency;
}
