#include "ScreenRectangle.h"

ScreenRectangle::ScreenRectangle(int width, int height, int x, int y, const RGBAColor colour, bool withTransparency)
        : Rectangle(width, height, MeasurementUnit::PX, colour, withTransparency), x(x), y(y) {

}

ScreenRectangle::ScreenRectangle(const Rectangle &rectangle, int x, int y) : Rectangle(
        rectangle.GetWidth(),
        rectangle.GetHeight(),
        MeasurementUnit::PX,
        rectangle.GetColour(),
        rectangle.WithTransparencyGradient()),
                                                                                   x(x), y(y) {

}

int ScreenRectangle::GetX() const {
    return x;
}

void ScreenRectangle::SetX(int x) {
    ScreenRectangle::x = x;
}

int ScreenRectangle::GetY() const {
    return y;
}

void ScreenRectangle::SetY(int y) {
    ScreenRectangle::y = y;
}
