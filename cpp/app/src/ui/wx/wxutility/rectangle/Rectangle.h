#ifndef QUALISYSKINOVAINTERFACE_RECTANGLE_H
#define QUALISYSKINOVAINTERFACE_RECTANGLE_H

#include <wx/wx.h>
#include "MeasurementUnit.h"
#include "src/ui/wx/wxutility/RGBAColor.h"

class Rectangle {
public:
    Rectangle(double width, double height, MeasurementUnit measurement_unit, RGBAColor colour = {0, 0, 0, 0},
              bool withTransparency = false);

    Rectangle(const Rectangle &rectangle, double CM_TO_PX_RATE);

    void SetColour(RGBAColor newColour);

    void SetTransparencyGradient(bool withTransparencyGradient);

    const RGBAColor &GetColour() const;

    bool WithTransparencyGradient() const;

    double GetWidth() const;

    void SetWidth(double width_cm);

    double GetHeight() const;

    void SetHeight(double height_cm);

    void SetTransparency(int transparency);

    bool isTakeMaxWidth() const;

    void setTakeMaxWidth(bool takeMaxWidth);

    bool isTakeMaxHeight() const;

    void setTakeMaxHeight(bool takeMaxHeight);

private:
    RGBAColor colour;
    bool withTransparencyGradient;
    double width, height;
    MeasurementUnit measurement_unit;
};

#endif //QUALISYSKINOVAINTERFACE_RECTANGLE_H
