#ifndef QUALISYSKINOVAINTERFACE_LINE_H
#define QUALISYSKINOVAINTERFACE_LINE_H


#include <wx/wx.h>

class Line {
public:
    Line(int length, int x_start, int y_start, int x_end, int y_end, const wxColour &colour = *wxBLACK);

    const wxColour &GetColour() const;

    void SetColour(const wxColour &colour);

    int GetLength() const;

    void SetLength(int length);

    int GetXStart() const;

    void SetXStart(int xStart);

    int GetYStart() const;

    void SetYStart(int yStart);

    int GetXEnd() const;

    void SetXEnd(int xEnd);

    int GetYEnd() const;

    void SetYEnd(int yEnd);

private:
    wxColour colour;
    int length, x_start, y_start, x_end, y_end;
};


#endif //QUALISYSKINOVAINTERFACE_LINE_H
