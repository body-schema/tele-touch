#ifndef QUALISYSKINOVAINTERFACE_COLORCONVERTER_H
#define QUALISYSKINOVAINTERFACE_COLORCONVERTER_H

#include <wx/colour.h>
#include "RGBAColor.h"

static inline wxColor RGBA_to_wxColor(const RGBAColor &rgbaColor) {
    return wxColour(std::get<0>(rgbaColor), std::get<1>(rgbaColor), std::get<2>(rgbaColor), std::get<3>(rgbaColor));
}

static inline RGBAColor wxColor_to_RGBA(const wxColour &wxColour) {
    return {wxColour.Red(), wxColour.Green(), wxColour.Blue(), wxColour.Alpha()};
}

#endif //QUALISYSKINOVAINTERFACE_COLORCONVERTER_H
