#include "TextStyle.h"

const RGBAColor &TextStyle::GetColour() const {
    return colour;
}

void TextStyle::SetColour(const RGBAColor colour) {
    TextStyle::colour = colour;
}

int TextStyle::GetHeightPx() const {
    return height_px;
}

void TextStyle::SetHeightPx(int heightPx) {
    height_px = heightPx;
}

TextStyle::TextStyle(const RGBAColor &colour, int heightPx) : colour(colour), height_px(heightPx) {}

TextStyle::TextStyle(const RGBAColor &colour) : colour(colour) {

}
