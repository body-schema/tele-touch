#ifndef QUALISYSKINOVAINTERFACE_TEXTSTYLE_H
#define QUALISYSKINOVAINTERFACE_TEXTSTYLE_H


#include "RGBAColor.h"

class TextStyle {

private:
public:

    TextStyle(const RGBAColor &colour);

    TextStyle(const RGBAColor &colour, int heightPx);

    void SetColour(RGBAColor newColour);

    const RGBAColor &GetColour() const;

    int GetHeightPx() const;

    void SetHeightPx(int heightPx);

private:
    RGBAColor colour;
    int height_px = 0;
};


#endif //QUALISYSKINOVAINTERFACE_TEXTSTYLE_H
