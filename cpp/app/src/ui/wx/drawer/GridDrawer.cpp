#include "GridDrawer.h"

GridDrawer::GridDrawer(wxDC &dc) : CustomDrawer(dc) {

}

GridDrawer::GridDrawer(wxDC &dc, short x_grid_size, short y_grid_size) : CustomDrawer(dc) {
    SetXGridSize(x_grid_size);
    SetYGridSize(y_grid_size);
}

short GridDrawer::GetXGridSize() const {
    return x_grid_size;
}

void GridDrawer::SetXGridSize(short xGridSize) {
    int window_width, window_height;
    this->getDc().GetSize(&window_width, &window_height);
    x_grid_size = xGridSize;
    x_grid_ceil_size = (double) window_width / x_grid_size;
}

short GridDrawer::GetYGridSize() const {
    return y_grid_size;
}

void GridDrawer::SetYGridSize(short yGridSize) {
    int window_width, window_height;
    this->getDc().GetSize(&window_width, &window_height);
    y_grid_size = yGridSize;
    y_grid_ceil_size = (double) window_height / y_grid_size;
}

void GridDrawer::UpdateCeilSize() {
    int window_width, window_height;
    this->getDc().GetSize(&window_width, &window_height);
    y_grid_ceil_size = (double) window_height / y_grid_size;
    x_grid_ceil_size = (double) window_width / x_grid_size;
}

void GridDrawer::DrawRectangleByGrid(const Rectangle &rectangle, short x_grid_pos, short y_grid_pos, bool isXCentered,
                                     bool isYCentered, enum GridDrawType gridDrawType) {
    int x_pos, y_pos;
    if (!isXCentered) {
        if (gridDrawType == FROM_CEIL_TOP_LEFT_ANGLE) {
            x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos);
        } else if (gridDrawType == TO_CEIL_TOP_LEFT_ANGLE) {
            x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos) - rectangle.GetWidth();
        } else if (gridDrawType == CENTERED_BY_CEIL_LEFT_BORDER) {
            x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos) -
                    (rectangle.GetWidth() / 2.0);
        }
    }
    if (!isYCentered) {
        y_pos = y_grid_ceil_size * (y_grid_pos > y_grid_size ? y_grid_size : y_grid_pos);
    }
    ScreenRectangle aligned_rectangle = ScreenRectangle(rectangle, x_pos, y_pos);
    this->DrawRectangle(aligned_rectangle, isXCentered, isYCentered);
}

void GridDrawer::DrawRectangleByAxisXGrid(const Rectangle &rectangle, short x_grid_pos, int y_px_pos, bool isXCentered,
                                          enum GridDrawType gridDrawType) {
    int x_pos;
    if (!isXCentered) {
        if (gridDrawType == FROM_CEIL_TOP_LEFT_ANGLE) {
            x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos);
        } else if (gridDrawType == TO_CEIL_TOP_LEFT_ANGLE) {
            x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos) - rectangle.GetWidth();
        } else if (gridDrawType == CENTERED_BY_CEIL_LEFT_BORDER) {
            x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos) -
                    (rectangle.GetWidth() / 2.0);
        }
    }
    ScreenRectangle aligned_rectangle = ScreenRectangle(rectangle, x_pos, y_px_pos);
    this->DrawRectangle(aligned_rectangle, isXCentered, false);
}

void GridDrawer::DrawTextLineByGreed(const std::string text_line, short x_grid_pos, short y_grid_pos,
                                     enum GridDrawType gridDrawType, const wxColour &text_color, int text_height_px) {
    auto dc_font = dc.GetFont();
    dc_font.SetPixelSize(wxSize(0, text_height_px));
    dc.SetFont(dc_font);
    dc.SetTextForeground(text_color);
    int x_pos, y_pos;
    wxString wx_text = wxString::FromUTF8(text_line);
    // Get the size of the text
    wxSize textSize = dc.GetTextExtent(wx_text);
    if (gridDrawType == FROM_CEIL_TOP_LEFT_ANGLE) {
        x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos);
    } else if (gridDrawType == TO_CEIL_TOP_LEFT_ANGLE) {
        x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos) - textSize.GetWidth();
    } else if (gridDrawType == CENTERED_BY_CEIL_LEFT_BORDER) {
        x_pos = x_grid_ceil_size * (x_grid_pos > x_grid_size ? x_grid_size : x_grid_pos) -
                (textSize.GetWidth() / 2.0);
    }
    y_pos = y_grid_ceil_size * (y_grid_pos > y_grid_size ? y_grid_size : y_grid_pos);
    dc.DrawText(wx_text, x_pos, y_pos);
}
