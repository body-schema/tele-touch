#ifndef QUALISYSKINOVAINTERFACE_CUSTOMDRAWER_H
#define QUALISYSKINOVAINTERFACE_CUSTOMDRAWER_H

#include <wx/wx.h>
#include <list>
#include "src/ui/wx/wxutility/rectangle/Rectangle.h"
#include "src/ui/wx/wxutility/Line.h"
#include "src/ui/wx/wxutility/rectangle/ScreenRectangle.h"
#include "src/ui/wx/wxutility/TextStyle.h"

class CustomDrawer {

public:
    explicit CustomDrawer(wxDC &dc);

    //void drawLine(int x1, int y1, int x2, int y2, const string &lineText = std::string());

    wxDC &getDc() const;

    void DrawRectangle(const ScreenRectangle &rectangle, bool isXCentered = false, bool isYCentered = false);

    void DrawLine(Line &line, bool isXCentered = false,
                  bool isYCentered = false);

    void DrawImage(const std::string &file_path, const wxBitmapType format);

    void DrawCenteredText(const std::vector<std::string> &text_lines, const wxColour &text_color = *wxBLACK, int text_height_px = 24);

    void DrawCenteredTextOnTop(std::vector<std::pair<const std::string, const TextStyle>> &lines_and_style_list, int margin_top = 14);

protected:
    wxDC &dc;

private:

    void DrawRectangleWithTransparencyGradient(const Rectangle &rectangle, int x_pos, int y_pos);
};


#endif //QUALISYSKINOVAINTERFACE_CUSTOMDRAWER_H
