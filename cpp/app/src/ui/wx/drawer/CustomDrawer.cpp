#include "CustomDrawer.h"
#include "src/ui/wx/wxutility/ColorConverter.h"

CustomDrawer::CustomDrawer(wxDC &dc) : dc(dc) {

}

void
CustomDrawer::DrawRectangle(const ScreenRectangle &rectangle, bool isXCentered, bool isYCentered) {
    auto previous_brush = dc.GetBrush();
    dc.SetBrush(RGBA_to_wxColor(rectangle.GetColour()));
    if (isXCentered || isYCentered) {
        int window_width, window_height;
        this->getDc().GetSize(&window_width, &window_height);
        if (isXCentered) {
            if (rectangle.WithTransparencyGradient()) {
                this->DrawRectangleWithTransparencyGradient(rectangle, (window_width - rectangle.GetWidth()) / 2,
                                                            rectangle.GetY());
            } else {
                dc.DrawRectangle((window_width - rectangle.GetWidth()) / 2, rectangle.GetY(), rectangle.GetWidth(),
                                 rectangle.GetHeight());
            }
        }
        if (isYCentered) {
            if (rectangle.WithTransparencyGradient()) {
                this->DrawRectangleWithTransparencyGradient(rectangle, rectangle.GetX(),
                                                            (window_height - rectangle.GetHeight()) / 2);
            } else {
                dc.DrawRectangle(rectangle.GetX(), (window_height - rectangle.GetHeight()) / 2, rectangle.GetWidth(),
                                 rectangle.GetHeight());
            }
        }
    } else {
        if (rectangle.WithTransparencyGradient()) {
            this->DrawRectangleWithTransparencyGradient(rectangle, rectangle.GetX(), rectangle.GetY());
        } else {
            dc.DrawRectangle(rectangle.GetX(), rectangle.GetY(), rectangle.GetWidth(), rectangle.GetHeight());
        }
    }
    dc.SetBrush(previous_brush);
}

void CustomDrawer::DrawRectangleWithTransparencyGradient(const Rectangle &rectangle, int x_pos, int y_pos) {
    int transparent_part_length = rectangle.GetHeight() / 10;
    if (transparent_part_length > 0) {
        double transparency_growing_step = 255.0 / transparent_part_length;
        double transparency = 0;
        for (int rectangle_y_part = 0; rectangle_y_part < transparent_part_length; ++rectangle_y_part) {
            transparency += transparency_growing_step;
            auto rectangle_part_color = rectangle.GetColour();
            std::get<3>(rectangle_part_color) = round(transparency);
            dc.SetBrush(RGBA_to_wxColor(rectangle_part_color));
            dc.DrawRectangle(x_pos, y_pos + rectangle_y_part, rectangle.GetWidth(), 1);
        }
        transparency = 255;
        auto rectangle_part_color = rectangle.GetColour();
        std::get<3>(rectangle_part_color) = round(transparency);
        dc.SetBrush(RGBA_to_wxColor(rectangle_part_color));
        dc.DrawRectangle(x_pos, y_pos + transparent_part_length, rectangle.GetWidth(),
                         rectangle.GetHeight() - transparent_part_length * 2);
        for (int rectangle_y_part = rectangle.GetHeight() - transparent_part_length;
             rectangle_y_part <= rectangle.GetHeight(); ++rectangle_y_part) {
            if (transparency > 1.0) {
                transparency -= transparency_growing_step;
            } else {
                transparency = 0;
            }
            auto rectangle_part_color = rectangle.GetColour();
            std::get<3>(rectangle_part_color) = round(transparency);
            dc.SetBrush(RGBA_to_wxColor(rectangle_part_color));
            dc.DrawRectangle(x_pos, y_pos + rectangle_y_part, rectangle.GetWidth(), 1);
        }
    } else {
        dc.SetBrush(RGBA_to_wxColor(rectangle.GetColour()));
        dc.DrawRectangle(x_pos, y_pos, rectangle.GetWidth(), rectangle.GetHeight());
    }
}

wxDC &CustomDrawer::getDc() const {
    return dc;
}

void CustomDrawer::DrawLine(Line &line, bool isXCentered, bool isYCentered) {
    auto previous_brush = dc.GetBrush();
    dc.SetBrush(line.GetColour());
    if (isXCentered) {
        int window_width, window_height;
        this->getDc().GetSize(&window_width, &window_height);
        dc.DrawLine(window_width / 2, line.GetYStart(), window_width / 2, line.GetYEnd());
    } else if (isYCentered) {
        int window_width, window_height;
        this->getDc().GetSize(&window_width, &window_height);
        dc.DrawLine(line.GetXStart(), window_height / 2, line.GetXEnd(), window_height / 2);
    } else {
        dc.DrawLine(line.GetXStart(), line.GetYStart(), line.GetXEnd(), line.GetYEnd());
    }
    dc.SetBrush(previous_brush);
}


void CustomDrawer::DrawImage(const std::string &file_path, const wxBitmapType format) {
    wxBitmap image = wxBitmap();
    image.LoadFile(wxString::FromUTF8(file_path), format);
    int panel_width, panel_height;
    dc.GetSize(&panel_width, &panel_height);
    wxBitmapBase::Rescale(image, {panel_width, panel_height});
    dc.DrawBitmap(image, 0, 0, false);
}

void
CustomDrawer::DrawCenteredText(const std::vector<std::string> &text_lines, const wxColour &text_color, int text_height_px) {
    int window_width, window_height;
    dc.GetSize(&window_width, &window_height);
    auto dc_font = dc.GetFont();
    dc_font.SetPixelSize(wxSize(0, text_height_px));
    dc.SetFont(dc_font);
    dc.SetTextForeground(text_color);

    if (text_lines.size() == 1) {
        const auto& text = text_lines.at(0);
        wxString wx_text = wxString::FromUTF8(text);
        // Get the size of the text
        wxSize textSize = dc.GetTextExtent(wx_text);
        // Calculate the position to center the text
        int x_centered = (window_width - textSize.GetWidth()) / 2;
        int y_centered = (window_height - textSize.GetHeight()) / 2;
        dc.DrawText(wx_text, x_centered, y_centered);
    } else {
        std::string concatinated_string = "";
        {
            int line_number = 0;
            for (const auto& text_line : text_lines) {
                concatinated_string += text_line + ((line_number == text_lines.size() - 1) ? "" : "\n");
                line_number++;
            }
        }
        wxString wx_text = wxString::FromUTF8(concatinated_string);
        // Get the size of the text
        wxSize textSize = dc.GetTextExtent(wx_text);
        // Calculate the position to center the text
        int x_center = window_width / 2;
        int text_upper_border = (window_height - textSize.GetHeight()) / 2;
        int text_lower_border = text_upper_border;
        for (const auto& text_line : text_lines) {
            wxString wx_text_line = wxString::FromUTF8(text_line);
            wxSize text_line_size = dc.GetTextExtent(wx_text_line);
            dc.DrawText(wx_text_line, x_center - (text_line_size.GetWidth() / 2), text_lower_border);
            text_lower_border += text_line_size.GetHeight();
        }
    }
}

void
CustomDrawer::DrawCenteredTextOnTop(std::vector<std::pair<const std::string, const TextStyle>> &lines_and_style_list, int margin_top) {
    for (const auto& pair : lines_and_style_list) {
        int window_width, window_height;
        dc.GetSize(&window_width, &window_height);

        if (lines_and_style_list.size() == 1) {
            const auto& text_and_style = lines_and_style_list.at(0);
            auto dc_font = dc.GetFont();
            dc_font.SetPixelSize(wxSize(0, text_and_style.second.GetHeightPx()));
            dc.SetFont(dc_font);
            dc.SetTextForeground(RGBA_to_wxColor(text_and_style.second.GetColour()));
            wxString wx_text = wxString::FromUTF8(text_and_style.first);
            // Get the size of the text
            wxSize textSize = dc.GetTextExtent(wx_text);
            // Calculate the position to center the text
            int x_centered = (window_width - textSize.GetWidth()) / 2;
            int y_top = margin_top;
            dc.DrawText(wx_text, x_centered, y_top);
        } else {
            std::string concatinated_string = "";
            {
                int line_number = 0;
                for (const auto& text_and_style : lines_and_style_list) {
                    concatinated_string += text_and_style.first + ((line_number == lines_and_style_list.size() - 1) ? "" : "\n");
                    line_number++;
                }
            }
            wxString wx_text = wxString::FromUTF8(concatinated_string);
            // Get the size of the text
            wxSize textSize = dc.GetTextExtent(wx_text);
            // Calculate the position to center the text
            int x_center = window_width / 2;
            int text_upper_border = margin_top;
            int text_lower_border = text_upper_border;
            for (const auto& text_and_style : lines_and_style_list) {
                auto dc_font = dc.GetFont();
                dc_font.SetPixelSize(wxSize(0, text_and_style.second.GetHeightPx()));
                dc.SetFont(dc_font);
                dc.SetTextForeground(RGBA_to_wxColor(text_and_style.second.GetColour()));
                wxString wx_text_line = wxString::FromUTF8(text_and_style.first);
                wxSize text_line_size = dc.GetTextExtent(wx_text_line);
                dc.DrawText(wx_text_line, x_center - (text_line_size.GetWidth() / 2), text_lower_border);
                text_lower_border += text_line_size.GetHeight();
            }
        }
    }

}
