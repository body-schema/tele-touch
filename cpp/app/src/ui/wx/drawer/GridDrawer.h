#ifndef QUALISYSKINOVAINTERFACE_GRIDDRAWER_H
#define QUALISYSKINOVAINTERFACE_GRIDDRAWER_H


#include "CustomDrawer.h"

enum GridDrawType {
    FROM_CEIL_TOP_LEFT_ANGLE, TO_CEIL_TOP_LEFT_ANGLE, CENTERED_BY_CEIL_LEFT_BORDER
};

class GridDrawer : public CustomDrawer {

public:
    GridDrawer(wxDC &dc);

    GridDrawer(wxDC &dc, short x_grid_size, short y_grid_size);

    short GetXGridSize() const;

    void SetXGridSize(short xGridSize);

    short GetYGridSize() const;

    void SetYGridSize(short yGridSize);

    void DrawRectangleByGrid(const Rectangle &rectangle, short x_grid_pos, short y_grid_pos, bool isXCentered = false,
                             bool isYCentered = false, enum GridDrawType gridDrawType = FROM_CEIL_TOP_LEFT_ANGLE);

    void DrawRectangleByAxisXGrid(const Rectangle &rectangle, short x_grid_pos, int y_px_pos, bool isXCentered = false, enum GridDrawType gridDrawType = FROM_CEIL_TOP_LEFT_ANGLE);

    void DrawTextLineByGreed(const std::string text_line, short x_grid_pos, short y_grid_pos, enum GridDrawType gridDrawType = FROM_CEIL_TOP_LEFT_ANGLE, const wxColour &text_color = *wxBLACK, int text_height_px = 24);

    void UpdateCeilSize();

private:
    short x_grid_size, y_grid_size;
    double x_grid_ceil_size, y_grid_ceil_size;

};


#endif //QUALISYSKINOVAINTERFACE_GRIDDRAWER_H
