#include "EditParticipantDialog.h"

EditParticipantDialog::EditParticipantDialog(wxWindow *parent, wxParticipantInfo &participantInfoArg,
                                             std::string &savedFilePathArg)
        : wxDialog(parent, wxID_ANY, "New participant", wxDefaultPosition, wxDefaultSize,
                   wxDEFAULT_DIALOG_STYLE), savedFilePath(savedFilePathArg), participantInfo(participantInfoArg) {
    SetLayoutAdaptationMode(wxDIALOG_ADAPTATION_MODE_ENABLED);

    initializeParticipantInput();
    initializeEventHandlers();
    initializeMainSizer();
}

void EditParticipantDialog::initializeParticipantInput() {
    participantInput = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0,
                                      wxTextValidator(wxFILTER_ALPHANUMERIC,
                                                      &participantInfo.participantCodename));
    ageInput = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0,
                              wxTextValidator(wxFILTER_DIGITS, &participantInfo.age));
    lengthOfForearmInput = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0,
                                          wxTextValidator(wxFILTER_NUMERIC, &participantInfo.lengthOfForearm));
    sensitivityOfTactileTestInput = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize,
                                                   0, wxTextValidator(wxFILTER_NUMERIC,
                                                                      &participantInfo.sensitivityOfTactileTest));
    descriptionInput = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0,
                                      wxTextValidator(wxFILTER_NONE,
                                                      &participantInfo.description));
    handednessInput->Append("Right-handed");
    handednessInput->Append("Left-handed");
    handednessInput->Append("Ambidextrous");
    handednessInput->SetSelection(0);
    participantInput->SetMaxLength(8);
    wxArrayString array;
    array.Add("AT");
    array.Add("AM");
    array.Add("PT");
    array.Add("PM");
    orderOfTheBlocksInput->SetStrings(array);
}

void EditParticipantDialog::initializeEventHandlers() {
    buttonSave->Bind(wxEVT_BUTTON, [&](wxCommandEvent &event) {
        if (Validate() && TransferDataFromWindow()) {
            wxFileDialog saveFileDialog(this, wxEmptyString, wxEmptyString,
                                        participantInfo.participantCodename + "_info.csv",
                                        "Csv Files (*.csv)|*.csv|All Files (*.*)|*.*", wxFD_SAVE);
            if (saveFileDialog.ShowModal() == wxID_OK) {
                updateParticipantInfo();
                savedFilePath = saveFileDialog.GetPath().ToStdString();
                if (IsModal()) {
                    EndModal(wxID_OK);
                } else {
                    SetReturnCode(wxID_OK);
                    this->Show(false);
                }
            }
        }
    });
}

void EditParticipantDialog::initializeMainSizer() {
    auto *sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(participantLabel, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
    sizer->Add(participantInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(ageLabel, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
    sizer->Add(ageInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(genderLabel, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
    sizer->Add(maleRadioButton, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(femaleRadioButton, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(lengthOfForearmLabel, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
    sizer->Add(lengthOfForearmInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(handednessLabel, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
    sizer->Add(handednessInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(sensitivityOfTactileTestLabel, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
    sizer->Add(sensitivityOfTactileTestInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(orderOfTheBlocksLabel, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
    sizer->Add(orderOfTheBlocksInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(descriptionLabel, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
    sizer->Add(descriptionInput, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->Add(buttonSave, 0, wxEXPAND | wxBOTTOM | wxLEFT | wxRIGHT, 10);
    sizer->SetSizeHints(this);
    this->SetSizer(sizer);
}

void EditParticipantDialog::updateParticipantInfo() {
    participantInfo.gender = maleRadioButton->GetValue() ? "male" : "female";
    auto *tempArrayString = new wxArrayString();
    orderOfTheBlocksInput->GetStrings(*tempArrayString);
    std::string order;
    for (const wxString &tmp: *tempArrayString) {
        order += tmp.ToStdString();
        if (tempArrayString->Last() != tmp) {
            order += "->";
        }
    }
    participantInfo.orderOfTheBlocks = order;
    participantInfo.handedness = handednessInput->GetStringSelection();
    std::string tempString = participantInfo.lengthOfForearm.ToStdString();
    for (char &c: tempString) {
        if (c == ',') {
            c = '.';
        }
    }
    participantInfo.lengthOfForearm = tempString;
    tempString = participantInfo.sensitivityOfTactileTest.ToStdString();
    for (char &c: tempString) {
        if (c == ',') {
            c = '.';
        }
    }
    participantInfo.sensitivityOfTactileTest = tempString;
    tempString = participantInfo.description.ToStdString();
    for (char &c: tempString) {
        if (c == ',') {
            c = ';';
        }
    }
    participantInfo.description = tempString;
}
