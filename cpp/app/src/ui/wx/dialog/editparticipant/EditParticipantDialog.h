#ifndef QUALISYSKINOVAINTERFACE_EDITPARTICIPANTDIALOG_H
#define QUALISYSKINOVAINTERFACE_EDITPARTICIPANTDIALOG_H

#include <wx/wx.h>
#include <wx/editlbox.h>
#include "src/datastruct/wxParticipantInfo.h"
#include <fstream>

class EditParticipantDialog : public wxDialog {
public:
    EditParticipantDialog(wxWindow *parent, wxParticipantInfo &participantInfoArg, std::string &savedFilePathArg);

private:
    wxStaticText *participantLabel = new wxStaticText(this, wxID_ANY, "Participant codename");
    wxTextCtrl *participantInput;

    wxStaticText *ageLabel = new wxStaticText(this, wxID_ANY, "Age");
    wxTextCtrl *ageInput;

    wxStaticText *genderLabel = new wxStaticText(this, wxID_ANY, "Gender:");
    wxRadioButton *maleRadioButton = new wxRadioButton(this, wxID_ANY, "male");
    wxRadioButton *femaleRadioButton = new wxRadioButton(this, wxID_ANY, "female");

    wxStaticText *lengthOfForearmLabel = new wxStaticText(this, wxID_ANY, "Length of forearm (in cm)");
    wxTextCtrl *lengthOfForearmInput;

    wxStaticText *handednessLabel = new wxStaticText(this, wxID_ANY, "Handedness");
    wxChoice *handednessInput = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, {}, 0);

    wxStaticText *sensitivityOfTactileTestLabel = new wxStaticText(this, wxID_ANY, "Sensitivity to tactile test");
    wxTextCtrl *sensitivityOfTactileTestInput;

    wxStaticText *orderOfTheBlocksLabel = new wxStaticText(this, wxID_ANY, "Order of the blocks");
    wxEditableListBox *orderOfTheBlocksInput = new wxEditableListBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
                                                                     wxDefaultSize,
                                                                     0);

    wxStaticText *descriptionLabel = new wxStaticText(this, wxID_ANY, "Description");
    wxTextCtrl *descriptionInput;

    wxButton *buttonSave = new wxButton(this, wxID_ANY, "Save...");
    wxParticipantInfo &participantInfo;
    std::string &savedFilePath;

    void initializeParticipantInput();

    void initializeEventHandlers();

    void initializeMainSizer();

    void updateParticipantInfo();
};


#endif //QUALISYSKINOVAINTERFACE_EDITPARTICIPANTDIALOG_H
