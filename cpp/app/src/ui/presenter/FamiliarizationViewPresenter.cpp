#include <fmt/format.h>

#include <utility>
#include "FamiliarizationViewPresenter.h"
#include "src/service/SingletonServiceProvider.h"

FamiliarizationViewPresenter::FamiliarizationViewPresenter(FamiliarizationView &familiarization_view,
                                                           FamiliarizationSettings &familiarization_settings,
                                                           ParticipantInfo participantInfo)
        : familiarization_view(familiarization_view),
          familiarization_settings(familiarization_settings),
          participantInfo(std::move(participantInfo)),
          familiarizationBlockLogger{this->participantInfo, familiarization_settings} {
    uniformDistributionValueGenerator = new UniformDistributionValueGenerator(
            familiarization_settings.reference_line_minimum_length_cm,
            familiarization_settings.reference_line_maximum_length_cm);
    reference_speed_dataset_cm_s = new PrimitiveSmoothDataset(familiarization_settings.smoothing_intencity);
    participant_speed_dataset_cm_s = new PrimitiveSmoothDataset(familiarization_settings.smoothing_intencity);
    capture_start_string = new StringWithAnimatedTailDots("Capture is starting, please wait", 4.0, true);
    information_saving_string = new StringWithAnimatedTailDots("Information is saving, please wait", 4.0, true);
    camera_service = (CameraService *) SingletonServiceProvider::GetService(CAMERA_SERVICE);
    robot_service = (RobotService *) SingletonServiceProvider::GetService(ROBOT_SERVICE);
    SetActualQtmFilenamePrefix();
}

FamiliarizationViewPresenter::~FamiliarizationViewPresenter() {
    delete uniformDistributionValueGenerator;
    delete reference_speed_dataset_cm_s;
    delete participant_speed_dataset_cm_s;
    delete capture_start_string;
    delete information_saving_string;
}

void FamiliarizationViewPresenter::OnPaintEvent(wxPaintEvent &evt) {
    switch (current_familiarization_scene) {
        case FamiliarizationScene::FIRST_FAMILIARIZATION_IMAGE:
            familiarization_view.DrawPNGImage(first_familiarization_image_path);
            break;
        case FamiliarizationScene::CAPTURE_STARTING:
            if (!capture_start_string->IsAnimationStarted()) {
                capture_start_string->StartAnimation();
            }
            familiarization_view.DrawCenteredText(
                    {capture_start_string->GetString()},
                    system_familiarization_text_style);
            if (camera_service->GetCameraStatus() == CONNECTED_AND_CAPTURING) {
                if (robot_service->GetRobotStatus() == RobotStatus::CONNECTED_AND_AT_NEAR_HOME_AREA) {
                    NextScene();
                }
            }
            break;
        case FamiliarizationScene::INSTRUCTION:
            familiarization_view.DrawCenteredText(
                    {"Familiarization with experiment. Click to start.", " ",
                     "Sezn\u00E1men\u00ED s experimentem. Klikn\u011Bte pro zah\u00E1jen\u00ED."},
                    default_familiarization_text_style);
            break;
        case FamiliarizationScene::SCENE_3_2_1: {
            if (int_value_incrementor == nullptr) {
                int_value_incrementor = new IntValueIncrementor(3, -1, 1);
                int_value_incrementor->Start();
            }
            auto countdown_value = int_value_incrementor->GetValue();
            if (countdown_value == 1) {
                familiarization_view.DrawCenteredText(
                        {"Focus on central cross.", " ", "1  +  1", " ",
                         "Soust\u0159e\u010Fte se na k\u0159\u00ED\u017Eek."}, default_familiarization_text_style);
                if (!start_hand_following_command_sent) {
                    robot_service->StartHandFollowing();
                    start_hand_following_command_sent = true;
                }
            } else if (countdown_value <= 0) {
                familiarization_view.DrawCenteredText(
                        {"Focus on central cross.", " ", "1  +  1", " ",
                         "Soust\u0159e\u010Fte se na k\u0159\u00ED\u017Eek."}, default_familiarization_text_style);
                int_value_incrementor->Stop();
                delete int_value_incrementor;
                int_value_incrementor = nullptr;
                NextScene();
            } else {
                familiarization_view.DrawCenteredText(
                        {"Focus on central cross.", " ",
                         std::to_string(countdown_value) + "  +  " + std::to_string(countdown_value), " ",
                         "Soust\u0159e\u010Fte se na k\u0159\u00ED\u017Eek."}, default_familiarization_text_style);
            }
            break;
        }
        case FamiliarizationScene::FAMILIARIZATION_ACTIVE_TRIAL: {
            PositionAndVelocity participant_hand_info = camera_service->GetHandEndPositionAndVelocity();
            if (participant_movement_computer == nullptr) {
                participant_movement_computer = new ParticipantMovementComputer(
                        participant_hand_info.position_m.x() * 100.0);
            }
            if (reference_line_length_incrementor == nullptr) {
                reference_line_length_incrementor = new DoubleValueIncrementor(0.0,
                                                                               familiarization_settings.reference_line_speed_cm_s,
                                                                               1.0, true, true,
                                                                               familiarization_settings.reference_line_speedup_time_s);
                reference_line_length_incrementor->Start(uniformDistributionValueGenerator->GetValue());
            }
            auto temp_last_participant_line_speed_update = std::chrono::high_resolution_clock::now();
            auto distance_temp = abs(participant_movement_computer->GetMovementLengthCM(
                    participant_hand_info.position_m.x() * 100.0) - participant_movement_length_cm);
            std::chrono::duration<double> time_temp = temp_last_participant_line_speed_update - last_participant_line_speed_update;
            double participant_velocity_cm_s = abs(distance_temp/time_temp.count());
            participant_speed_dataset_cm_s->AddValue(participant_velocity_cm_s);
            participant_movement_length_cm = participant_movement_computer->GetMovementLengthCM(
                    participant_hand_info.position_m.x() * 100.0);
            last_participant_line_speed_update = temp_last_participant_line_speed_update;
            std::pair<double, double> reference_line_value_and_speed = reference_line_length_incrementor->GetValueAndSpeed(
                    participant_hand_info.measurement_timestamp);
            reference_speed_dataset_cm_s->AddValue(reference_line_value_and_speed.second);
            familiarization_view.DrawFamiliarizationActiveScene(Rectangle(familiarization_settings.line_width_cm,
                                                                          participant_movement_length_cm,
                                                                          MeasurementUnit::CM,
                                                                          familiarization_lines_color,
                                                                          false),
                                                                Rectangle(familiarization_settings.line_width_cm,
                                                                          reference_line_value_and_speed.first,
                                                                          MeasurementUnit::CM,
                                                                          familiarization_lines_color,
                                                                          true),
                                                                Rectangle(0,
                                                                          familiarization_settings.borders_size_cm,
                                                                          MeasurementUnit::CM,
                                                                          familiarization_border_color, true),
                                                                Rectangle(0,
                                                                          familiarization_settings.borders_size_cm,
                                                                          MeasurementUnit::CM,
                                                                          familiarization_border_color, true),
                                                                reference_line_length_incrementor->GetMaximumValue(),
                                                                central_cross_style
            );
            LogData(date::format("%F %T",
                                 participant_hand_info.measurement_timestamp), participant_movement_length_cm, reference_line_value_and_speed.first,
                    participant_velocity_cm_s, participant_speed_dataset_cm_s->GetLastElement(),
                    reference_line_value_and_speed.second, reference_speed_dataset_cm_s->GetLastElement());
            break;
        }
        case FamiliarizationScene::FAMILIARIZATION_TRIAL_EVALUATION: {
            if (!evaluation_feedback_cached) {
                EvaluationResults evaluation_results = EvaluateSpeedInTrial(*reference_speed_dataset_cm_s,
                                                                            *participant_speed_dataset_cm_s);
                if (!evaluation_results.participant_didnt_reach_gray_zone &&
                    !evaluation_results.participant_crossed_upperbound &&
                    !evaluation_results.participant_crossed_lowerbound) {
                    current_ideal_trials++;
                }
                cached_evaluation_feedback = GetEvaluationFeedback(evaluation_results);
                evaluation_feedback_cached = true;
            }
            auto trial_information = std::pair<const std::string, const TextStyle>(
                    std::to_string(current_ideal_trials) + "/" +
                    std::to_string(familiarization_settings.ideal_trials_number), default_familiarization_text_style);
            familiarization_view.DrawFamiliarizationEvaluationScene(Rectangle(familiarization_settings.line_width_cm,
                                                                              participant_movement_length_cm,
                                                                              MeasurementUnit::CM,
                                                                              familiarization_lines_color,
                                                                              false), Rectangle(0,
                                                                                                familiarization_settings.borders_size_cm,
                                                                                                MeasurementUnit::CM,
                                                                                                familiarization_border_color,
                                                                                                false),
                                                                    Rectangle(0,
                                                                              familiarization_settings.borders_size_cm,
                                                                              MeasurementUnit::CM,
                                                                              familiarization_border_color, false),
                                                                    reference_line_length_incrementor->GetMaximumValue(),
                                                                    familiarization_settings.ideal_speed_lowerbound_cm_s,
                                                                    familiarization_settings.ideal_speed_upperbound_cm_s,
                                                                    reference_speed_dataset_cm_s->GetDataset(),
                                                                    participant_speed_dataset_cm_s->GetDataset(),
                                                                    familiarization_settings.reference_line_speed_cm_s,
                                                                    cached_evaluation_feedback, trial_information);
            break;
        }
        case FamiliarizationScene::CAMERA_DATA_SAVING: {
            if (!information_saving_string->IsAnimationStarted()) {
                information_saving_string->StartAnimation();
            }
            familiarization_view.DrawCenteredText(
                    {information_saving_string->GetString()},
                    system_familiarization_text_style);
            if (camera_service->GetCameraStatus() == CONNECTED_AND_NOT_CAPTURING) {
                if (robot_service->GetRobotStatus() == RobotStatus::CONNECTED_AND_AT_NEAR_HOME_AREA) {
                    NextScene();
                }
            }
            break;
        }
        case FamiliarizationScene::FAMILIARIZATION_COMPLETED:
            familiarization_view.DrawCenteredText(
                    {"Familiarization completed."},
                    familiarization_completed_text_style);
    }
}

void FamiliarizationViewPresenter::OnMouseClick(wxMouseEvent &evt) {
    if (current_familiarization_scene != FamiliarizationScene::SCENE_3_2_1 &&
        current_familiarization_scene != FamiliarizationScene::CAPTURE_STARTING &&
        current_familiarization_scene != FamiliarizationScene::CAMERA_DATA_SAVING) {
        NextScene();
    }
}

void FamiliarizationViewPresenter::NextScene() {
    switch (current_familiarization_scene) {
        case FamiliarizationScene::FIRST_FAMILIARIZATION_IMAGE:
            camera_service->AddCommand({"", START_CAPTURE});
            current_familiarization_scene = FamiliarizationScene::CAPTURE_STARTING;
            break;
        case FamiliarizationScene::CAPTURE_STARTING:
            capture_start_string->StopAnimation();
            current_familiarization_scene = FamiliarizationScene::INSTRUCTION;
            break;
        case FamiliarizationScene::INSTRUCTION:
            camera_service->AddCommand({"3_2_1_scene_started", SEND_QTM_EVENT});
            robot_service->MoveToHome();
            current_familiarization_scene = FamiliarizationScene::SCENE_3_2_1;
            break;
        case FamiliarizationScene::SCENE_3_2_1:
            reference_speed_dataset_cm_s->Clear();
            participant_speed_dataset_cm_s->Clear();
            camera_service->AddCommand({"Active_trial_scene_started", SEND_QTM_EVENT});
            current_familiarization_scene = FamiliarizationScene::FAMILIARIZATION_ACTIVE_TRIAL;
            break;
        case FamiliarizationScene::FAMILIARIZATION_ACTIVE_TRIAL: {
            robot_service->StopHandFollowing();
            camera_service->AddCommand({"Evaluation_scene_started", SEND_QTM_EVENT});
            delete participant_movement_computer;
            participant_movement_computer = nullptr;
            current_familiarization_scene = FamiliarizationScene::FAMILIARIZATION_TRIAL_EVALUATION;
            break;
        }
        case FamiliarizationScene::FAMILIARIZATION_TRIAL_EVALUATION:
            delete reference_line_length_incrementor;
            reference_line_length_incrementor = nullptr;
            evaluation_feedback_cached = false;
            camera_service->AddCommand({"", STOP_AND_SAVE_CAPTURE});
            current_familiarization_scene = FamiliarizationScene::CAMERA_DATA_SAVING;
            break;
        case FamiliarizationScene::CAMERA_DATA_SAVING:
            information_saving_string->StopAnimation();
            if (current_ideal_trials == familiarization_settings.ideal_trials_number) {
                current_familiarization_scene = FamiliarizationScene::FAMILIARIZATION_COMPLETED;
            } else {
                trial_counter++;
                SetActualQtmFilenamePrefix();
                camera_service->AddCommand({"", START_CAPTURE});
                start_hand_following_command_sent = false;
                familiarizationBlockLogger.OnNextTrialStarted();
                current_familiarization_scene = FamiliarizationScene::CAPTURE_STARTING;
            }
            break;
    }
}

EvaluationResults
FamiliarizationViewPresenter::EvaluateSpeedInTrial(PrimitiveSmoothDataset &reference_speed_dataset_cm_s,
                                                   PrimitiveSmoothDataset &participant_speed_dataset_cm_s) {
    EvaluationResults evaluation_results;
    auto reference_speed_extracted_dataset_cm_s = reference_speed_dataset_cm_s.GetDataset();
    auto participant_speed_extracted_dataset_cm_s = participant_speed_dataset_cm_s.GetDataset();
    size_t datasets_size = reference_speed_extracted_dataset_cm_s.size();
    for (size_t i = 0; i < datasets_size; ++i) {
        if (familiarization_settings.reference_line_speed_cm_s == reference_speed_extracted_dataset_cm_s[i]) {
            if (participant_speed_extracted_dataset_cm_s[i] > familiarization_settings.ideal_speed_upperbound_cm_s) {
                evaluation_results.participant_crossed_upperbound = true;
            } else if (participant_speed_extracted_dataset_cm_s[i] <
                       familiarization_settings.ideal_speed_lowerbound_cm_s) {
                evaluation_results.participant_crossed_lowerbound = true;
            }
        }
    }
    if (participant_movement_length_cm >
        reference_line_length_incrementor->GetMaximumValue() + familiarization_settings.borders_size_cm ||
        participant_movement_length_cm <
        reference_line_length_incrementor->GetMaximumValue() - familiarization_settings.borders_size_cm) {
        evaluation_results.participant_didnt_reach_gray_zone = true;
    }
    return evaluation_results;
}

std::vector<std::pair<const std::string, const TextStyle>>
FamiliarizationViewPresenter::GetEvaluationFeedback(EvaluationResults &evaluation_results) {
    std::vector<std::pair<const std::string, const TextStyle>> evaluation_feedback;
    if (evaluation_results.participant_didnt_reach_gray_zone) {
        evaluation_feedback.emplace_back("You didn't reach the gray zone.",
                                         TextStyle({255, 0, 0, 255}, default_familiarization_text_style.GetHeightPx()));
    }
    if (evaluation_results.participant_crossed_upperbound) {
        evaluation_feedback.emplace_back("You have crossed speed upperbound (" +
                                         fmt::format("{:.2f}", familiarization_settings.ideal_speed_upperbound_cm_s) +
                                         "cm/s)!",
                                         TextStyle({227, 93, 29, 255},
                                                   default_familiarization_text_style.GetHeightPx()));
    }
    if (evaluation_results.participant_crossed_lowerbound) {
        evaluation_feedback.emplace_back("You have crossed speed lowerbound (" +
                                         fmt::format("{:.2f}", familiarization_settings.ideal_speed_lowerbound_cm_s) +
                                         "cm/s)!",
                                         TextStyle({100, 2, 101, 255},
                                                   default_familiarization_text_style.GetHeightPx()));
    }
    if (!evaluation_results.participant_didnt_reach_gray_zone && !evaluation_results.participant_crossed_upperbound &&
        !evaluation_results.participant_crossed_lowerbound) {
        evaluation_feedback.emplace_back("Good try!",
                                         TextStyle({0, 255, 0, 255}, default_familiarization_text_style.GetHeightPx()));
    }
    return evaluation_feedback;
}

void FamiliarizationViewPresenter::SetActualQtmFilenamePrefix() {
    std::string qtm_filename_prefix =
            participantInfo.participantCodename + "_" +
            "0_FAM_trial_" +
            std::to_string(trial_counter);
    camera_service->SetQtmFilenamePrefix(qtm_filename_prefix);
}

void FamiliarizationViewPresenter::LogData(std::string measurement_timestamp, double participant_line_height_cm,
                                           double reference_line_height_cm,
                                           double participant_line_speed_cm_s,
                                           double smoothed_participant_line_speed_cm_s,
                                           double reference_line_speed_cm_s,
                                           double smoothed_reference_line_speed_cm_s) {
    familiarizationBlockLogger.LogData(measurement_timestamp, camera_service->GetCurrentFrameNumber(), trial_counter, current_ideal_trials,
                                       participant_line_height_cm, reference_line_height_cm,
                                       participant_line_speed_cm_s, smoothed_participant_line_speed_cm_s,
                                       reference_line_speed_cm_s, smoothed_reference_line_speed_cm_s,
                                       reference_line_length_incrementor->GetMaximumValue());
}
