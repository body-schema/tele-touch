#ifndef QUALISYSKINOVAINTERFACE_SETUPVIEWPRESENTER_H
#define QUALISYSKINOVAINTERFACE_SETUPVIEWPRESENTER_H


#include "src/ui/view/SetupView.h"
#include "src/service/ParticipantService/ParticipantService.h"

class SetupViewPresenter {

public:
    explicit SetupViewPresenter(SetupView *setup_view);

    ~SetupViewPresenter();

    void OnCreateParticipantRequest();

    void OnCreateAndOpenParticipantRequest();

    void OnOpenParticipantRequest();

    void OnOpenParticipant(std::string &file_path);

    void OnEditParticipantRequest();

    void OnExitRequest();

    void OnStartExperimentRequest();

    void OnChangeBlockType(std::string new_block_type);

    void OnSaveParticipant(ParticipantInfo &participant_info, std::string &file_path);

    void OnSaveAndOpenParticipant(ParticipantInfo &participant_info, std::string &file_path);
private:
    SetupView *setup_view;
    ExperimentBlockType experiment_block_type;

    ParticipantInfo *current_participant_info = nullptr;
    ParticipantService *participantService;
};


#endif //QUALISYSKINOVAINTERFACE_SETUPVIEWPRESENTER_H
