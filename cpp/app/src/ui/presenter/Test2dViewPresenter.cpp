#include "Test2dViewPresenter.h"

Test2dViewPresenter::Test2dViewPresenter(Test2dView &test2D_view): test2D_view(test2D_view) {
    camera_service = (CameraService *) SingletonServiceProvider::GetService(CAMERA_SERVICE);
    capture_start_string = new StringWithAnimatedTailDots("Capture is starting, please wait", 4.0, true);
    camera_service->AddCommand({"", START_CAPTURE});
    robot_service = (RobotService *) SingletonServiceProvider::GetService(ROBOT_SERVICE);
}

Test2dViewPresenter::~Test2dViewPresenter() {
    delete capture_start_string;
    camera_service->AddCommand({"", STOP_AND_SAVE_CAPTURE});
}

void Test2dViewPresenter::OnPaintEvent(wxPaintEvent &evt) {
    switch (current_scene) {
        case Test2dScene::CAPTURE_STARTING: {
            if (!capture_start_string->IsAnimationStarted()) {
                capture_start_string->StartAnimation();
            }
            test2D_view.DrawCenteredText(
                    {capture_start_string->GetString()},
                    system_text_style);
            if (camera_service->GetCameraStatus() == CONNECTED_AND_CAPTURING) {
                if (robot_service->GetRobotStatus() == RobotStatus::CONNECTED_AND_AT_NEAR_HOME_AREA) {
                    NextScene();
                }
            }
            break;
        }
        case Test2dScene::CLICK_TO_START_TRACKING:
            test2D_view.DrawCenteredText(
                    {"Click to start tracking."},
                    default_text_style);
            break;
        case Test2dScene::TRACKING:
            test2D_view.DrawCenteredText(
                    {"Movement is tracking"},
                    default_text_style);
            if (robot_service->GetRobotStatus() != RobotStatus::CONNECTED_AND_FOLLOWING_2D_HAND) {
                On2dTrackingError();
            }
            break;
        case Test2dScene::ERROR:
            test2D_view.DrawCenteredText(
                    {"Error happened.", "The robot tried to move outside of the bondary box or", "with higher than maximum speed"},
                    error_text_style);
            break;
    }
}

void Test2dViewPresenter::OnMouseClick(wxMouseEvent &evt) {
    if (current_scene != Test2dScene::CAPTURE_STARTING) {
        NextScene();
    }
}

void Test2dViewPresenter::NextScene() {
    switch (current_scene) {
        case Test2dScene::CAPTURE_STARTING:
            capture_start_string->StopAnimation();
            robot_service->Stop2DHandFollowing();
            current_scene = Test2dScene::CLICK_TO_START_TRACKING;
            break;
        case Test2dScene::CLICK_TO_START_TRACKING:
            if (robot_service->GetRobotStatus() == RobotStatus::CONNECTED_AND_AT_HOME) {
                robot_service->Start2DHandFollowing();
                current_scene = Test2dScene::TRACKING;
            }
            break;
        case Test2dScene::TRACKING:
            robot_service->Stop2DHandFollowing();
            current_scene = Test2dScene::CLICK_TO_START_TRACKING;
            break;
        case Test2dScene::ERROR:
            current_scene = Test2dScene::CLICK_TO_START_TRACKING;
            break;
    }
}

void Test2dViewPresenter::On2dTrackingError() {
    robot_service->Stop2DHandFollowing();
    current_scene = Test2dScene::ERROR;
}
