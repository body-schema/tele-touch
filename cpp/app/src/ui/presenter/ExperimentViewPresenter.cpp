#include "ExperimentViewPresenter.h"
#include "src/service/CameraService/CameraService.h"
#include "src/service/SingletonServiceProvider.h"
#include "src/utility/ScreenSettings.h"

ExperimentViewPresenter::ExperimentViewPresenter(ExperimentView &experiment_view,
                                                 ExperimentSettings &experiment_settings,
                                                 ParticipantInfo participant_info,
                                                 ExperimentBlockType experimentBlockType) : experiment_view(
        experiment_view),
                                                                                            experiment_settings(
                                                                                                    experiment_settings),
                                                                                            participant_info(std::move(
                                                                                                    participant_info)),
                                                                                            experimentBlockType{
                                                                                                    experimentBlockType} {
    uniformDistributionValueGenerator = new UniformDistributionValueGenerator(
            experiment_settings.initial_judgement_line_min_height,
            experiment_settings.initial_judgement_line_max_height);
    capture_start_string = new StringWithAnimatedTailDots("Capture is starting, please wait", 4.0, true);
    information_saving_string = new StringWithAnimatedTailDots("Information is saving, please wait", 4.0, true);
    camera_service = (CameraService *) SingletonServiceProvider::GetService(CAMERA_SERVICE);
    robot_service = (RobotService *) SingletonServiceProvider::GetService(ROBOT_SERVICE);
    experiment_logger = new ExperimentLoggerService(this->participant_info, experiment_settings, experimentBlockType, 1);
    camera_service->AddCommand({"", START_CAPTURE});
    auto new_ratio = ratio_values[random_ratio_generator(gen)];
    robot_service->UpdateGainRatio(new_ratio);
    current_trial_info.gain_value = new_ratio;
    SetActualQtmFilenamePrefix();
    experiment_logger->LogBlockData(camera_service->GetCamerasSettings(),
                                    camera_service->GetParticipantArmLengthCm(),
                                    camera_service->GetCurrentFrameNumber(),
                                    current_trial_number);
}

ExperimentViewPresenter::~ExperimentViewPresenter() {
    if (current_experiment_scene != ExperimentScene::EXPERIMENT_BLOCK_COMPLETED) {
        experiment_logger->LogBlockData(camera_service->GetCamerasSettings(),
                                        camera_service->GetParticipantArmLengthCm(),
                                        camera_service->GetCurrentFrameNumber(),
                                        current_trial_number);
    }
    delete experiment_logger;
    delete capture_start_string;
    delete information_saving_string;
    delete uniformDistributionValueGenerator;
}

void ExperimentViewPresenter::OnPaintEvent(wxPaintEvent &evt) {
    switch (current_experiment_scene) {
        case ExperimentScene::CAPTURE_STARTING:
            if (!capture_start_string->IsAnimationStarted()) {
                capture_start_string->StartAnimation();
            }
            experiment_view.DrawCenteredText(
                    {capture_start_string->GetString()},
                    system_experiment_text_style);
            if (camera_service->GetCameraStatus() == CONNECTED_AND_CAPTURING) {
                if (robot_service->GetRobotStatus() == RobotStatus::CONNECTED_AND_AT_NEAR_HOME_AREA) {
                    NextScene();
                }
            }
            break;
        case ExperimentScene::INSTRUCTION:
            if (experimentBlockType == AM || experimentBlockType == PM) {
                experiment_view.DrawCenteredText(
                        {"Concentrate on MOVEMENT.", " ",
                         "Soust\u0159e\u010Fte se na POHYB."},
                        default_experiment_text_style);
            } else if (experimentBlockType == AT || experimentBlockType == PT) {
                experiment_view.DrawCenteredText(
                        {"Concentrate on TOUCH.", " ",
                         "Soust\u0159e\u010Fte se na DOTYK."},
                        default_experiment_text_style);
            }
            DrawCurrentTrialInfo();
            break;
        case ExperimentScene::SCENE_3_2_1: {
            if (int_value_incrementor == nullptr) {
                int_value_incrementor = new IntValueIncrementor(3, -1, 1);
                int_value_incrementor->Start();
            }
            auto countdown_value = int_value_incrementor->GetValue();
            if (countdown_value == 1) {
                experiment_view.DrawCenteredText(
                        {"Focus on central cross.", " ",
                         std::to_string(countdown_value) + "  +  " + std::to_string(countdown_value), " ",
                         "Soust\u0159e\u010Fte se na k\u0159\u00ED\u017Eek."}, default_experiment_text_style);
                if (!start_hand_following_command_sent) {
                    robot_service->StartHandFollowing();
                    current_trial_info.robot_participant_sync_datetime = std::chrono::high_resolution_clock::now();
                    current_trial_info.robot_participant_sync_frame = camera_service->GetCurrentFrameNumber();
                    start_hand_following_command_sent = true;
                }
            } else if (countdown_value <= 0) {
                experiment_view.DrawCenteredText(
                        {"Focus on central cross.", " ", "1  +  1", " ",
                         "Soust\u0159e\u010Fte se na k\u0159\u00ED\u017Eek."}, default_experiment_text_style);
                int_value_incrementor->Stop();
                delete int_value_incrementor;
                int_value_incrementor = nullptr;
                NextScene();
            } else {
                experiment_view.DrawCenteredText(
                        {"Focus on central cross.", " ",
                         std::to_string(countdown_value) + "  +  " + std::to_string(countdown_value), " ",
                         "Soust\u0159e\u010Fte se na k\u0159\u00ED\u017Eek."}, default_experiment_text_style);
            }
            break;
        }
        case ExperimentScene::EXPERIMENT_ACTIVE_TRIAL: {
            experiment_view.DrawCenteredText({"+"}, central_cross_style);
            break;
        }
        case ExperimentScene::EXPERIMENT_TRIAL_EVALUATION: {
            if (experimentBlockType == AT || experimentBlockType == PT) {
                experiment_view.DrawExperimentEvaluationScene(Rectangle(experiment_settings.lines_width_cm,
                                                                        participant_judgement_length_cm,
                                                                        MeasurementUnit::CM,
                                                                        experiment_lines_color,
                                                                        false), false);
            } else {
                experiment_view.DrawExperimentEvaluationScene(Rectangle(experiment_settings.lines_width_cm,
                                                                        participant_judgement_length_cm,
                                                                        MeasurementUnit::CM,
                                                                        experiment_lines_color,
                                                                        false), true);
            }
            DrawCurrentTrialInfo();
            break;
        }
        case ExperimentScene::CAMERA_DATA_SAVING:
            if (!information_saving_string->IsAnimationStarted()) {
                information_saving_string->StartAnimation();
            }
            experiment_view.DrawCenteredText(
                    {information_saving_string->GetString()},
                    system_experiment_text_style);
            if (camera_service->GetCameraStatus() == CONNECTED_AND_NOT_CAPTURING) {
                if (robot_service->GetRobotStatus() == RobotStatus::CONNECTED_AND_AT_NEAR_HOME_AREA) {
                    NextScene();
                }
            }
            break;
        case ExperimentScene::EXPERIMENT_BLOCK_COMPLETED:
            experiment_view.DrawCenteredText(
                    {"Experiment block completed."},
                    experiment_completed_text_style);
    }
}

void ExperimentViewPresenter::OnMouseClick(wxMouseEvent &evt) {
    if (current_experiment_scene != ExperimentScene::SCENE_3_2_1 &&
        current_experiment_scene != ExperimentScene::CAPTURE_STARTING &&
        current_experiment_scene != ExperimentScene::CAMERA_DATA_SAVING) {
        NextScene();
    }
}

void ExperimentViewPresenter::NextScene() {
    switch (current_experiment_scene) {
        case ExperimentScene::CAPTURE_STARTING:
            capture_start_string->StopAnimation();
            current_trial_info.trial_start_datetime = std::chrono::high_resolution_clock::now();
            current_experiment_scene = ExperimentScene::INSTRUCTION;
            break;
        case ExperimentScene::INSTRUCTION:
            robot_service->MoveToHome();
            camera_service->AddCommand({"3_2_1_scene_started", SEND_QTM_EVENT});
            current_experiment_scene = ExperimentScene::SCENE_3_2_1;
            break;
        case ExperimentScene::SCENE_3_2_1:
            camera_service->AddCommand({"Active_trial_scene_started", SEND_QTM_EVENT});
            current_experiment_scene = ExperimentScene::EXPERIMENT_ACTIVE_TRIAL;
            break;
        case ExperimentScene::EXPERIMENT_ACTIVE_TRIAL: {
            robot_service->StopHandFollowing();
            participant_judgement_length_cm = uniformDistributionValueGenerator->GetValue();
            camera_service->AddCommand({"Evaluation_scene_started", SEND_QTM_EVENT});
            current_trial_info.judgement_start_datetime = std::chrono::high_resolution_clock::now();
            current_trial_info.judgement_start_frame = camera_service->GetCurrentFrameNumber();
            current_experiment_scene = ExperimentScene::EXPERIMENT_TRIAL_EVALUATION;
            break;
        }
        case ExperimentScene::EXPERIMENT_TRIAL_EVALUATION:
            camera_service->AddCommand({"", STOP_AND_SAVE_CAPTURE});
            current_experiment_scene = ExperimentScene::CAMERA_DATA_SAVING;
            break;
        case ExperimentScene::CAMERA_DATA_SAVING:
            information_saving_string->StopAnimation();
            auto hand_tracking_robot_info = robot_service->GetHandTrackingRobotInfo();
            current_trial_info.trial_robot_initial_coordinates = hand_tracking_robot_info.movement_tracking_robot_initial_coordinates;
            current_trial_info.trial_robot_final_coordinates = hand_tracking_robot_info.movement_tracking_robot_final_coordinates;
            current_trial_info.trial_MW_begin_coordinates = hand_tracking_robot_info.movement_tracking_MW_begin_coordinates;
            current_trial_info.trial_MW_end_coordinates = hand_tracking_robot_info.movement_tracking_MW_end_coordinates;
            experiment_logger->OnTrialComplete(current_trial_info, participant_judgement_length_cm);
            if (current_trial_number == experiment_settings.max_trial_number) {
                experiment_logger->LogBlockData(camera_service->GetCamerasSettings(),
                                                camera_service->GetParticipantArmLengthCm(),
                                                camera_service->GetCurrentFrameNumber(),
                                                current_trial_number);
                current_experiment_scene = ExperimentScene::EXPERIMENT_BLOCK_COMPLETED;
            } else {
                current_trial_number++;
                start_hand_following_command_sent = false;
                SetActualQtmFilenamePrefix();
                auto new_ratio = ratio_values[random_ratio_generator(gen)];
                robot_service->UpdateGainRatio(new_ratio);
                current_trial_info.gain_value = new_ratio;
                camera_service->AddCommand({"", START_CAPTURE});
                current_experiment_scene = ExperimentScene::CAPTURE_STARTING;
            }
            break;
    }
}

void ExperimentViewPresenter::OnKeyDown(wxKeyEvent &evt) {
    if (current_experiment_scene == ExperimentScene::EXPERIMENT_TRIAL_EVALUATION) {
        auto pressed_key_code = evt.GetKeyCode();
        if (pressed_key_code == 329) {
            participant_judgement_length_cm -= experiment_settings.judgement_line_increasing_step;
            if (participant_judgement_length_cm < 0) {
                participant_judgement_length_cm = 0;
            }
        } else if (pressed_key_code == 330) {
            participant_judgement_length_cm += experiment_settings.judgement_line_increasing_step;
            if (participant_judgement_length_cm > app_screen_settings->GetScreenHeightCm() - 2) {
                participant_judgement_length_cm = app_screen_settings->GetScreenHeightCm() - 2;
            }
        }
    }
}

void ExperimentViewPresenter::DrawCurrentTrialInfo() {
    experiment_view.DrawDownRightCornerText(
            std::to_string(current_trial_number) + "\\" + std::to_string(experiment_settings.max_trial_number),
            default_experiment_text_style);
}

void ExperimentViewPresenter::SetActualQtmFilenamePrefix() {
    std::string qtm_filename_prefix =
            participant_info.participantCodename + "_" + std::to_string(getBlockOrderNumber(experimentBlockType, participant_info.orderOfTheBlocks))
            + "_" +getBlockTypeStr(experimentBlockType)+ "_trial_" +
            std::to_string(current_trial_number);
    camera_service->SetQtmFilenamePrefix(qtm_filename_prefix);
}

