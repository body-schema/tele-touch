#include "SetupViewPresenter.h"
#include "src/service/SingletonServiceProvider.h"
#include "src/datastruct/ExperimentBlockType.h"

SetupViewPresenter::SetupViewPresenter(SetupView *setup_view) : setup_view(setup_view) {
    participantService = (ParticipantService *) SingletonServiceProvider::GetService(PARTICIPANT_SERVICE);
}

void SetupViewPresenter::OnCreateParticipantRequest() {
    setup_view->OpenCreateParticipantDialog(false);
}

void SetupViewPresenter::OnSaveAndOpenParticipant(ParticipantInfo &participant_info, std::string &file_path) {
    participantService->SaveParticipantInfo(participant_info, file_path);
    if (current_participant_info == nullptr) {
        current_participant_info = participantService->GetParticipantInfo(file_path);
    } else {
        delete current_participant_info;
        current_participant_info = participantService->GetParticipantInfo(file_path);
    }
    setup_view->DisplayUserInfo(*current_participant_info);
}

void SetupViewPresenter::OnSaveParticipant(ParticipantInfo &participant_info, std::string &file_path) {
    participantService->SaveParticipantInfo(participant_info, file_path);
}

void SetupViewPresenter::OnCreateAndOpenParticipantRequest() {
    setup_view->OpenCreateParticipantDialog(true);
}

void SetupViewPresenter::OnOpenParticipantRequest() {
    setup_view->OpenParticipantOpenDialog();
}

void SetupViewPresenter::OnOpenParticipant(std::string &file_path) {
    if (current_participant_info == nullptr) {
        current_participant_info = participantService->GetParticipantInfo(file_path);
    } else {
        delete current_participant_info;
        current_participant_info = participantService->GetParticipantInfo(file_path);
    }
    setup_view->DisplayUserInfo(*current_participant_info);
}

void SetupViewPresenter::OnEditParticipantRequest() {
    if (current_participant_info != nullptr) {
        setup_view->OpenEditParticipantDialog(*current_participant_info);
    }
}

void SetupViewPresenter::OnExitRequest() {
    //todo: implement and connect with the view
}

void SetupViewPresenter::OnStartExperimentRequest() {
    if (experiment_block_type == FAMILIARIZATION) {
        FamiliarizationSettings *dynamicFamiliarizationSettings = setup_view->GetFamiliarizationSettings();
        if (current_participant_info != nullptr) {
            setup_view->OpenFamiliarizationView(*current_participant_info, dynamicFamiliarizationSettings);
        } else {
            setup_view->OpenFamiliarizationView(ParticipantInfo(), dynamicFamiliarizationSettings);
        }
    } else if (experiment_block_type == TESTING_2D) {
        setup_view->Open2dTestView();
    } else {
        ExperimentSettings *dynamicExperimentSettings = setup_view->GetExperimentSettings();
        if (current_participant_info != nullptr) {
            setup_view->OpenExperimentView(*current_participant_info, dynamicExperimentSettings, experiment_block_type);
        } else {
            setup_view->OpenExperimentView(ParticipantInfo(), dynamicExperimentSettings, experiment_block_type);
        }
    }

}

SetupViewPresenter::~SetupViewPresenter() {

}

void SetupViewPresenter::OnChangeBlockType(std::string new_block_type) {
    experiment_block_type = toBlockTypeEnum(new_block_type);
    switch (experiment_block_type) {
        case FAMILIARIZATION:
            setup_view->DisplayFamiliarizationSettings();
            break;
        case AM:
            setup_view->DisplayActiveMovementSettings();
            break;
        case AT:
            setup_view->DisplayActiveTouchSettings();
            break;
        case PM:
            setup_view->DisplayPassiveMovementSettings();
            break;
        case PT:
            setup_view->DisplayPassiveTouchSettings();
            break;
        case TESTING_2D:
            setup_view->Display2DTestingSettings();
            break;
    }
}
