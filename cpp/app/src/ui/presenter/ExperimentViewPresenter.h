#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTVIEWPRESENTER_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTVIEWPRESENTER_H


#include <wx/event.h>
#include "src/datastruct/ParticipantInfo.h"
#include "src/ui/view/ExperimentView.h"
#include "src/datastruct/ExperimentSettings.h"
#include "src/utility/StringWithAnimatedTailDots.h"
#include "src/service/CameraService/CameraService.h"
#include "src/datastruct/ExperimentBlockType.h"
#include "src/service/RobotService/RobotService.h"
#include "src/utility/UniformDistributionValueGenerator.h"
#include "src/datastruct/TrialInfo.h"
#include "src/service/logger/ExperimentLoggerService.h"

enum class ExperimentScene {
    INSTRUCTION,
    SCENE_3_2_1,
    EXPERIMENT_ACTIVE_TRIAL,
    EXPERIMENT_TRIAL_EVALUATION,
    CAMERA_DATA_SAVING,
    CAPTURE_STARTING,
    EXPERIMENT_BLOCK_COMPLETED
};

class ExperimentViewPresenter {

public:
    explicit ExperimentViewPresenter(ExperimentView &experiment_view,
                                     ExperimentSettings &experiment_settings,
                                     ParticipantInfo participant_info,
                                     ExperimentBlockType experimentBlockType);

    ~ExperimentViewPresenter();

    void OnPaintEvent(wxPaintEvent &evt);

    void OnMouseClick(wxMouseEvent &evt);

    void OnKeyDown(wxKeyEvent &evt);

private:
    void DrawCurrentTrialInfo();

    ExperimentBlockType experimentBlockType;
    ExperimentView &experiment_view;
    ExperimentSettings &experiment_settings;
    ParticipantInfo participant_info;
    StringWithAnimatedTailDots *capture_start_string;
    StringWithAnimatedTailDots *information_saving_string;
    CameraService *camera_service;
    RobotService *robot_service;
    ExperimentScene current_experiment_scene = ExperimentScene::CAPTURE_STARTING; // first scene
    int current_trial_number = 1;
    static inline const TextStyle default_experiment_text_style = TextStyle({150, 150, 150, 255}, 48);
    static inline const TextStyle system_experiment_text_style = TextStyle({150, 150, 200, 255}, 48);
    static inline const TextStyle experiment_completed_text_style = TextStyle({41, 221, 127, 255}, 48);
    static inline const TextStyle central_cross_style = TextStyle({255, 0, 0, 255}, 64);
    static inline const RGBAColor experiment_lines_color = {150, 150, 150, 255};
    IntValueIncrementor *int_value_incrementor = nullptr;
    double participant_judgement_length_cm = 0.0;
    UniformDistributionValueGenerator *uniformDistributionValueGenerator;
    ExperimentLoggerService *experiment_logger;
    TrialInfo current_trial_info;
    bool start_hand_following_command_sent = false;
    double ratio_values[3] = {1., 1./1.5, 1.5};
    std::uniform_int_distribution<int> random_ratio_generator = std::uniform_int_distribution<int>(0, 2);
    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen = std::mt19937(rd());

    void NextScene();

    void SetActualQtmFilenamePrefix();

};


#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTVIEWPRESENTER_H
