#ifndef QUALISYSKINOVAINTERFACE_FAMILIARIZATIONVIEWPRESENTER_H
#define QUALISYSKINOVAINTERFACE_FAMILIARIZATIONVIEWPRESENTER_H


#include <wx/event.h>
#include "src/ui/view/FamiliarizationView.h"
#include "src/datastruct/FamiliarizationSettings.h"
#include "src/utility/IntValueIncrementor.h"
#include "src/utility/DoubleValueIncrementor.h"
#include "src/utility/UniformDistributionValueGenerator.h"
#include "src/utility/PrimitiveSmoothDataset.h"
#include "src/utility/StringWithAnimatedTailDots.h"
#include "src/service/CameraService/CameraService.h"
#include "src/datastruct/wxParticipantInfo.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/utility/ParticipantMovementComputer.h"
#include "src/service/RobotService/RobotService.h"
#include "src/service/logger/FamiliarizationBlockLoggerService.h"

enum class FamiliarizationScene {
    FIRST_FAMILIARIZATION_IMAGE,
    INSTRUCTION,
    SCENE_3_2_1,
    FAMILIARIZATION_ACTIVE_TRIAL,
    FAMILIARIZATION_TRIAL_EVALUATION,
    CAMERA_DATA_SAVING,
    CAPTURE_STARTING,
    FAMILIARIZATION_COMPLETED
};

struct EvaluationResults {
    bool participant_crossed_lowerbound = false;
    bool participant_crossed_upperbound = false;
    bool participant_didnt_reach_gray_zone = false;
};

class FamiliarizationViewPresenter {


public:
    explicit FamiliarizationViewPresenter(FamiliarizationView &familiarization_view,
                                          FamiliarizationSettings &familiarization_settings,
                                          ParticipantInfo participantInfo);

    ~FamiliarizationViewPresenter();

    void OnPaintEvent(wxPaintEvent &evt);

    void OnMouseClick(wxMouseEvent &evt);

private:
    const std::string first_familiarization_image_path = "./assets/test1.png";
    const std::string second_familiarization_image_path = "./assets/test2.png";

    FamiliarizationView &familiarization_view;
    FamiliarizationScene current_familiarization_scene = FamiliarizationScene::FIRST_FAMILIARIZATION_IMAGE; // first scene
    FamiliarizationSettings &familiarization_settings;
    ParticipantInfo participantInfo;
    IntValueIncrementor *int_value_incrementor = nullptr;
    DoubleValueIncrementor *reference_line_length_incrementor = nullptr;
    static inline const TextStyle default_familiarization_text_style = TextStyle({150, 150, 150, 255}, 48);
    static inline const TextStyle system_familiarization_text_style = TextStyle({150, 150, 200, 255}, 48);
    static inline const TextStyle familiarization_completed_text_style = TextStyle({41, 221, 127, 255}, 48);
    static inline const TextStyle central_cross_style = TextStyle({255, 0, 0, 255}, 64);
    static inline const RGBAColor familiarization_border_color = {200, 200, 200, 255};
    static inline const RGBAColor familiarization_lines_color = {150, 150, 150, 255};
    UniformDistributionValueGenerator *uniformDistributionValueGenerator;
    PrimitiveSmoothDataset *reference_speed_dataset_cm_s;
    PrimitiveSmoothDataset *participant_speed_dataset_cm_s;
    StringWithAnimatedTailDots *capture_start_string;
    StringWithAnimatedTailDots *information_saving_string;
    double participant_movement_length_cm = 0.0;
    int current_ideal_trials = 0;
    std::vector<std::pair<const std::string, const TextStyle>> cached_evaluation_feedback;
    bool evaluation_feedback_cached = false;
    CameraService *camera_service;
    int trial_counter = 1;
    FamiliarizationBlockLoggerService familiarizationBlockLogger;
    ParticipantMovementComputer *participant_movement_computer = nullptr;
    RobotService *robot_service;
    bool start_hand_following_command_sent = false;

    std::chrono::high_resolution_clock::time_point last_participant_line_speed_update = std::chrono::high_resolution_clock::now();

    void NextScene();
    void LogData(std::string measurement_timestamp, double participant_line_height_cm,
                 double reference_line_height_cm,
                 double participant_line_speed_cm_s,
                 double smoothed_participant_line_speed_cm_s,
                 double reference_line_speed_cm_s,
                 double smoothed_reference_line_speed_cm_s);
    EvaluationResults EvaluateSpeedInTrial(PrimitiveSmoothDataset &reference_speed_dataset_cm_s,
                                           PrimitiveSmoothDataset &participant_speed_dataset_cm_s);

    std::vector<std::pair<const std::string, const TextStyle>> GetEvaluationFeedback(EvaluationResults &evaluation_results);

    void SetActualQtmFilenamePrefix();

};


#endif //QUALISYSKINOVAINTERFACE_FAMILIARIZATIONVIEWPRESENTER_H
