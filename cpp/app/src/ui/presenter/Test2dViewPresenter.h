#ifndef QUALISYSKINOVAINTERFACE_TEST2DVIEWPRESENTER_H
#define QUALISYSKINOVAINTERFACE_TEST2DVIEWPRESENTER_H


#include <wx/event.h>
#include "src/ui/view/Test2dView.h"
#include "src/utility/StringWithAnimatedTailDots.h"
#include "src/service/CameraService/CameraService.h"
#include "src/service/SingletonServiceProvider.h"

enum class Test2dScene {
    CAPTURE_STARTING,
    CLICK_TO_START_TRACKING,
    TRACKING,
    ERROR
};

class Test2dViewPresenter {

public:
    explicit Test2dViewPresenter(Test2dView &test2D_view);

    void OnPaintEvent(wxPaintEvent &evt);

    void OnMouseClick(wxMouseEvent &evt);

    void On2dTrackingError();

    ~Test2dViewPresenter();

private:
    Test2dView &test2D_view;
    Test2dScene current_scene = Test2dScene::CAPTURE_STARTING;
    static inline const TextStyle default_text_style = TextStyle({150, 150, 150, 255}, 48);
    static inline const TextStyle system_text_style = TextStyle({150, 150, 200, 255}, 48);
    static inline const TextStyle error_text_style = TextStyle({183, 24, 24, 255}, 48);
    StringWithAnimatedTailDots *capture_start_string;
    CameraService *camera_service;
    RobotService *robot_service;

    void NextScene();

};


#endif //QUALISYSKINOVAINTERFACE_TEST2DVIEWPRESENTER_H
