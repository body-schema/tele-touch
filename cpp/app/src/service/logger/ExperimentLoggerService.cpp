#include "ExperimentLoggerService.h"
#include "src/datastruct/BehavioralInformation.h"

ExperimentLoggerService::ExperimentLoggerService(const ParticipantInfo &participant_info,
                                                 const ExperimentSettings &experiment_settings,
                                                 ExperimentBlockType experiment_block_type, int current_trial)
        : participant_info(participant_info), participant_id(participant_info.participantCodename),
          experiment_settings(experiment_settings), experiment_block_type(experiment_block_type),
          current_trial(current_trial) {
    CreateTrialInfoFile();
}

ExperimentLoggerService::~ExperimentLoggerService() {
    if (trial_information_file->isOpen()) {
        trial_information_file->close();
        delete trial_information_file;
    }
}

void
ExperimentLoggerService::LogBlockData(CamerasSettings cameras_settings,
                                      double participant_arm_length_cm,
                                      unsigned int total_frames_number,
                                      int trials_done) {
    auto block_information_file = ExperimentBlockInfoFile(
            participant_id + "_" + getBlockTypeStr(experiment_block_type) + "_block_info_" +
            date::format("%F_%H-%M", std::chrono::system_clock::now()) + ".csv");
    block_information_file.FillInfo(participant_info, experiment_settings, experiment_block_type, cameras_settings,
                                    participant_arm_length_cm, total_frames_number, trials_done);
    block_information_file.saveInfo();
    block_information_file.close();
}

void ExperimentLoggerService::OnTrialComplete(const TrialInfo &trial_info,
                                              double judgement_length_cm) {
    trial_information_file->open(std::fstream::app);
    trial_information_file->appendDSVValue(participant_info.participantCodename);
    trial_information_file->appendDSVValue(participant_info.age);
    trial_information_file->appendDSVValue(participant_info.gender);
    trial_information_file->appendDSVValue(participant_info.lengthOfForearm);
    trial_information_file->appendDSVValue(participant_info.handedness);
    trial_information_file->appendDSVValue(participant_info.sensitivityOfTactileTest);
    trial_information_file->appendDSVValue(participant_info.orderOfTheBlocks);
    trial_information_file->appendDSVValue(participant_info.description);
    trial_information_file->appendDSVValue(getBlockTypeStr(experiment_block_type));
    trial_information_file->appendDSVValue(std::to_string(current_trial));
    trial_information_file->appendDSVValue(std::to_string(experiment_settings.max_trial_number));
    trial_information_file->appendDSVValue(std::to_string(trial_info.gain_value));
    trial_information_file->appendDSVValue(date::format("%F_%H-%M-%S", trial_info.trial_start_datetime));
    trial_information_file->appendDSVValue(date::format("%F_%H-%M-%S", trial_info.robot_participant_sync_datetime));
    trial_information_file->appendDSVValue(std::to_string(trial_info.robot_participant_sync_frame));
    trial_information_file->appendDSVValue(date::format("%F_%H-%M-%S", trial_info.judgement_start_datetime));
    trial_information_file->appendDSVValue(std::to_string(trial_info.judgement_start_frame));
    trial_information_file->appendDSVValue(std::to_string(judgement_length_cm));
    trial_information_file->appendDSVValue(convertVector3dToString(trial_info.trial_robot_initial_coordinates));
    trial_information_file->appendDSVValue(convertVector3dToString(trial_info.trial_robot_final_coordinates));
    trial_information_file->appendDSVValue(convertVector3dToString(trial_info.trial_MW_begin_coordinates));
    trial_information_file->appendDSVValue(convertVector3dToString(trial_info.trial_MW_end_coordinates));
    trial_information_file->appendDSVValue(date::format("%F_%H-%M-%S", std::chrono::system_clock::now()), false, true);
    trial_information_file->close();
    current_trial++;
}

void ExperimentLoggerService::CreateTrialInfoFile() {
    trial_information_file = new DSVFile(
            participant_id + "_" + getBlockTypeStr(experiment_block_type) + "_trials_info_" +
            date::format("%F_%H-%M", std::chrono::system_clock::now()) + ".csv");
    trial_information_file->open(std::fstream::out);
    trial_information_file->appendDSVValue(std::string("participant_codename"));
    trial_information_file->appendDSVValue(std::string("age"));
    trial_information_file->appendDSVValue(std::string("gender"));
    trial_information_file->appendDSVValue(std::string("length_of_forearm"));
    trial_information_file->appendDSVValue(std::string("handedness"));
    trial_information_file->appendDSVValue(std::string("sensitivity_to_tactile_test"));
    trial_information_file->appendDSVValue(std::string("order_of_the_blocks"));
    trial_information_file->appendDSVValue(std::string("description"));
    trial_information_file->appendDSVValue(std::string("block_type"));
    trial_information_file->appendDSVValue(std::string("trial_number"));
    trial_information_file->appendDSVValue(std::string("total_trial_number"));
    trial_information_file->appendDSVValue(std::string("gain"));
    trial_information_file->appendDSVValue(std::string("trial_start_datetime"));
    trial_information_file->appendDSVValue(std::string("robot_participant_sync_datetime"));
    trial_information_file->appendDSVValue(std::string("robot_participant_sync_frame"));
    trial_information_file->appendDSVValue(std::string("judgement_start_datetime"));
    trial_information_file->appendDSVValue(std::string("judgement_start_frame"));
    trial_information_file->appendDSVValue(std::string("judgement_length_cm"));
    trial_information_file->appendDSVValue(std::string("robot_initial_coordinates"));
    trial_information_file->appendDSVValue(std::string("robot_final_coordinates"));
    trial_information_file->appendDSVValue(std::string("motion_windows_begin_coordinates"));
    trial_information_file->appendDSVValue(std::string("motion_windows_end_coordinates"));
    trial_information_file->appendDSVValue(std::string("trial_end_datetime"), false, true);
    trial_information_file->close();
}
