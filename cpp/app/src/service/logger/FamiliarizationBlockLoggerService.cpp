#include <chrono>
#include "FamiliarizationBlockLoggerService.h"
#include "lib/date/date.h"

FamiliarizationBlockLoggerService::FamiliarizationBlockLoggerService(ParticipantInfo &participantInfo,
                                                                     FamiliarizationSettings &familiarizationSettings,
                                                                     int current_trial) : participant_id{
        participantInfo.participantCodename}, current_trial{current_trial} {
    CreateNewTrialInfoFile();
    block_information_file.FillInfo(participantInfo, familiarizationSettings);
    block_information_file.saveInfo();
    block_information_file.close();
}

FamiliarizationBlockLoggerService::~FamiliarizationBlockLoggerService() {
    if (current_trial_information_file != nullptr) {
        if (current_trial_information_file->isOpen()) {
            current_trial_information_file->close();
        }
        delete current_trial_information_file;
    }
}

void FamiliarizationBlockLoggerService::CreateNewTrialInfoFile() {
    if (current_trial_information_file != nullptr) {
        if (current_trial_information_file->isOpen()) {
            current_trial_information_file->close();
        }
        delete current_trial_information_file;
    }
    current_trial_information_file = new DSVFile(participant_id + "_" + "FAMILIARIZATION_" +
                                                 std::to_string(current_trial) + "_trial_info_" +
                                                 date::format("%F_%H-%M", std::chrono::system_clock::now()) + ".csv");
    current_trial_information_file->clear();
    current_trial_information_file->open(std::fstream::out);
    current_trial_information_file->appendDSVValue(std::string("timestamp"));
    current_trial_information_file->appendDSVValue(std::string("frame_number"));
    current_trial_information_file->appendDSVValue(std::string("participant"));
    current_trial_information_file->appendDSVValue(std::string("trial_number"));
    current_trial_information_file->appendDSVValue(std::string("good_trials"));
    current_trial_information_file->appendDSVValue(std::string("participant_line_height_cm"));
    current_trial_information_file->appendDSVValue(std::string("reference_line_height_cm"));
    current_trial_information_file->appendDSVValue(std::string("participant_line_speed_cm_s"));
    current_trial_information_file->appendDSVValue(std::string("smoothed_participant_line_speed_cm_s"));
    current_trial_information_file->appendDSVValue(std::string("reference_line_speed_cm_s"));
    current_trial_information_file->appendDSVValue(std::string("smoothed_reference_line_speed_cm_s"));
    current_trial_information_file->appendDSVValue(std::string("reference_line_final_length_cm"),
                                                   false, true);
}

void FamiliarizationBlockLoggerService::LogData(std::string measurement_timestamp, unsigned int frame_number, int trial_number, int ideal_trials,
                                                double participant_line_height_cm, double reference_line_height_cm,
                                                double participant_line_speed_cm_s,
                                                double smoothed_participant_line_speed_cm_s, double reference_line_speed_cm_s,
                                                double smoothed_reference_line_speed_cm_s,
                                                double reference_line_final_length_cm) {
    current_trial_information_file->appendDSVValue(measurement_timestamp);
    current_trial_information_file->appendDSVValue(std::to_string(frame_number));
    current_trial_information_file->appendDSVValue(participant_id);
    current_trial_information_file->appendDSVValue(std::to_string(trial_number));
    current_trial_information_file->appendDSVValue(std::to_string(ideal_trials));
    current_trial_information_file->appendDSVValue(std::to_string(participant_line_height_cm));
    current_trial_information_file->appendDSVValue(std::to_string(reference_line_height_cm));
    current_trial_information_file->appendDSVValue(std::to_string(participant_line_speed_cm_s));
    current_trial_information_file->appendDSVValue(std::to_string(smoothed_participant_line_speed_cm_s));
    current_trial_information_file->appendDSVValue(std::to_string(reference_line_speed_cm_s));
    current_trial_information_file->appendDSVValue(std::to_string(smoothed_reference_line_speed_cm_s));
    current_trial_information_file->appendDSVValue(std::to_string(reference_line_final_length_cm), false, true);
}

void FamiliarizationBlockLoggerService::OnNextTrialStarted() {
    current_trial++;
    CreateNewTrialInfoFile();
}
