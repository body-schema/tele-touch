#ifndef QUALISYSKINOVAINTERFACE_EXPERIMENTLOGGERSERVICE_H
#define QUALISYSKINOVAINTERFACE_EXPERIMENTLOGGERSERVICE_H


#include "src/datastruct/ParticipantInfo.h"
#include "src/datastruct/ExperimentSettings.h"
#include "src/datastruct/ExperimentBlockType.h"
#include "src/utility/file/DSVFile.h"
#include "src/utility/file/FamiliarizationBlockInfoFile.h"
#include "lib/date/date.h"
#include "src/utility/file/ExperimentBlockInfoFile.h"
#include "src/datastruct/TrialInfo.h"

class ExperimentLoggerService : public Service {

public:
    ExperimentLoggerService(const ParticipantInfo &participant_info, const ExperimentSettings &experiment_settings,
                            ExperimentBlockType experiment_block_type, int current_trial = 1);

    ~ExperimentLoggerService();

    void OnTrialComplete(const TrialInfo &trial_info,
                         double judgement_length_cm);

    void LogBlockData(CamerasSettings cameras_settings,
                      double participant_arm_length_cm,
                      unsigned int total_frames_number,
                      int trials_done);

private:
    void CreateTrialInfoFile();
    const ParticipantInfo &participant_info;
    const ExperimentSettings &experiment_settings;
    const std::string &participant_id;
    int current_trial;
    DSVFile *trial_information_file = nullptr;
    ExperimentBlockType experiment_block_type;

};


#endif //QUALISYSKINOVAINTERFACE_EXPERIMENTLOGGERSERVICE_H
