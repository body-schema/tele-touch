#ifndef QUALISYSKINOVAINTERFACE_FAMILIARIZATIONBLOCKLOGGERSERVICE_H
#define QUALISYSKINOVAINTERFACE_FAMILIARIZATIONBLOCKLOGGERSERVICE_H


#include "lib/date/date.h"
#include "src/datastruct/FamiliarizationSettings.h"
#include "src/datastruct/ParticipantInfo.h"
#include "src/utility/file/FamiliarizationBlockInfoFile.h"
#include "src/service/Service.h"

class FamiliarizationBlockLoggerService : public Service {

public:
    FamiliarizationBlockLoggerService(ParticipantInfo &participantInfo, FamiliarizationSettings &familiarizationSettings, int current_trial = 1);

    ~FamiliarizationBlockLoggerService();

    void OnNextTrialStarted();

    void LogData(std::string measurement_timestamp, unsigned int frame_number, int trial_number, int ideal_trials, double participant_line_height_cm,
                 double reference_line_height_cm,
                 double participant_line_speed_cm_s,
                 double smoothed_participant_line_speed_cm_s,
                 double reference_line_speed_cm_s,
                 double smoothed_reference_line_speed_cm_s,
                 double reference_line_final_length_cm);

private:
    void CreateNewTrialInfoFile();

    const std::string &participant_id;
    int current_trial;
    DSVFile *current_trial_information_file = nullptr;
    FamiliarizationBlockInfoFile block_information_file = FamiliarizationBlockInfoFile(participant_id + "_" + "FAMILIARIZATION_block_info_" + date::format("%F_%H-%M", std::chrono::system_clock::now()) + ".csv");

};


#endif //QUALISYSKINOVAINTERFACE_FAMILIARIZATIONBLOCKLOGGERSERVICE_H
