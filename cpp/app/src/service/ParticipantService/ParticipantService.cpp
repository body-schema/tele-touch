#include "ParticipantService.h"
#include "src/utility/file/ParticipantInfoFile.h"

ParticipantService::ParticipantService() : Service() {

}

void ParticipantService::SaveParticipantInfo(ParticipantInfo &participant_info, std::string &file_path) {
    ParticipantInfoFile participantInfoFile = ParticipantInfoFile(file_path);
    participantInfoFile.getParticipantInfoFromStruct(participant_info);
    participantInfoFile.saveInfo();
    participantInfoFile.close();
}

ParticipantInfo *ParticipantService::GetParticipantInfo(std::string &file_path) {
    ParticipantInfoFile participantInfoFile = ParticipantInfoFile(file_path);
    participantInfoFile.loadInfo();
    auto *participant_info = new ParticipantInfo();
    participantInfoFile.fillParticipantInfoStruct(*participant_info);
    return participant_info;
}
