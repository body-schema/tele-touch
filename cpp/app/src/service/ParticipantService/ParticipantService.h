#ifndef QUALISYSKINOVAINTERFACE_PARTICIPANTSERVICE_H
#define QUALISYSKINOVAINTERFACE_PARTICIPANTSERVICE_H


#include <string>
#include "src/service/Service.h"
#include "src/datastruct/wxParticipantInfo.h"

class ParticipantService : public Service {

public:
    explicit ParticipantService();

    void SaveParticipantInfo(ParticipantInfo &participant_info, std::string &file_path);

    ParticipantInfo *GetParticipantInfo(std::string &file_path);

};


#endif //QUALISYSKINOVAINTERFACE_PARTICIPANTSERVICE_H
