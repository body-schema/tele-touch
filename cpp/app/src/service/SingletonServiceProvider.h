#ifndef ARCH_TEST_PROJECT_SINGLETONSERVICEPROVIDER_H
#define ARCH_TEST_PROJECT_SINGLETONSERVICEPROVIDER_H


#include <map>
#include <stdexcept>
#include "Service.h"
#include "src/service/ParticipantService/ParticipantService.h"
#include "src/service/CameraService/CameraService.h"
#include "src/service/RobotService/RobotService.h"

enum ServiceEnum {
    PARTICIPANT_SERVICE = 0,
    CAMERA_SERVICE,
    ROBOT_SERVICE
};

class SingletonServiceProvider {
public:
    static inline Service *GetService(ServiceEnum service_name) {
        Service *result;
        try {
            result = services_map->at(service_name);
        } catch (const std::out_of_range &e) {
            result = CreateService(service_name);
        }
        if (result == nullptr) {
            throw std::runtime_error("SingletonServiceProvider: can't find or create the service!");
        } else {
            return result;
        }
    };

    static inline bool RemoveService(ServiceEnum service_name) {
        auto service_pointer= GetService(service_name);
        bool result = services_map->erase(service_name);
        delete service_pointer;
        return result;
    };

    static inline void RemoveAllServices() {
        std::list<Service*> service_refs;
        for (const auto& service_pair: *services_map) {
            service_refs.push_back(service_pair.second);
        }
        services_map->clear();
        for (const Service *service_ref: service_refs) {
            delete service_ref;
        }
    };

private:
    static inline std::map<ServiceEnum, Service *> *services_map = new std::map<ServiceEnum, Service *>();

    static inline Service *CreateService(ServiceEnum service_name) {
        switch (service_name) {
            case PARTICIPANT_SERVICE: {
                const auto [iterator, success] = services_map->insert(
                        {service_name, (Service *) new ParticipantService()});
                if (success) {
                    return iterator->second;
                } else {
                    return nullptr;
                }
                break;
            }
            case CAMERA_SERVICE:{
                const auto [iterator, success] = services_map->insert(
                        {service_name, (Service *) new CameraService()});
                if (success) {
                    return iterator->second;
                } else {
                    return nullptr;
                }
                break;
            }
            case ROBOT_SERVICE:{
                const auto [iterator, success] = services_map->insert(
                        {service_name, (Service *) new RobotService()});
                if (success) {
                    return iterator->second;
                } else {
                    return nullptr;
                }
                break;
            }
        }
    }
};


#endif //ARCH_TEST_PROJECT_SINGLETONSERVICEPROVIDER_H
