#include "CameraWorker.h"

CameraWorker::CameraWorker(mINI::INIStructure &config, SharedDataCameraOutput &shared_data_camera_output,
                           std::mutex &shared_data_camera_output_mutex, SharedDataCameraInput &shared_data_camera_input,
                           std::mutex &shared_data_camera_input_mutex) : config(config),
                                                                         shared_data_camera_output(shared_data_camera_output),
                                                                         shared_data_camera_output_mutex(shared_data_camera_output_mutex),
                                                                         shared_data_camera_input(shared_data_camera_input),
                                                                         shared_data_camera_input_mutex(shared_data_camera_input_mutex) {

}

void CameraWorker::operator()(const std::stop_token &stop_token) {
    while (!stop_token.stop_requested()) {
        try {
            bool dataAvailable = false;
            bool streamFrames = false;

            while (!stop_token.stop_requested()) {
                Connect(stop_token);

                bool has_command;
                {
                    std::lock_guard<std::mutex> lock(shared_data_camera_input_mutex);
                    has_command = shared_data_camera_input.has_command;
                }
                if (has_command) {
                    // GET COMMAND LIST
                    std::list<std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE>> command_list = std::list<std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE>>();
                    {
                        std::lock_guard<std::mutex> lock(shared_data_camera_input_mutex);
                        std::copy(shared_data_camera_input.command_list.begin(),
                                  shared_data_camera_input.command_list.end(),
                                  std::back_inserter(command_list));
                        shared_data_camera_input.command_list.clear();
                        shared_data_camera_input.has_command = false;
                    }
                    // PROCESS COMMAND LIST
                    for (const std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE> &command: command_list) {
                        ProcessCommand(stop_token, command);
                    }
                }

                if (!camera_info_loaded) {
                    if (rtProtocol.ReadGeneralSettings()) {
                        camera_info_loaded = true;
                    }
                    {
                        const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                        shared_data_camera_output.camera_info.cameras_number = rtProtocol.GetCameraCount();
                        shared_data_camera_output.camera_info.markers_number = rtProtocol.Get3DLabeledMarkerCount();
                        shared_data_camera_output.camera_info.motion_capture_sys_frequency = rtProtocol.GetSystemFrequency();
                    }
                }

                if (!dataAvailable) {
                    if (!rtProtocol.Read3DSettings(dataAvailable)) {
                        spdlog::error("CameraReader(): rtProtocol.Read3DSettings {}", rtProtocol.GetErrorString());
                        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                        continue;
                    }
                }

                if (!streamFrames) {
                    if (!rtProtocol.StreamFrames(CRTProtocol::RateAllFrames, 0, 0, nullptr,
                                                 CRTProtocol::cComponent3d)) {
                        spdlog::error("CameraReader(): rtProtocol.StreamFrames {}", rtProtocol.GetErrorString());
                        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                        continue;
                    }
                    streamFrames = true;

                    spdlog::info("CameraReader(): Starting to streaming 3D data");
                }

                CRTPacket::EPacketType packetType;
                /*std::cout << "CAMERA_WORKER: Waiting for packet...\n";*/
                if (rtProtocol.Receive(packetType, true, 500000) == CNetwork::ResponseType::success) {
                    if (packetType == CRTPacket::PacketData) {
                        float fX, fY, fZ;

                        CRTPacket *rtPacket = rtProtocol.GetRTPacket();

                        auto t_now = std::chrono::high_resolution_clock::now();
                        if (rtPacket->Get3DMarkerCount() > 0) {
                            {
                                const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                                shared_data_camera_output.camera_info.frame_counter = rtPacket->GetFrameNumber();
                            }
                            for (unsigned int i = 0; i < rtPacket->Get3DMarkerCount(); i++) {
                                const char *tmpStr = rtProtocol.Get3DLabelName(i);

                                if (rtPacket->Get3DMarker(i, fX, fY, fZ)) {
                                    // Convert everything to meters (SI units used everywhere)
                                    fX = fX / 1000.0f;
                                    fY = fY / 1000.0f;
                                    fZ = fZ / 1000.0f;
                                    Eigen::Vector3d marker_pos = Eigen::Vector3d(fX, fY, fZ);
                                    std::chrono::duration<double> diff = t_now -
                                                                         shared_data_camera_output.hand_measurement_timestamp;

                                    if (tmpStr != nullptr && strcmp(tmpStr, "hand_end") == 0) {
                                        /*std::cout << "CAMERA_WORKER: It is hand end packet with coordinates: " << marker_pos << "\n";*/
                                        const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                                        shared_data_camera_output.hand_vel_m_ns =
                                                (shared_data_camera_output.hand_pos_m - marker_pos) / diff.count();
                                        shared_data_camera_output.hand_pos_m = marker_pos;
                                        shared_data_camera_output.hand_measurement_timestamp = t_now;
                                    }
                                    if (tmpStr != nullptr && strcmp(tmpStr, "elbow") == 0) {
                                        const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                                        shared_data_camera_output.elbow_pos = marker_pos;
                                        shared_data_camera_output.elbow_measurement_timestamp = t_now;
                                    }
                                    if (tmpStr != nullptr && strcmp(tmpStr, "hand_left") == 0) {
                                        const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                                        shared_data_camera_output.hand_left_pos = marker_pos;
                                        shared_data_camera_output.hand_left_measurement_timestamp = t_now;
                                    }
                                    if (tmpStr != nullptr && strcmp(tmpStr, "robot_end") == 0) {
                                        const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                                        shared_data_camera_output.robot_vel =
                                                (shared_data_camera_output.robot_pos - marker_pos) / diff.count();
                                        shared_data_camera_output.robot_pos = marker_pos;
                                        shared_data_camera_output.robot_measurement_timestamp = t_now;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Disconnect(stop_token);
            spdlog::info("CameraReader(): exitting");
        } catch (std::exception &e) {
            spdlog::error("CameraReader(): {}", e.what());
        }
    }
}

void CameraWorker::Connect(const std::stop_token &stop_token) {
    while (!rtProtocol.Connected() && !stop_token.stop_requested()) {
        auto aaa = config["CameraReader"]["port"];
        if (!rtProtocol.Connect(
                config["CameraReader"]["ip"].c_str(),
                std::stoi(config["CameraReader"]["port"]),
                &udpPort,
                std::stoi(config["CameraReader"]["versionMajor"]),
                std::stoi(config["CameraReader"]["versionMinor"]),
                std::stoi(config["CameraReader"]["bigEndian"])
        )
                ) {
            spdlog::error("CameraReader(): rtProtocol.Connect {}", rtProtocol.GetErrorString());
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            continue;
        } else {
            while (!rtProtocol.Connected() && !stop_token.stop_requested()) {
                spdlog::error("CameraReader() not connected!");
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
            spdlog::warn("CameraReader() is connected!");
            if (!rtProtocol.TakeControl(config["CameraReader"]["password"].c_str())) {
                spdlog::error("CameraReader(): rtProtocol.TakeControl {}", rtProtocol.GetErrorString());
            }
            spdlog::warn("CameraReader() has control!");
            while (!rtProtocol.IsControlling() && !stop_token.stop_requested()) {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
            spdlog::info("CameraReader(): connected and has control.");
            camera_info_loaded = false;
            {
                const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
                shared_data_camera_output.camera_info.camera_worker_status = CONNECTED_AND_NOT_CAPTURING;
            }
        }
    }
}

void CameraWorker::Disconnect(const std::stop_token &stop_token) {
    if (rtProtocol.Connected()) {
        // Forced saving, disconnect etc.
        if (rtProtocol.StopCapture()) {
            std::string qtm_filename_prefix;
            {
                std::lock_guard<std::mutex> lock(shared_data_camera_input_mutex);
                qtm_filename_prefix = shared_data_camera_input.qtm_filename_prefix;
            }

            char new_filename[300];
            std::string str = qtm_filename_prefix + "_ERR_" + date::format("%F_%H-%M",
                                                                           std::chrono::system_clock::now()) +
                              ".qtm";
            const char *filename = str.c_str();
            rtProtocol.SaveCapture(filename, false, new_filename, sizeof(new_filename));
            if (strlen(new_filename) == 0) {
                spdlog::info("CameraReader(): measurement saved as " + str);
            } else {
                spdlog::info("CameraReader(): measurement saved as {}", new_filename);
            }
        }

        rtProtocol.CloseMeasurement();
        spdlog::info("CameraReader(): Measurement closed");

        rtProtocol.ReleaseControl();
        spdlog::info("CameraReader(): Released control");

        rtProtocol.Disconnect();
        spdlog::info("CameraReader(): disconnected");
    }
    {
        const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
        shared_data_camera_output.camera_info.camera_worker_status = DISCONNECTED;
    }
}

void CameraWorker::ProcessCommand(const std::stop_token &stop_token,
                                  const std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE> &command) {
    if (command.second == STOP_AND_SAVE_CAPTURE) {
        //close previous measurement and save data
        while (rtProtocol.StopCapture() && !stop_token.stop_requested()) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        spdlog::info("CameraReader(): Stopped capturing");
        char new_filename[300];
        std::string qtm_filename_prefix;
        {
            std::lock_guard<std::mutex> lock(shared_data_camera_input_mutex);
            qtm_filename_prefix = shared_data_camera_input.qtm_filename_prefix;
        }
        std::string str = qtm_filename_prefix + "_" +
                          (date::format("%F_%H-%M",
                                        std::chrono::system_clock::now())) + ".qtm";
        const char *filename = str.c_str();
        while (!rtProtocol.SaveCapture(filename, false, new_filename, sizeof(new_filename)) &&
               !stop_token.stop_requested()) {
            std::cout << "CameraReader(): rtProtocol.SaveCapture " << rtProtocol.GetErrorString()
                      << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        if (strlen(new_filename) == 0) {
            spdlog::info("CameraReader(): measurement saved as " + str);
        } else {
            spdlog::info("CameraReader(): measurement saved as {}", new_filename);
        }
        while (!rtProtocol.CloseMeasurement() && !stop_token.stop_requested()) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        spdlog::info("CameraReader(): Measurement closed");
        {
            const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
            shared_data_camera_output.event_triggered = true;
            shared_data_camera_output.event = CAPTURE_STOPPED_AND_SAVED;
            shared_data_camera_output.camera_info.camera_worker_status = CONNECTED_AND_NOT_CAPTURING;
            shared_data_camera_output.camera_info.frame_counter = 0;
        }
    } else if (command.second == START_CAPTURE) {
        //start new
        while (!rtProtocol.Connected() && !stop_token.stop_requested()) {
            spdlog::error("CameraReader() not connected!");
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        while (!rtProtocol.IsControlling() && !stop_token.stop_requested()) {
            spdlog::error("CameraReader() no control!");
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        while (!rtProtocol.NewMeasurement() && !stop_token.stop_requested()) {
            spdlog::error("CameraReader(): rtProtocol.NewMeasurement '{}'",
                          rtProtocol.GetErrorString());
            while (rtProtocol.StopCapture() && !stop_token.stop_requested()) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
            spdlog::info("CameraReader(): Stopped capturing");
            char new_filename[300];
            std::string filename = "unknown_measurement_" +
                                   (date::format("%F_%H-%M",
                                                 std::chrono::system_clock::now())) + ".qtm";
            while (!rtProtocol.SaveCapture(filename.c_str(), false, new_filename,
                                           sizeof(new_filename)) && !stop_token.stop_requested()) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
            if (strlen(new_filename) == 0) {
                spdlog::info("CameraReader(): measurement saved as unknown_measurement.qtm");
            } else {
                spdlog::info("CameraReader(): measurement saved as {}", new_filename);
            }
            rtProtocol.CloseMeasurement();
        }
        while (!rtProtocol.StartCapture() && !stop_token.stop_requested()) {
            spdlog::error("CameraReader(): rtProtocol.StartCapture {}",
                          rtProtocol.GetErrorString());
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        {
            const std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
            shared_data_camera_output.camera_info.last_capture_start_datetime = date::format("%F %T",
                                                                                             std::chrono::system_clock::now());
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // wait till recording starts
        spdlog::info("CameraReader(): started recording");
        {
            std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
            shared_data_camera_output.event_triggered = true;
            shared_data_camera_output.event = CAPTURE_STARTED;
            shared_data_camera_output.camera_info.camera_worker_status = CONNECTED_AND_CAPTURING;
        }
    } else if (command.second == SEND_QTM_EVENT) {
        if (!rtProtocol.SetQTMEvent(command.first.c_str())) {
            spdlog::error("CameraReader(): rtProtocol.SetQTMEvent {} - for", rtProtocol.GetErrorString(),
                          command.first.c_str());
        }
    }
}
