#ifndef QUALISYSKINOVAINTERFACE_CAMERAWORKER_H
#define QUALISYSKINOVAINTERFACE_CAMERAWORKER_H

#include <string>
#include <iostream>
#include <stop_token>
#include "spdlog/spdlog.h"
#include "RTProtocol.h"
#include "RTPacket.h"
#include "ini.h"
#include "lib/date/date.h"
#include <thread>
#include <Eigen/Core>
#include <list>

enum CameraWorkerStatus {
    CONNECTED_AND_CAPTURING,
    CONNECTED_AND_NOT_CAPTURING,
    DISCONNECTED
};

enum CAMERA_INPUT_COMMAND_TYPE {
    STOP_AND_SAVE_CAPTURE,
    START_CAPTURE,
    SEND_QTM_EVENT
};


enum CAMERA_EVENT_TYPE {
    CAPTURE_STOPPED_AND_SAVED,
    CAPTURE_STARTED,
    CAMERA_EVENT_UNDEFINED
};

struct SharedDataCameraInput {
    bool has_command = false;
    std::string qtm_filename_prefix;
    std::list<std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE>> command_list;
};

struct CameraInfo {
    CameraWorkerStatus camera_worker_status = DISCONNECTED;
    std::string last_capture_start_datetime;
    unsigned int frame_counter = 0;
    int cameras_number = -1; // todo:: always equals to zero BUG
    int markers_number = -1;
    int motion_capture_sys_frequency = -1;
};

struct SharedDataCameraOutput {
    CameraInfo camera_info;
    bool event_triggered = false;
    CAMERA_EVENT_TYPE event;
    Eigen::Vector3d hand_pos_m = Eigen::Vector3d::Zero();
    Eigen::Vector3d hand_vel_m_ns = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point hand_measurement_timestamp;
    Eigen::Vector3d robot_pos = Eigen::Vector3d::Zero();
    Eigen::Vector3d robot_vel = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point robot_measurement_timestamp;
    Eigen::Vector3d hand_left_pos = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point hand_left_measurement_timestamp;
    Eigen::Vector3d elbow_pos = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point elbow_measurement_timestamp;
};


class CameraWorker {
public:
    CameraWorker(mINI::INIStructure &config,
                 SharedDataCameraOutput &shared_data_camera_output,
                 std::mutex &shared_data_camera_output_mutex,
                 SharedDataCameraInput &shared_data_camera_input,
                 std::mutex &shared_data_camera_input_mutex);

    void operator()(const std::stop_token &stop_token);

private:
    CRTProtocol rtProtocol;
    unsigned short udpPort = 0;
    bool camera_info_loaded = false;
    mINI::INIStructure &config;
    SharedDataCameraOutput &shared_data_camera_output;
    std::mutex &shared_data_camera_output_mutex;
    SharedDataCameraInput &shared_data_camera_input;
    std::mutex &shared_data_camera_input_mutex;

    void Connect(const std::stop_token &stop_token);

    void Disconnect(const std::stop_token &stop_token);

    void
    ProcessCommand(const std::stop_token &stop_token, const std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE> &command);
};


#endif //QUALISYSKINOVAINTERFACE_CAMERAWORKER_H
