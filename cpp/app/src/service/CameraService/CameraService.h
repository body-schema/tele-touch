#ifndef QUALISYSKINOVAINTERFACE_CAMERASERVICE_H
#define QUALISYSKINOVAINTERFACE_CAMERASERVICE_H


#include <mutex>
#include "src/service/CameraService/worker/CameraWorker.h"
#include "src/service/Service.h"

struct CamerasSettings {
    int cameras_number;
    int markers_number;
    int camera_frequency;
};

struct PositionAndVelocity {
    Eigen::Vector3d position_m = Eigen::Vector3d::Zero();
    Eigen::Vector3d velocity_m_s = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point measurement_timestamp;
};

class CameraService : public Service {

public:
    CameraService(const std::string &qtm_filename_prefix = "");
    ~CameraService();

    Eigen::Vector3d GetHandPosition();
    Eigen::Vector3d GetRobotPosition();
    void SetQtmFilenamePrefix(const std::string &qtm_filename_prefix);
    void AddCommand(const std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE> &command);
    CamerasSettings GetCamerasSettings();
    double GetParticipantArmLengthCm();
    CameraInfo GetCameraInfo();
    unsigned int GetCurrentFrameNumber();
    CameraWorkerStatus GetCameraStatus();
    PositionAndVelocity GetHandEndPositionAndVelocity();


private:
    SharedDataCameraOutput shared_data_camera_output;
    std::mutex shared_data_camera_output_mutex;
    SharedDataCameraInput shared_data_camera_input;
    std::mutex shared_data_camera_input_mutex;
    mINI::INIStructure config;
    std::jthread camera_thread;

    CameraWorker *camera_worker;

};


#endif //QUALISYSKINOVAINTERFACE_CAMERASERVICE_H
