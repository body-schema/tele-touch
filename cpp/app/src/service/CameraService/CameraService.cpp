#include "CameraService.h"

CameraService::CameraService(const std::string &qtm_filename_prefix) {
    SetQtmFilenamePrefix(qtm_filename_prefix);
    mINI::INIFile configFile("./config/settings.ini");
    configFile.read(config);
    camera_worker = new CameraWorker(config, shared_data_camera_output, shared_data_camera_output_mutex,
                                     shared_data_camera_input, shared_data_camera_input_mutex);
    camera_thread = std::jthread(*camera_worker);
}

CameraService::~CameraService() {
    camera_thread.request_stop();
    camera_thread.join();
}

Eigen::Vector3d CameraService::GetHandPosition() {
    std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex); //todo: use r/w locks
    return shared_data_camera_output.hand_pos_m;
}

Eigen::Vector3d CameraService::GetRobotPosition() {
    std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
    return shared_data_camera_output.robot_pos;
}

void CameraService::SetQtmFilenamePrefix(const std::string &qtm_filename_prefix) {
    std::lock_guard<std::mutex> lock(shared_data_camera_input_mutex);
    shared_data_camera_input.qtm_filename_prefix = qtm_filename_prefix;
}

void CameraService::AddCommand(const std::pair<std::string, CAMERA_INPUT_COMMAND_TYPE> &command) {
    std::lock_guard<std::mutex> lock(shared_data_camera_input_mutex);
    shared_data_camera_input.command_list.push_back(command);
    shared_data_camera_input.has_command = true;
}

CameraWorkerStatus CameraService::GetCameraStatus() {
    std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
    return shared_data_camera_output.camera_info.camera_worker_status;
}

unsigned int CameraService::GetCurrentFrameNumber() {
    std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
    return shared_data_camera_output.camera_info.frame_counter;
}

PositionAndVelocity CameraService::GetHandEndPositionAndVelocity() {
    std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
    PositionAndVelocity positionAndVelocity;
    positionAndVelocity.position_m = shared_data_camera_output.hand_pos_m;
    positionAndVelocity.velocity_m_s = shared_data_camera_output.hand_vel_m_ns;
    positionAndVelocity.measurement_timestamp = shared_data_camera_output.hand_measurement_timestamp;
    return positionAndVelocity;
}

CamerasSettings CameraService::GetCamerasSettings() {
    CamerasSettings cameras_settings = CamerasSettings();
    {
        std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
        cameras_settings.cameras_number = shared_data_camera_output.camera_info.cameras_number;
        cameras_settings.markers_number = shared_data_camera_output.camera_info.markers_number;
        cameras_settings.camera_frequency = shared_data_camera_output.camera_info.motion_capture_sys_frequency;
    }
    return cameras_settings;
}

double CameraService::GetParticipantArmLengthCm() {
    std::lock_guard<std::mutex> lock(shared_data_camera_output_mutex);
    double result = fmul(shared_data_camera_output.hand_left_pos.x() - shared_data_camera_output.elbow_pos.x(),
                         100); // meters to cm
    return result;
}
