#include "RobotService.h"
#include "src/service/SingletonServiceProvider.h"

RobotService::RobotService() {
    camera_service = (CameraService *) SingletonServiceProvider::GetService(CAMERA_SERVICE);
    mINI::INIFile configFile("./config/settings.ini");
    configFile.read(config);
    robot_worker = new RobotWorker(config, camera_service, shared_data_robot_input, shared_data_robot_input_mutex,
                                   shared_data_robot_output, shared_data_robot_output_mutex);
    robot_thread = std::jthread(*robot_worker);
}

RobotService::~RobotService() {
    robot_thread.request_stop();
    robot_thread.join();
}

void RobotService::MoveToNearHomeArea() {
    std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
    shared_data_robot_input.command_list.emplace_back("", ROBOT_INPUT_COMMAND_TYPE::GO_NEAR_HOME_AREA);
    shared_data_robot_input.has_command = true;
}

void RobotService::StartHandFollowing() {
    std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
    shared_data_robot_input.command_list.emplace_back("", ROBOT_INPUT_COMMAND_TYPE::FOLLOW_THE_HAND);
    shared_data_robot_input.has_command = true;
}

void RobotService::MoveToHome() {
    std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
    shared_data_robot_input.command_list.emplace_back("", ROBOT_INPUT_COMMAND_TYPE::GO_HOME);
    shared_data_robot_input.has_command = true;
}

void RobotService::StopHandFollowing() {
    MoveToNearHomeArea();
}

RobotStatus RobotService::GetRobotStatus() {
    std::lock_guard<std::mutex> lock(shared_data_robot_output_mutex);
    return shared_data_robot_output.robot_worker_status;
}

HandTrackingRobotInfo RobotService::GetHandTrackingRobotInfo() {
    std::lock_guard<std::mutex> lock(shared_data_robot_output_mutex);
    return shared_data_robot_output.hand_tracking_robot_info;
}

void RobotService::UpdateGainRatio(double new_ratio) {
    if (new_ratio <= 2 && new_ratio >= 0.5) {
        std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
        shared_data_robot_input.ratio = new_ratio;
    }
}

void RobotService::Start2DHandFollowing() {
    std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
    shared_data_robot_input.command_list.emplace_back("", ROBOT_INPUT_COMMAND_TYPE::FOLLOW_THE_HAND_2D);
    shared_data_robot_input.has_command = true;
}

void RobotService::Stop2DHandFollowing() {
    MoveToHome();
}
