#ifndef QUALISYSKINOVAINTERFACE_ROBOTSERVICE_H
#define QUALISYSKINOVAINTERFACE_ROBOTSERVICE_H


#include <thread>
#include "src/service/Service.h"
#include "ini.h"
#include "src/service/RobotService/worker/RobotWorker.h"
#include "src/service/CameraService/CameraService.h"

class RobotService : public Service {

public:
    RobotService();
    ~RobotService();

    void MoveToNearHomeArea();
    void MoveToHome();
    void StartHandFollowing();
    void Start2DHandFollowing();
    void Stop2DHandFollowing();
    void StopHandFollowing();
    HandTrackingRobotInfo GetHandTrackingRobotInfo();
    RobotStatus GetRobotStatus();
    void UpdateGainRatio(double new_ratio);

private:
    mINI::INIStructure config;
    std::jthread robot_thread;
    CameraService *camera_service;
    RobotWorker *robot_worker;

    SharedDataRobotInput shared_data_robot_input;
    std::mutex shared_data_robot_input_mutex;
    SharedDataRobotOutput shared_data_robot_output;
    std::mutex shared_data_robot_output_mutex;
};


#endif //QUALISYSKINOVAINTERFACE_ROBOTSERVICE_H
