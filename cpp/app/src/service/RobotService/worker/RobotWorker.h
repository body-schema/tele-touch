#ifndef QUALISYSKINOVAINTERFACE_ROBOTWORKER_H
#define QUALISYSKINOVAINTERFACE_ROBOTWORKER_H

#include <Eigen/Dense>
#include <thread>
#include <deque>
#include <utility>
#include <random>
#include "spdlog/spdlog.h"
#include "src/datastruct/BehavioralInformation.h"
#include "lib/date/date.h"

#include "json.h"
#include "shared_memory.h"
#include "src/legacy/robot/kortex_controller.h"
#include "src/legacy/robot/ik_computer.h"
#include "src/service/CameraService/CameraService.h"
#include "src/legacy/robot/MovingWindow/MovingWindow.h"
#include "src/service/RobotService/RobotStatusEnum.h"
#include "src/utility/PositionComputer.h"

using json = nlohmann::json;
using TimePoint = std::chrono::high_resolution_clock::time_point;
using Point = std::pair<Eigen::Vector3d, TimePoint>;

// Assuming 0.1mm might be detectable by the FK model
#define DETECTABLE_DISTANCE_M 0.0001

struct Transformation {
    Eigen::Matrix3d rotation = Eigen::Matrix3d::Zero();
    Eigen::Vector3d translation = Eigen::Vector3d::Zero();
};

enum class ROBOT_INPUT_COMMAND_TYPE {
    GO_HOME/*, Sam*/,
    FOLLOW_THE_HAND,
    FOLLOW_THE_HAND_2D,
    GO_NEAR_HOME_AREA
};

struct SharedDataRobotInput {
    double ratio = 1.;
    SCENE_TYPE scene = SCENE_CAPTURE_STARTING;
    std::chrono::high_resolution_clock::time_point scene_change_timestamp;
    double max_distance_cm = 21.; // Todo: use information from the config file by default
    bool has_command = false;
    std::list<std::pair<std::string, ROBOT_INPUT_COMMAND_TYPE>> command_list;
};

struct HandTrackingRobotInfo {
    Eigen::Vector3d movement_tracking_robot_initial_coordinates = Eigen::Vector3d::Zero();;
    Eigen::Vector3d movement_tracking_MW_begin_coordinates = Eigen::Vector3d::Zero();;
    Eigen::Vector3d movement_tracking_robot_final_coordinates = Eigen::Vector3d::Zero();;
    Eigen::Vector3d movement_tracking_MW_end_coordinates = Eigen::Vector3d::Zero();;
    std::string movement_tracking_sync_date_time;
};

struct SharedDataRobotOutput {
    RobotStatus robot_worker_status = RobotStatus::DISCONNECTED;
    HandTrackingRobotInfo hand_tracking_robot_info;
};

class RobotWorker {
private:
    void ChangeRobotWorkerStatus(RobotStatus new_robot_worker_status);

    mINI::INIStructure &config;
    SharedDataRobotInput &shared_data_robot_input;
    std::mutex &shared_data_robot_input_mutex;
    SharedDataRobotOutput &shared_data_robot_output;
    std::mutex &shared_data_robot_output_mutex;
    KortexController *kc = nullptr;
    IKComputer *ikc = nullptr;
    Transformation TRANSFORMATION;
    double X_MIN, X_MAX, Y_MIN, Y_MAX, Z_MIN, Z_MAX;
    double MAX_VEL_NORM, MAX_VEL_COMPENSATION_MULTIPLIER, MAX_VEL_DECREASE_MULTIPLIER, MAX_DIST_RECOVERY_MULTIPLIER;
    Eigen::AlignedBox3d BOUNDARY_BOX;
    Eigen::Vector3d MIN_VECT, MAX_VECT;
    Eigen::Vector3d ROBOT_POS_INIT_R;
    int MOVING_WINDOW_SIZE, MOVING_WINDOW_DEADLINE_MS;
    double MOVING_WINDOW_ACTIVE_FROM_SEC;
    std::mt19937 random_engine;
    std::uniform_real_distribution<double> random_hand_offset_x, random_hand_offset_y, random_hand_offset_z;
    CameraService *camera_service;
    RobotStatus robot_worker_status;
    std::deque<TRAJECTORY_TYPE> trajectory_queue;
    bool is_hand_offset_init = false;
    bool use_predefined_trajectory = true;
    bool trajectory_started = false;
    bool trajectory_done_caused_status_change = false;

public:
    const Eigen::Vector3d HAND_AXES = Eigen::Vector3d(1., 0., 0.);
    const Eigen::Vector3d HAND_AXES_INVERSE = -1 * (HAND_AXES - Eigen::Vector3d::Ones());

    const Eigen::Vector3d HAND_AXES_2D = Eigen::Vector3d(1., 1., 0.);
    const Eigen::Vector3d HAND_AXES_2D_INVERSE = -1 * (HAND_AXES_2D - Eigen::Vector3d::Ones());

    RobotWorker(mINI::INIStructure &config,
                CameraService *camera_service,
                SharedDataRobotInput &shared_data_robot_input,
                    std::mutex &shared_data_robot_input_mutex,
                SharedDataRobotOutput &shared_data_robot_output,
                std::mutex &shared_data_robot_output_mutex);

    Eigen::Vector3d transform_rob2cam(Eigen::Vector3d position);

    Eigen::Vector3d transform_cam2rob(Eigen::Vector3d position);

    bool calibration();

    void check_robot_model_FK(std::stop_token should_run, KortexController *kc);

    Eigen::Vector3d compute_trajectory_goal(
            const TRAJECTORY_TYPE &trajectory,
            Eigen::Vector3d &robot_pos,
            Eigen::Vector3d &trajectory_start_pos,
            Eigen::Vector3d &trajectory_random_pos,
            TimePoint &trajectory_start_time,
            bool &trajectory_done
    );

    Eigen::Vector3d compute_goal_position(
            Eigen::Vector3d &hand_pos,
            Eigen::Vector3d &hand_vel,
            TimePoint &hand_measurement_timestamp,
            Eigen::Vector3d &hand_offset_pos,
            Eigen::Vector3d &robot_offset_pos,
            Eigen::Vector3d &trajectory_pos,
            double &ratio
    );

    Eigen::Vector3d compute_2D_position(
            Eigen::Vector3d &hand_pos,
            Eigen::Vector3d &hand_offset_pos,
            Eigen::Vector3d &robot_offset_pos
    );

    bool verify_position(Eigen::Vector3d &goal_pos);

    bool position_2d_speed_is_under_limits(
            MovingWindow &mw_robot_pos,
            Vector3d &goal_pos,
            TimePoint &joint_control_timestamp
    );

    Eigen::Vector3d get_hand_offset_pos();

    void get_shared_data(
            SharedDataRobot &shared_data_robot,
            std::mutex &shared_data_robot_mutex,
            double &ratio,
            double &max_distance_cm,
            Eigen::Vector3d &hand_pos_r,
            Eigen::Vector3d &hand_vel, // should be in _r also
            TimePoint &hand_measurement_timestamp,
            Matrix<double, 7, 1> &joint_angles,
            Matrix<double, 7, 1> &angular_velocities,
            TimePoint &joint_measurement_timestamp,
            TimePoint &joint_control_timestamp
    );

    void operator()(std::stop_token should_run);
};


#endif //QUALISYSKINOVAINTERFACE_ROBOTWORKER_H
