#include "RobotWorker.h"

RobotWorker::RobotWorker(mINI::INIStructure &config, CameraService *camera_service,
                         SharedDataRobotInput &shared_data_robot_input,
                         mutex &shared_data_robot_input_mutex, SharedDataRobotOutput &shared_data_robot_output,
                         std::mutex &shared_data_robot_output_mutex)
        : config{config},
          camera_service{camera_service},
          shared_data_robot_input{shared_data_robot_input},
          shared_data_robot_input_mutex{shared_data_robot_input_mutex},
          shared_data_robot_output{shared_data_robot_output},
          shared_data_robot_output_mutex{shared_data_robot_output_mutex} {
    try {
        ROBOT_POS_INIT_R = Eigen::Vector3d(
                std::stod(config["RobotController"]["x_init"]),
                std::stod(config["RobotController"]["y_init"]),
                std::stod(config["RobotController"]["z_init"])
        );
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse x_init, y_init, z_init: {}", e.what());
        throw std::invalid_argument("RobotController: x,y,z initial position must be set");
    }
    try {
        MAX_VEL_NORM = std::stod(config["RobotController"]["norm_cartesian_vel_max"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse norm_cartesian_vel_max: {}", e.what());
        throw std::invalid_argument("RobotController: Cartesian maximal velocity norm must be set");
    }
    try {
        MAX_VEL_COMPENSATION_MULTIPLIER = std::stod(config["RobotController"]["max_vel_compensation_multiplier"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse max_vel_compensation_multiplier: {}", e.what());
        throw std::invalid_argument("RobotController: Norm compensation multiplier must be set");
    }
    try {
        MAX_VEL_DECREASE_MULTIPLIER = std::stod(config["RobotController"]["max_vel_decrease_multiplier"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse max_vel_decrease_multiplier: {}", e.what());
        throw std::invalid_argument("RobotController: Max vel decreasing multiplier must be set");
    }
    try {
        MAX_DIST_RECOVERY_MULTIPLIER = std::stod(config["RobotController"]["max_dist_recovery_multiplier"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse max_dist_recovery_multiplier: {}", e.what());
        throw std::invalid_argument("RobotController: Max distance recovery multiplier must be set");
    }
    try {
        MOVING_WINDOW_SIZE = std::stoi(config["RobotController"]["moving_window_size"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse moving_window_size: {}", e.what());
        throw std::invalid_argument("RobotController: Moving window size must be set");
    }
    try {
        MOVING_WINDOW_DEADLINE_MS = std::stoi(config["RobotController"]["moving_window_deadline_ms"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse moving_window_deadline_ms: {}", e.what());
        throw std::invalid_argument("RobotController: Moving window deadline for deletion must be set");
    }
    try {
        MOVING_WINDOW_ACTIVE_FROM_SEC = 0.01 * std::stod(config["RobotController"]["moving_window_active_from_ms"]);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse moving_window_active_from_ms: {}", e.what());
        throw std::invalid_argument("RobotController: Moving window activation zone must be set");
    }
    try {
        double hand_random_init_x_min = std::stod(config["RobotController"]["x_random_init_min"]);
        double hand_random_init_x_max = std::stod(config["RobotController"]["x_random_init_max"]);
        double hand_random_init_y_min = std::stod(config["RobotController"]["y_random_init_min"]);
        double hand_random_init_y_max = std::stod(config["RobotController"]["y_random_init_max"]);
        double hand_random_init_z_min = std::stod(config["RobotController"]["z_random_init_min"]);
        double hand_random_init_z_max = std::stod(config["RobotController"]["z_random_init_max"]);

        std::random_device rd;  // Will be used to obtain a seed for the random number engine
        random_engine = std::mt19937(rd()); // Standard mersenne_twister_engine seeded with rd()
        random_hand_offset_x = std::uniform_real_distribution<double>(hand_random_init_x_min, hand_random_init_x_max);
        random_hand_offset_y = std::uniform_real_distribution<double>(hand_random_init_y_min, hand_random_init_y_max);
        random_hand_offset_z = std::uniform_real_distribution<double>(hand_random_init_z_min, hand_random_init_z_max);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse x_random_init_min, x_random_init_max: {}", e.what());
        throw std::invalid_argument("RobotController: Initial random offset position must be set");
    }
    try {
        X_MIN = std::stod(config["RobotController"]["x_min"]);
        X_MAX = std::stod(config["RobotController"]["x_max"]);
        Y_MIN = std::stod(config["RobotController"]["y_min"]);
        Y_MAX = std::stod(config["RobotController"]["y_max"]);
        Z_MIN = std::stod(config["RobotController"]["z_min"]);
        Z_MAX = std::stod(config["RobotController"]["z_max"]);
        MIN_VECT = Eigen::Vector3d(X_MIN, Y_MIN, Z_MIN);
        MAX_VECT = Eigen::Vector3d(X_MAX, Y_MAX, Z_MAX);
        BOUNDARY_BOX = Eigen::AlignedBox3d(MIN_VECT, MAX_VECT);
    } catch (std::exception &e) {
        spdlog::error("RobotController: Unable to parse x_min, x_max, y_min, y_max, z_min, z_max: {}", e.what());
        throw std::invalid_argument("RobotController: x,y,z limits must be set");
    }
    spdlog::info("RobotController: XYZ settings: x<{}; {}>, y<{}; {}>, z<{}; {}>", X_MIN, X_MAX, Y_MIN, Y_MAX, Z_MIN,
                 Z_MAX);
    if (config["RobotController"]["transformation"].length() > 0) {
        try {
            json data = json::parse(config["RobotController"]["transformation"]);
            Transformation t;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    t.rotation(i, j) = data[0][i][j];
                }
                t.translation(i) = data[1][i][0];
            }
            spdlog::info("RobotController: Using calibration from settings");
            TRANSFORMATION = t;
        } catch (std::exception &e) {
            spdlog::error("RobotController: Unable to parse transformation json, {}", e.what());
        }
    }
}

Eigen::Vector3d RobotWorker::transform_rob2cam(Eigen::Vector3d position) {
    return (TRANSFORMATION.rotation * position) + TRANSFORMATION.translation;
}

Eigen::Vector3d RobotWorker::transform_cam2rob(Eigen::Vector3d position) {
    return (TRANSFORMATION.rotation.transpose() * (position - TRANSFORMATION.translation));
}

bool RobotWorker::calibration() {
    if (kc == nullptr) {
        spdlog::error("RobotController.calibration: Unable to run calibration, kortex not initialized.");
        return false;
    }

    spdlog::info("RobotController.calibration: Waiting for robot position from camera");
    Eigen::Vector3d robot_position_c = Eigen::Vector3d::Zero();
    while (robot_position_c.isZero()) {
        robot_position_c = camera_service->GetRobotPosition();
        if (!robot_position_c.isZero()) break;
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    spdlog::info("RobotController.calibration: Running calibration");

    int calibration_size = std::stoi(config["RobotController"]["calibration_size"]);
    calibration_size = calibration_size - calibration_size % 10;
    Eigen::MatrixXd camera_set(3, calibration_size);
    Eigen::MatrixXd robot_set(3, calibration_size);

    Eigen::Vector3d robot_position_r;
    auto mv_robot = [&](float n, int i, Eigen::Vector3d velocity) {
        kc->goContinuous(velocity * n);
        std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<int>(std::round(500. * n))));
        kc->goContinuous();
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        robot_position_c = camera_service->GetRobotPosition();
        robot_position_r = kc->feedback();

        camera_set(0, i) = robot_position_c.x();
        camera_set(1, i) = robot_position_c.y();
        camera_set(2, i) = robot_position_c.z();

        robot_set(0, i) = robot_position_r.x();
        robot_set(1, i) = robot_position_r.y();
        robot_set(2, i) = robot_position_r.z();
    };

    // Init safe position
    kc->goHome();

    kc->goContinuous(Eigen::Vector3d(0.2, 0.0, 0.));
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    kc->goContinuous(Eigen::Vector3d(0., 0., 0.));

    float n = 1.f;
    for (int i = 0; i < calibration_size;) {
        mv_robot(n, i++, Eigen::Vector3d(-.15, -.1, -.1));
        mv_robot(n, i++, Eigen::Vector3d(-.1, .1, 0.));
        mv_robot(n, i++, Eigen::Vector3d(.15, -.15, 0.));
        mv_robot(n, i++, Eigen::Vector3d(0., .15, .1));
        mv_robot(n, i++, Eigen::Vector3d(.1, .1, 0.));
        mv_robot(n, i++, Eigen::Vector3d(.12, -.12, .15));
        mv_robot(n, i++, Eigen::Vector3d(-.12, .12, -.15));
        mv_robot(n, i++, Eigen::Vector3d(0., -.2, 0.));
        mv_robot(n, i++, Eigen::Vector3d(0., .2, 0.2));
        mv_robot(n, i++, Eigen::Vector3d(0., -.1, -0.2));
        n *= .8;
    }
    kc->goHome();

    // Find transformation
    Eigen::Matrix t = Eigen::umeyama(robot_set, camera_set, false);
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            TRANSFORMATION.rotation(i, j) = t(i, j);
        }
        TRANSFORMATION.translation(i) = t(i, 3);
    }

    std::string json_transformation = "[[";
    for (int i = 0; i < 3; i++) {
        json_transformation += "[";
        for (int j = 0; j < 3; j++) {
            json_transformation += std::to_string(TRANSFORMATION.rotation(i, j));
            if (j < 2) json_transformation += ", ";
        }
        if (i < 2) json_transformation += "],";
        else
            json_transformation += "]],[[" + std::to_string(TRANSFORMATION.translation.x()) + "], [" +
                                   std::to_string(TRANSFORMATION.translation.y()) + "], [" +
                                   std::to_string(TRANSFORMATION.translation.z()) + "]]]";
    }

    // Calculate RMSE:
    Eigen::MatrixXd robot_set_calc = TRANSFORMATION.rotation * robot_set + TRANSFORMATION.translation;
    Eigen::MatrixXd error = (robot_set_calc - camera_set);
    float rmse = sqrt((error * error).sum() / calibration_size);
    spdlog::info("RobotController.calibration: RMSE {}", rmse);
    spdlog::info("RobotController.calibration: Transformation done {}", json_transformation);
    return true;
}

void RobotWorker::check_robot_model_FK(std::stop_token should_run, KortexController *kc) {
    // Way of checking the robot model vs reality
    ikc = new IKComputer(kc->feedbackRotation());
    Eigen::Vector3d positionDiffAvg = Eigen::Vector3d(0, 0, 0);
    double counter = 0;
    while (!should_run.stop_requested()) {
        Eigen::Matrix4d robot_ik = kc->full_feedback();
        Eigen::Matrix4d model_ik = ikc->fk_full(kc->get_joint_angles());
        Eigen::Matrix4d diff = (robot_ik - model_ik);
        Eigen::Vector3d positionDiff = Eigen::Vector3d(diff(0, 3), diff(1, 3), diff(2, 3));
        positionDiffAvg += positionDiff;
        counter += 1;
        std::cout << "\n\nRobot-Model FK norm:" << positionDiffAvg.norm() / counter << std::endl;
    }
}


Eigen::Vector3d RobotWorker::compute_trajectory_goal(const TRAJECTORY_TYPE &trajectory,
                                                     Vector3d &robot_pos, Vector3d &trajectory_start_pos,
                                                     Vector3d &trajectory_random_pos,
                                                     TimePoint &trajectory_start_time, bool &trajectory_done) {
    // All the inputs are in robot frame
    Eigen::Vector3d robot_target_pos = robot_pos;
    Eigen::Vector3d trajectory_goal_pos = robot_pos;
    double trajectory_duration_sec = 0.;
    TimePoint t_now = std::chrono::high_resolution_clock::now();

    if (!trajectory_started) {
        trajectory_start_pos = robot_pos; // Load current position and go to goal from there
        trajectory_start_time = t_now;
        trajectory_done = false;
        if (trajectory == TRAJECTORY_HOME_RANDOM) {
            trajectory_random_pos = Eigen::Vector3d(random_hand_offset_x(random_engine),
                                                    random_hand_offset_y(random_engine),
                                                    random_hand_offset_z(random_engine));
        }
        trajectory_started = true;
    }

    switch (trajectory) {
        case TRAJECTORY_RIGHT:
            trajectory_goal_pos = trajectory_start_pos + Eigen::Vector3d(0., 0.05, 0.);
            trajectory_duration_sec = 1.;
            break;
        case TRAJECTORY_HOME_RIGHT:
            trajectory_goal_pos = ROBOT_POS_INIT_R + Eigen::Vector3d(0., 0.05, 0.);
            trajectory_duration_sec = 3.;
            break;
        case TRAJECTORY_HOME_RANDOM:
            trajectory_goal_pos = ROBOT_POS_INIT_R + trajectory_random_pos;
            trajectory_duration_sec = 1.;
            break;
    }

    robot_target_pos = PositionComputer::generate_linear_trajectory(
            t_now,
            trajectory_start_time,
            trajectory_duration_sec,
            trajectory_start_pos,
            trajectory_goal_pos
    );

    if (robot_target_pos == trajectory_goal_pos) {
        trajectory_done = true;
    }

    Eigen::Vector3d robot_target_pos_limited = robot_target_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
    return robot_target_pos_limited;
}


Eigen::Vector3d
RobotWorker::compute_goal_position(Vector3d &hand_pos, Vector3d &hand_vel, TimePoint &hand_measurement_timestamp,
                                   Vector3d &hand_offset_pos, Vector3d &robot_offset_pos, Vector3d &trajectory_pos,
                                   double &ratio) {
    // All the inputs are in robot frame
    Eigen::Vector3d robot_target_pos;

    if (trajectory_pos == Eigen::Vector3d::Zero()) {
        Eigen::Vector3d hand_traget_pos = (hand_pos - hand_offset_pos) * ratio;
        Eigen::Vector3d robot_pos = ROBOT_POS_INIT_R;
        robot_target_pos = robot_pos.cwiseProduct(HAND_AXES_INVERSE) +
                           (robot_offset_pos + hand_traget_pos).cwiseProduct(HAND_AXES);
    } else {
        robot_target_pos = trajectory_pos;
    }

    Eigen::Vector3d robot_target_pos_limited = robot_target_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
    return robot_target_pos_limited;
}

Eigen::Vector3d
RobotWorker::compute_2D_position(Vector3d &hand_pos, Vector3d &hand_offset_pos, Vector3d &robot_offset_pos) {
    // All the inputs are in robot frame
    Eigen::Vector3d robot_target_pos;

    Eigen::Vector3d hand_target_pos = (hand_pos - hand_offset_pos);
    Eigen::Vector3d robot_pos = ROBOT_POS_INIT_R;
    robot_target_pos = robot_pos.cwiseProduct(HAND_AXES_2D_INVERSE) +
                           (robot_offset_pos + hand_target_pos).cwiseProduct(HAND_AXES_2D);

    Eigen::Vector3d robot_target_pos_limited = robot_target_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
    return robot_target_pos_limited;
}


bool RobotWorker::verify_position(Vector3d &goal_pos) {
    // All the inputs are in robot frame
    return BOUNDARY_BOX.contains(goal_pos);
}

bool RobotWorker::position_2d_speed_is_under_limits(MovingWindow &mw_robot_pos, Vector3d &goal_pos,
                                                    TimePoint &joint_control_timestamp) {
    // All the inputs are in robot frame
    Eigen::Vector3d intergoal_pos = goal_pos;
    const Point &point = mw_robot_pos.points().back();
    Eigen::Vector3d robot_pos = point.first;

    TimePoint now = std::chrono::high_resolution_clock::now();
    double dt = std::chrono::duration<double>(now - joint_control_timestamp).count();

    // max distance from the last frame - first limiting factor
    double max_distance = MAX_VEL_NORM * MAX_VEL_COMPENSATION_MULTIPLIER * dt;

    // calculate average velocity
    const std::deque<Point> &points = mw_robot_pos.points();
    double sum_distance = 0.;
    double sum_time = 0.;
    for (const Point &historical_point: points) {
        double historical_dt = std::chrono::duration<double>(now - historical_point.second).count();
        if (historical_dt >
            MOVING_WINDOW_ACTIVE_FROM_SEC) { // Take samples atleast 20ms old, otherwise, there is too much noise
            sum_distance += (robot_pos - historical_point.first).norm();
            sum_time += historical_dt;
        }
    }
    double average_velocity = 0.;
    if (sum_time != 0.) {
        average_velocity = sum_distance / sum_time;
    }

    double distance = (robot_pos - intergoal_pos).norm();

    return distance < max_distance && average_velocity < MAX_VEL_NORM;
}

Eigen::Vector3d RobotWorker::get_hand_offset_pos() {
    return transform_cam2rob(camera_service->GetHandPosition());
}

void RobotWorker::get_shared_data(SharedDataRobot &shared_data_robot, mutex &shared_data_robot_mutex, double &ratio,
                                  double &max_distance_cm,
                                  Vector3d &hand_pos_r, Vector3d &hand_vel, TimePoint &hand_measurement_timestamp,
                                  Matrix<double, 7, 1> &joint_angles, Matrix<double, 7, 1> &angular_velocities,
                                  TimePoint &joint_measurement_timestamp, TimePoint &joint_control_timestamp) {
    // Get shared data
    {
        std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
        ratio = shared_data_robot_input.ratio;
        max_distance_cm = shared_data_robot_input.max_distance_cm;
    }
    auto hand_info = camera_service->GetHandEndPositionAndVelocity();
    if (hand_info.position_m == Eigen::Vector3d::Zero()) {
        hand_pos_r = Eigen::Vector3d::Zero();
    } else {
        hand_pos_r = transform_cam2rob(hand_info.position_m);
    }
    hand_vel = hand_info.velocity_m_s;
    hand_measurement_timestamp = hand_info.measurement_timestamp;
    {
        std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
        joint_angles = shared_data_robot.joint_angles;
        angular_velocities = shared_data_robot.angular_velocities;
        joint_measurement_timestamp = shared_data_robot.joint_measurement_timestamp;
        joint_control_timestamp = shared_data_robot.joint_control_timestamp;
    }
}

void RobotWorker::operator()(std::stop_token should_run) {
    // initialize shared variables
    SharedDataRobot shared_data_robot;
    std::mutex shared_data_robot_mutex;

    // run loop
    while (!should_run.stop_requested()) {
        try {
            // Init KortexController
            kc = new KortexController(config, shared_data_robot, shared_data_robot_mutex);
            spdlog::info("RobotController.(): Connected!");
            ChangeRobotWorkerStatus(RobotStatus::CONNECTED);
            // Init calibration if there is no transformation
            if (TRANSFORMATION.rotation.isZero() && TRANSFORMATION.translation.isZero()) {
                calibration();
            }
            // Init starting position
            kc->goPosition(ROBOT_POS_INIT_R, Eigen::Vector3d(180., 0., 90.));

            ChangeRobotWorkerStatus(RobotStatus::MOVING_TO_HOME);

            while (!(kc->feedback() - ROBOT_POS_INIT_R).isZero(DETECTABLE_DISTANCE_M) && !should_run.stop_requested()) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }

            std::this_thread::sleep_for(std::chrono::seconds(1));

            ChangeRobotWorkerStatus(RobotStatus::CONNECTED_AND_AT_HOME);

            // Init inverse kinematics computer with initial feedback rotation
            ikc = new IKComputer(kc->feedbackRotation());

            // Run real-time loop thread
            std::jthread kortexThread(*kc);

            // Setup variables
            Eigen::Vector3d robot_offset_pos_r = ROBOT_POS_INIT_R;

            double max_distance_cm;
            double ratio;

            Eigen::Vector3d hand_pos_r, hand_vel, hand_offset_pos_r;
            TimePoint hand_measurement_timestamp;

            Matrix<double, 7, 1> joint_angles;
            Matrix<double, 7, 1> angular_velocities;
            Matrix<double, 7, 1> goal_joint_angles;
            TimePoint joint_measurement_timestamp;
            TimePoint goal_computed_timestamp;
            TimePoint joint_control_timestamp;

            MovingWindow mw_robot_pos_r(MOVING_WINDOW_SIZE, MOVING_WINDOW_DEADLINE_MS);
            mw_robot_pos_r.push_back(ROBOT_POS_INIT_R, std::chrono::high_resolution_clock::now());

            bool previously_limited_intergoal = false;

            TimePoint trajectory_start_time = std::chrono::high_resolution_clock::now();
            Eigen::Vector3d trajectory_start_pos_r = ROBOT_POS_INIT_R;
            Eigen::Vector3d trajectory_pos_r;
            Eigen::Vector3d trajectory_random_pos_r = Eigen::Vector3d::Zero();

            TRAJECTORY_TYPE trajectory = TRAJECTORY_HOME_RANDOM;
            TRAJECTORY_TYPE prev_trajectory = TRAJECTORY_HOME_RANDOM;
            trajectory_queue.push_back(TRAJECTORY_HOME_RIGHT);
            use_predefined_trajectory = true;
            trajectory_started = false;
            bool trajectory_done = true;

            while (!should_run.stop_requested()) {
                get_shared_data(
                        shared_data_robot,
                        shared_data_robot_mutex,
                        ratio,
                        max_distance_cm,
                        hand_pos_r,
                        hand_vel,
                        hand_measurement_timestamp,
                        joint_angles,
                        angular_velocities,
                        joint_measurement_timestamp,
                        joint_control_timestamp
                );

                // If the robot is not initialized, wait for the joint angles
                if (joint_angles.isZero()) {
                    std::this_thread::sleep_for(std::chrono::microseconds(50));
                    continue;
                }

                Eigen::Vector3d robot_pos_r = ikc->fk(joint_angles);
                mw_robot_pos_r.push_back(robot_pos_r, joint_measurement_timestamp);

                TimePoint t_now = std::chrono::high_resolution_clock::now();

                ///
                bool has_command;
                {
                    std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
                    has_command = shared_data_robot_input.has_command;
                }
                if (has_command) {
                    // GET COMMAND LIST
                    std::list<std::pair<std::string, ROBOT_INPUT_COMMAND_TYPE>> command_list = std::list<std::pair<std::string, ROBOT_INPUT_COMMAND_TYPE>>();
                    {
                        std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
                        std::copy(shared_data_robot_input.command_list.begin(),
                                  shared_data_robot_input.command_list.end(),
                                  std::back_inserter(command_list));
                        shared_data_robot_input.command_list.clear();
                        shared_data_robot_input.has_command = false;
                    }
                    // PROCESS COMMAND LIST
                    for (const std::pair<std::string, ROBOT_INPUT_COMMAND_TYPE> &command: command_list) {
                        if (command.second == ROBOT_INPUT_COMMAND_TYPE::FOLLOW_THE_HAND) {
                            trajectory_queue.push_back(FOLLOW_THE_HAND);
                        } else if (command.second == ROBOT_INPUT_COMMAND_TYPE::GO_NEAR_HOME_AREA) {
                            trajectory_queue.push_back(TRAJECTORY_RIGHT);
                            trajectory_queue.push_back(TRAJECTORY_HOME_RIGHT);
                        } else if (command.second == ROBOT_INPUT_COMMAND_TYPE::GO_HOME) {
                            trajectory_queue.push_back(TRAJECTORY_HOME_RANDOM);
                        } else if (command.second == ROBOT_INPUT_COMMAND_TYPE::FOLLOW_THE_HAND_2D) {
                            trajectory_queue.push_back(FOLLOW_THE_HAND_2D);
                        }
                    }
                }
                ///
                if (trajectory_done && !trajectory_queue.empty()) {
                    auto temp_trajectory = trajectory_queue.front();
                    trajectory_queue.pop_front();
                    std::cout << "Possible new trajectory: " << temp_trajectory << "\n";
                    if (temp_trajectory == TRAJECTORY_RIGHT) {
                        ChangeRobotWorkerStatus(RobotStatus::MOVING_AWAY_FROM_HAND);
                        trajectory_done = false;
                        use_predefined_trajectory = true;
                        trajectory_started = false;
                        trajectory_done_caused_status_change = false;
                        prev_trajectory = trajectory;
                        trajectory = temp_trajectory;
                    } else if (temp_trajectory == TRAJECTORY_HOME_RIGHT) {
                        ChangeRobotWorkerStatus(RobotStatus::MOVING_TO_NEAR_HOME_AREA);
                        trajectory_done = false;
                        use_predefined_trajectory = true;
                        trajectory_started = false;
                        trajectory_done_caused_status_change = false;
                        prev_trajectory = trajectory;
                        trajectory = temp_trajectory;
                    } else if (temp_trajectory == TRAJECTORY_HOME_RANDOM) {
                        ChangeRobotWorkerStatus(RobotStatus::MOVING_TO_HOME);
                        trajectory_done = false;
                        use_predefined_trajectory = true;
                        trajectory_started = false;
                        trajectory_done_caused_status_change = false;
                        prev_trajectory = trajectory;
                        trajectory = temp_trajectory;
                    } else if (temp_trajectory == FOLLOW_THE_HAND &&
                               robot_worker_status == RobotStatus::CONNECTED_AND_AT_HOME) {
                        if (hand_pos_r != Eigen::Vector3d::Zero() && robot_pos_r != Eigen::Vector3d::Zero()) {
                            hand_offset_pos_r = hand_pos_r;
                            robot_offset_pos_r = robot_pos_r;
                            {
                                std::string event_message = "TRACKING_STARTED";
                                spdlog::info("RobotController(): TRACKING_STARTED robot_pos_r: x-" +
                                             std::to_string(robot_pos_r.x()) + " y-" +
                                             std::to_string(robot_pos_r.y()) + " z-" +
                                             std::to_string(robot_pos_r.z()));
                                {
                                    std::lock_guard<std::mutex> lock(shared_data_robot_output_mutex);
                                    shared_data_robot_output.hand_tracking_robot_info.movement_tracking_robot_initial_coordinates = robot_pos_r;
                                    shared_data_robot_output.hand_tracking_robot_info.movement_tracking_MW_begin_coordinates = robot_pos_r;
                                    shared_data_robot_output.hand_tracking_robot_info.movement_tracking_MW_end_coordinates = robot_pos_r;
                                    shared_data_robot_output.hand_tracking_robot_info.movement_tracking_sync_date_time = date::format(
                                            "%F %T",
                                            std::chrono::system_clock::now());
                                }
                            }
                            use_predefined_trajectory = false; // start hand_tracking
                            prev_trajectory = trajectory;
                            trajectory = temp_trajectory;
                            ChangeRobotWorkerStatus(RobotStatus::CONNECTED_AND_FOLLOWING_HAND);
                        } else {
                            if (shared_data_robot_input.command_list.empty()) {
                                std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
                                shared_data_robot_input.command_list.emplace_back("",
                                                                                  ROBOT_INPUT_COMMAND_TYPE::FOLLOW_THE_HAND);
                                shared_data_robot_input.has_command = true;
                            }
                        }
                    } else if (temp_trajectory == FOLLOW_THE_HAND_2D && robot_worker_status == RobotStatus::CONNECTED_AND_AT_HOME) {
                        if (hand_pos_r != Eigen::Vector3d::Zero() && robot_pos_r != Eigen::Vector3d::Zero()) {
                            hand_offset_pos_r = hand_pos_r;
                            robot_offset_pos_r = robot_pos_r;
                            {
                                std::string event_message = "TRACKING_STARTED";
                                spdlog::info("RobotController(): TRACKING_STARTED robot_pos_r: x-" +
                                             std::to_string(robot_pos_r.x()) + " y-" +
                                             std::to_string(robot_pos_r.y()) + " z-" +
                                             std::to_string(robot_pos_r.z()));
                            }
                            use_predefined_trajectory = false; // start hand_tracking
                            prev_trajectory = trajectory;
                            trajectory = temp_trajectory;
                            ChangeRobotWorkerStatus(RobotStatus::CONNECTED_AND_FOLLOWING_2D_HAND);
                        } else {
                            if (shared_data_robot_input.command_list.empty()) {
                                std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
                                shared_data_robot_input.command_list.emplace_back("", ROBOT_INPUT_COMMAND_TYPE::FOLLOW_THE_HAND_2D);
                                shared_data_robot_input.has_command = true;
                            }
                        }
                    }

                    if (prev_trajectory == FOLLOW_THE_HAND && trajectory != FOLLOW_THE_HAND) {
                        spdlog::info(
                                "RobotController(): TRACKING_ENDED robot_pos_r: x-" +
                                std::to_string(robot_pos_r.x()) +
                                " y-" + std::to_string(robot_pos_r.y()) + " z-" + std::to_string(robot_pos_r.z()));
                        {
                            std::lock_guard<std::mutex> lock(shared_data_robot_output_mutex);
                            shared_data_robot_output.hand_tracking_robot_info.movement_tracking_robot_final_coordinates = robot_pos_r;
                        }
                    }

                    if (prev_trajectory == FOLLOW_THE_HAND_2D && trajectory != FOLLOW_THE_HAND_2D) {
                        spdlog::info(
                                "RobotController(): TRACKING_ENDED robot_pos_r: x-" +
                                std::to_string(robot_pos_r.x()) +
                                " y-" + std::to_string(robot_pos_r.y()) + " z-" + std::to_string(robot_pos_r.z()));
                    }
                } else if (trajectory_done && !trajectory_done_caused_status_change) {
                    if (trajectory == TRAJECTORY_HOME_RIGHT) {
                        ChangeRobotWorkerStatus(RobotStatus::CONNECTED_AND_AT_NEAR_HOME_AREA);
                    } else if (trajectory == TRAJECTORY_HOME_RANDOM) {
                        ChangeRobotWorkerStatus(RobotStatus::CONNECTED_AND_AT_HOME);
                    }
                    trajectory_done_caused_status_change = true;
                }

                if (use_predefined_trajectory) {
                    trajectory_pos_r = compute_trajectory_goal(
                            trajectory,
                            robot_pos_r,
                            trajectory_start_pos_r,
                            trajectory_random_pos_r,
                            trajectory_start_time,
                            trajectory_done
                    );
                } else {
                    trajectory_pos_r = Eigen::Vector3d::Zero();
                }

                Eigen::Vector3d goal_pos_r;
                if (robot_worker_status == RobotStatus::CONNECTED_AND_FOLLOWING_HAND) {
                    {
                        std::lock_guard<std::mutex> lock(shared_data_robot_output_mutex);
                        if (robot_pos_r.x() <
                            shared_data_robot_output.hand_tracking_robot_info.movement_tracking_MW_begin_coordinates.x()) {
                            shared_data_robot_output.hand_tracking_robot_info.movement_tracking_MW_begin_coordinates = robot_pos_r;
                        } else if (robot_pos_r.x() >
                                   shared_data_robot_output.hand_tracking_robot_info.movement_tracking_MW_end_coordinates.x()) {
                            shared_data_robot_output.hand_tracking_robot_info.movement_tracking_MW_end_coordinates = robot_pos_r;
                        }
                    }
                    goal_pos_r = compute_goal_position(
                            hand_pos_r,
                            hand_vel,
                            hand_measurement_timestamp,
                            hand_offset_pos_r,
                            robot_offset_pos_r,
                            trajectory_pos_r,
                            ratio
                    );
                } else if (robot_worker_status == RobotStatus::CONNECTED_AND_FOLLOWING_2D_HAND) {
                    goal_pos_r = compute_2D_position(
                            hand_pos_r,
                            hand_offset_pos_r,
                            robot_offset_pos_r
                    );
                } else {
                    goal_pos_r = trajectory_pos_r;
                }

                Eigen::Vector3d intergoal_pos_r;
                if (robot_worker_status == RobotStatus::CONNECTED_AND_FOLLOWING_HAND) {
                    intergoal_pos_r = PositionComputer::compute_intergoal_position(mw_robot_pos_r, goal_pos_r,
                                                                                   joint_control_timestamp,
                                                                                   previously_limited_intergoal,
                                                                                   MIN_VECT, MAX_VECT, MAX_VEL_NORM,
                                                                                   MAX_VEL_COMPENSATION_MULTIPLIER,
                                                                                   MAX_VEL_DECREASE_MULTIPLIER,
                                                                                   MAX_DIST_RECOVERY_MULTIPLIER,
                                                                                   MOVING_WINDOW_ACTIVE_FROM_SEC,
                                                                                   HAND_AXES, ROBOT_POS_INIT_R,
                                                                                   HAND_AXES_INVERSE);
                } else {
                    intergoal_pos_r = goal_pos_r;
                }
                //std::cout << "goal_pos_r: " << goal_pos_r.transpose() << " trajectory:" << trajectory_pos_r.transpose()  << " intergoal:" << intergoal_pos_r.transpose() << std::endl;
                //std::cout << "Robot and goal pos diff: " << (robot_pos_r - intergoal_pos_r).transpose() << "\n";

                if (robot_worker_status == RobotStatus::CONNECTED_AND_FOLLOWING_2D_HAND && !position_2d_speed_is_under_limits(mw_robot_pos_r, intergoal_pos_r,
                                                      joint_control_timestamp)) {
                    std::cout << "Robot pos: " << robot_pos_r << "\n";
                    std::cout << "Goal pos: " << intergoal_pos_r << "\n";
                    std::lock_guard<std::mutex> lock(shared_data_robot_input_mutex);
                    shared_data_robot_input.command_list.emplace_back("", ROBOT_INPUT_COMMAND_TYPE::GO_HOME);
                    shared_data_robot_input.has_command = true;
                } else if (verify_position(intergoal_pos_r)) {
                    if (ikc->ik(joint_angles, intergoal_pos_r)) {
                        std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
                        goal_joint_angles = ikc->Q_star;
                        shared_data_robot.goal_joint_angles = goal_joint_angles;
                        shared_data_robot.goal_computed_timestamp = std::chrono::high_resolution_clock::now();
                    }
                }
                std::this_thread::sleep_for(std::chrono::microseconds(50));
            }
            kortexThread.request_stop();
            kortexThread.join();
        } catch (std::exception &e) {
            spdlog::error("RobotController(): {}", e.what());
            delete kc;
            kc = nullptr;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void RobotWorker::ChangeRobotWorkerStatus(RobotStatus new_robot_worker_status) {
    robot_worker_status = new_robot_worker_status;
    {
        std::lock_guard<std::mutex> lock(shared_data_robot_output_mutex);
        shared_data_robot_output.robot_worker_status = new_robot_worker_status;
    }
}
