# Real-time teleoperation of a robot arm for manipulating self-localization - C++ application

This project aims to provide capabilities for organizing experiments involving the teleoperation of the Kinova3 robot using Qualisys motion cameras for tracking human motion.

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- A Kinova3 robot connected to the local network
- A Qualisys Motion Tracking system connected to the local network with cameras ready
- Docker installed on the machine where this application will run
- Xquartz (on MacOS)

### Installing

1. Clone the repository onto your local machine:
```
git clone https://gitlab.fel.cvut.cz/body-schema/tele-touch.git
```

2. Build the Docker container using the `docker compose` command:
```
cd cpp
docker compose build
```

3. Ensure that you use the correct settings in `/cpp/app/config/settings.ini`

4. Before running the project, make sure the robot and Qualisys Motion Capture system are ready with the correct AIM model and calibration.

### Running the project

0. Enable `xhost` (for macOS and Linux only) by running the following commands in the console:
```
export DISPLAY=:0
/opt/X11/bin/xhost +
```

1. Start the Docker containers using the `docker compose` command:
```
cd cpp
docker compose run kinova_cpp
cmake ..
cmake --build .
./QualisysKinovaInterface
```

2. Start the application using the command:
```
./QualisysKinovaInterface
```

3. The project should now be running, and you should see the setup screen for experiment configuration.

## Project directory Structure

### ./cpp
Contains Docker files, the `app`, and the `output_data_example` project directories.

### ./cpp/app
Contains all source code required to run the application and unit tests.

### ./cpp/output_data_example
Contains examples of input and output files of all possible types.

### ./cpp/app/assets
Contains media elements that are used in the application.

### ./cpp/app/config
Contains the `settings.ini` file for the configuration of the main aspects of the application.

### ./cpp/app/lib
Contains copies of the application libraries' source code.

### ./cpp/app/test
Contains unit test classes.

### ./cpp/app/src
Contains the application source code.

## Input and output files
The application may require a CSV participant data file as input for experiments. Additionally, the application will generate several output files:
+ The output file from Qualisys motion capture system for each trial;
+ The output file that contain information about the experiment block;
+ The output file with detailed information for every trial in the familiarization block;
+ The output file with information about every trial that was performed during an experiment block.

Examples of all these files can be found in the `./cpp/output_data_example` directory.

## Application architecture
The application source code is organized into classes, each of which belongs to one of the following components:
### View
An application component represented by an interface that defines the necessary UI-drawing functions used by the presenter. These functions must be implemented in the view implementation. Utilizing this interface allows the application to have several different implementations of the same view, for example with different graphics libraries, which will work with the same presenter.

### View Implementation
An application component represented by a class that implements the view interface. It can utilize graphical libraries and utilities to draw the UI as directed by the presenter. The View is designed to be maximally passive; it receives UI events and directly passes those events which can significantly affect the application state changes without processing them to the presenter by calling the presenter's methods and waits for presenter’s commands. Additionally, the View can only store the data needed to draw the user interface and to utilize UI libraries. The logic behind the View can be distributed among individual graphical components at the programmer's discretion.

### Presenter 
An application component represented by a class that includes methods and data to handle and react to events that can significantly affect application state changes coming from the view. The presenter should not strictly depend on any graphical libraries or components; the view is responsible for work with graphical elements. The presenter can independently process and store data received as a result of reacting to events from the view, or it can utilize services and utilities for this purpose. Additionally, the presenter calls functions of the view interface to draw the application state changes.

### Service 
An application component represented by a class that includes semantically separated non-UI methods, with names that reflect its purpose. These methods can be called in various presenters and other application services. A service encapsulates the data required for the operation of these methods and can also modify data that can be used outside the application context, such as files on ROM, the motion capture system state, robot states, and others. Only services can create new threads, with the operation logic described in workers, and communicate with them using shared data. This ensures that every communication between UI and non-UI components is done by calling methods with names that represent their semantics.

### Worker
An application component represented by a class that includes methods and data designed to create new threads and work within them. Workers can only appear as part of some services and may include utilities and services to perform certain operations.

### Utility
An application component represented by a class that includes semantically separated methods, which can be used in various presenters, views, services, workers and other utilities. A utility can also encapsulate the data required for its methods, but can modify data that can be used outside the application context only as a part of a service and cannot spawn new threads.

![The application architecture diagram](arch_diagram.png)
## Unit tests

For simplicity in writing unit tests, the Catch2 framework is used. The following unit tests are located in the `./cpp/app/tests` directory:
+ Trajectory boundaries test for the generate_linear_trajectory method: Generates the linear trajectory from the point [-1, 5.5, 3.5] to [10, -1, 3.4] and checks that the trajectory points are always inside the boundaries defined in the application settings.
+ Trajectory start and end position test for the generate_linear_trajectory method: Generates the linear trajectory from the point [-1, 5.5, 3.5] to [10, -1, 3.4] and checks that the trajectory starts and ends at the defined points.
+ Trajectory traversing speed tests for the generate_linear_trajectory method: Emulates situations where an object is traversing a generated trajectory and checks if the object speed is always constant and consistent, indicating that the trajectory is generated correctly.
+ Boundary box checks for the compute_intergoal_position method: Emulates movements inside and outside the boundary box defined in the application settings and sends them to the compute_intergoal_position method. Checks if compute_intergoal_position always returns a position inside the boundary box.
+ Slow mode check for the compute_intergoal_position method: Emulates extremely fast movement and sends it to the compute_intergoal_position method. Checks if compute_intergoal_position always returns a position that results in a speed not exceeding 1 km/h.

## Built With

* [Kortex API](https://github.com/Kinovarobotics/kortex) - Used for communication with the Kinova3 robot;
* [Qualisys Motion Capture](https://www.qualisys.com/) - Used for capturing 3D keypoints used by the AIM model;
* [Catch2](https://github.com/catchorg/Catch2) - Used for run unit tests;
* [wxWidgets](https://www.wxwidgets.org/) - Used for render GUI;

## Authors

* **Oleg Baryshnikov**
* **Adam Rojík** - [Author's Github](https://github.com/rojikada)

